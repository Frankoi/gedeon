# - try to find the cutil library provided with the cuda SDK
#  CUTIL_INCLUDE_DIR, where to find cutil.h, etc.
#  CUTIL_LIBRARIES, the libraries to link against
#  CUTIL_FOUND, If false, do not try to use GLUT.

# the path to the SDK (CUDA_SDK_ROOT_DIR) needs to be defined while looking for the cuda library like ~/NVIDIA_GPU_Computing_SDK/C/



find_path(CUDA_CUT_INCLUDE_DIR cutil.h
	PATHS ${CUDA_SDK_SEARCH_PATH}
	PATH_SUFFIXES "common/inc"
	DOC "Location of cutil.h"
	NO_DEFAULT_PATH
)
find_path(CUDA_CUT_INCLUDE_DIR cutil.h DOC "Location of cutil.h")
mark_as_advanced(CUDA_CUT_INCLUDE_DIR)

 if(CMAKE_SIZEOF_VOID_P EQUAL 8)
   set(cuda_cutil_name cutil64 cutil_x86_64)
 else(CMAKE_SIZEOF_VOID_P EQUAL 8)
   set(cuda_cutil_name cutil32 cutil_x86_32)
 endif(CMAKE_SIZEOF_VOID_P EQUAL 8)

 find_library(CUDA_CUT_LIBRARY
   NAMES cutil ${cuda_cutil_name}
   PATHS ${CUDA_SDK_SEARCH_PATH}
   # The new version of the sdk shows up in common/lib, but the old one is in lib
   PATH_SUFFIXES "common/lib" "lib"
   DOC "Location of cutil library"
   NO_DEFAULT_PATH
   )
 # Now search system paths
 find_library(CUDA_CUT_LIBRARY NAMES cutil ${cuda_cutil_name} DOC "Location of cutil library")
 mark_as_advanced(CUDA_CUT_LIBRARY)
 set(CUDA_CUT_LIBRARIES ${CUDA_CUT_LIBRARY})

SET( CUDA_CUT_FOUND OFF )
IF(CUDA_CUT_INCLUDE_DIR)
  IF(CUDA_CUT_LIBRARY)
    SET( CUDA_CUT_FOUND ON )    
  ENDIF(CUDA_CUT_LIBRARY)
ENDIF(CUDA_CUT_INCLUDE_DIR)

