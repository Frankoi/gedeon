#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindOpenNI2.cmake created in 10 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindOpenNI2.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

# This sets the following variables:
# OPENNI2_FOUND - True if OpenNI2 was found.
# OPENNI2_INCLUDE_DIR - Directories containing the OpenNI2 include files.
# OPENNI2_LIBRARIES - Libraries needed to use OpenNI2.
# OPENNI2_DEFINITIONS - Compiler flags for OpenNI2.

find_package(PkgConfig)
if(${CMAKE_VERSION} VERSION_LESS 2.8.2)
  pkg_check_modules(PC_OPENNI2 openni2-dev)
else()
  pkg_check_modules(PC_OPENNI2 QUIET openni2-dev)
endif()

set(OPENNI2_DEFINITIONS ${PC_OPENNI2_CFLAGS_OTHER})

#add a hint so that it can find it without the pkg-config
find_path(OPENNI2_INCLUDE_DIR OpenNI.h
          HINTS ${PC_OPENNI2_INCLUDEDIR} ${PC_OPENNI2_INCLUDE_DIRS} /usr/include/openni2
          PATHS "$ENV{PROGRAMFILES}/openni2/include" "$ENV{PROGRAMW6432}/openni2/include")

#add a hint so that it can find it without the pkg-config
find_library(OPENNI2_LIBRARY
             NAMES OpenNI2
             HINTS ${PC_OPENNI2_LIBDIR} ${PC_OPENNI2_LIBRARY_DIRS} /usr/lib
             PATHS "$ENV{PROGRAMFILES}/openni2/lib${OPENNI2_SUFFIX}" "$ENV{PROGRAMW6432}/openni2/lib${OPENNI2_SUFFIX}"
)

if(APPLE)
  set(OPENNI2_LIBRARIES ${OPENNI2_LIBRARY} usb)
else()
  set(OPENNI2_LIBRARIES ${OPENNI2_LIBRARY} )
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OPENNI2 DEFAULT_MSG OPENNI2_LIBRARIES OPENNI2_INCLUDE_DIR)

mark_as_advanced(OPENNI2_LIBS OPENNI2_INCLUDE_DIR)
if(OPENNI2_FOUND)
  include_directories(${OPENNI2_INCLUDE_DIR})
endif(OPENNI2_FOUND)

