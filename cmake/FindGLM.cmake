#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindGLM.cmake created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindGLM.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

# - try to find GLM include files (No library)
#  GLM_INCLUDE_DIR, where to find glm/glm.hpp, etc.
#  GLM_FOUND will be set to TRUE if found

FIND_PATH(GLM_INCLUDE_DIR glm/glm.hpp
	/usr/include/
	/usr/local/include/
	${PROJECT_SOURCE_DIR}/thirdparty/include/
      )
 
SET( GLM_FOUND OFF )
IF(GLM_INCLUDE_DIR)
	SET( GLM_FOUND ON )
ENDIF()

MARK_AS_ADVANCED( GLM_FOUND GLM_INCLUDE_DIR )
