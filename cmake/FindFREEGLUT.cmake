#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindFREEGLUT.cmake created in 11 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindFREEGLUT.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

# - try to find glut library and include files
#  FREEGLUT_INCLUDE_DIR, where to find GL/glut.h, etc.
#  FREEGLUT_LIBRARIES, the libraries to link against
#  FREEGLUT_FOUND, If false, do not try to use GLUT.
# Also defined, but not for general use are:
#  FREEGLUT_glut_LIBRARY = the full path to the glut library.

IF (WIN32)
  FIND_PATH( FREEGLUT_INCLUDE_DIR NAMES GL/freeglut.h 
    PATHS  
	${FREEGLUT_ROOT_PATH}/include
	$ENV{PROGRAMFILES}/include
    ${PROJECT_SOURCE_DIR}/thirdparty/include)
  FIND_LIBRARY( FREEGLUT_glut_LIBRARY NAMES freeglut freeglut32 freeglut
    PATHS
    ${OPENGL_LIBRARY_DIR}
	$ENV{PROGRAMFILES}/lib
    ${FREEGLUT_ROOT_PATH}/Release
    ${PROJECT_SOURCE_DIR}/thirdparty/lib/
    )

ELSE (WIN32)
  
  IF (APPLE)
    # These values for Apple could probably do with improvement.
    FIND_PATH( FREEGLUT_INCLUDE_DIR freeglut.h
      /System/Library/Frameworks/GLUT.framework/Versions/A/Headers
      ${PROJECT_SOURCE_DIR}/thirdparty/include
      ${OPENGL_LIBRARY_DIR}
      )
    SET(FREEGLUT_glut_LIBRARY "-framework FREEGLUT" CACHE STRING "FREEGLUT library for OSX") 
  ELSE (APPLE)
    
    FIND_PATH( FREEGLUT_INCLUDE_DIR GL/freeglut.h
      /usr/include/GL
	/usr/local/include
	/opt/local/include
      /usr/openwin/share/include
      /usr/openwin/include
      /opt/graphics/OpenGL/include
      /opt/graphics/OpenGL/contrib/libglut
      ${PROJECT_SOURCE_DIR}/thirdparty/include
      )
  
    FIND_LIBRARY( FREEGLUT_glut_LIBRARY glut
      /usr/openwin/lib
      /usr/lib
      /usr/lib64
	  /usr/local/lib64
	  /usr/local/lib
	  /sw/lib
	  /opt/local/lib

      ${PROJECT_SOURCE_DIR}/thirdparty/lib
      )
    
  ENDIF (APPLE)
  
ENDIF (WIN32)

SET( FREEGLUT_FOUND OFF )
IF(FREEGLUT_INCLUDE_DIR)
  IF(FREEGLUT_glut_LIBRARY)
    # Is -lXi and -lXmu required on all platforms that have it?
    # If not, we need some way to figure out what platform we are on.
    SET( FREEGLUT_LIBRARIES
      ${FREEGLUT_glut_LIBRARY}
      )
    SET( FREEGLUT_FOUND ON )
    
    #The following deprecated settings are for backwards compatibility with CMake1.4
    SET (FREEGLUT_LIBRARY ${FREEGLUT_LIBRARIES})
    SET (FREEGLUT_INCLUDE_PATH ${FREEGLUT_INCLUDE_DIR})
    
  ENDIF(FREEGLUT_glut_LIBRARY)
ENDIF(FREEGLUT_INCLUDE_DIR)

MARK_AS_ADVANCED(
  FREEGLUT_INCLUDE_DIR
  FREEGLUT_glut_LIBRARY
  )
