/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/pointcloudprocessing/normalestimation.cuh"
#include "gedeon/calibration/calibration.cuh"
#include "gedeon/imageprocessing/bilateralfiltering.cuh"
#include "gedeon/pointcloudprocessing/pointcloudprocessing.cuh"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "gedeon/grabber/opennidriver.hpp"
#include "gedeon/grabber/opennigrabber.hpp"

#include <cuda.h>
#include <cuda_runtime.h>

#include <cstdlib>

using namespace gedeon;
cv::Mat intensity;
cv::Mat color;
cv::Mat depth;
boost::mutex mutex;

float *d_input_depth = 0;

float intrinsic[9] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
float rotation[9] = { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
float translation[3] = { 0.0, 0.0, 0.0 };
bool first = true;

GPU::Tracker *t = 0;;

void displayImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				if (true == s->hasIntensity()) {
					if (0 == intensity.data) {
						intensity = cv::Mat(
								s->getImage()->intensity.getSize().height,
								s->getImage()->intensity.getSize().width,
								CV_8UC1);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(intensity.data, imgptr->intensity.getData().get(),
							s->getImage()->color.getSize().width
									* s->getImage()->color.getSize().height
									* sizeof(unsigned char));
				}
				if (true == s->hasColor()) {
					if (0 == color.data) {
						color = cv::Mat(s->getImage()->color.getSize().height,
								s->getImage()->color.getSize().width, CV_8UC3);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(color.data, imgptr->color.getData().get(),
							s->getImage()->color.getSize().width
									* s->getImage()->color.getSize().height * 3
									* sizeof(unsigned char));
					cvtColor(color, color, cv::COLOR_RGB2BGR);
				}
				if (true == s->hasDepth()) {
					if (0 == depth.data) {
						depth = cv::Mat(s->getImage()->depth.getSize().height,
								s->getImage()->depth.getSize().width, CV_32FC1);
						cudaMalloc((void**) &(d_input_depth),
								s->getImage()->depth.getSize().height
										* s->getImage()->depth.getSize().width
										* sizeof(float));

						s->getParameter(OPENNI_FOCAL_DEPTH, intrinsic[0]);
						s->getParameter(OPENNI_PP_X_DEPTH, intrinsic[2]);
						s->getParameter(OPENNI_FOCAL_DEPTH, intrinsic[4]);
						s->getParameter(OPENNI_PP_Y_DEPTH, intrinsic[5]);

						t = new GPU::Tracker(640,480,intrinsic);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(depth.data, imgptr->depth.getData().get(),
							s->getImage()->depth.getSize().height
									* s->getImage()->depth.getSize().width
									* sizeof(float));

					cudaMemcpy(d_input_depth, imgptr->depth.getData().get(),
							s->getImage()->depth.getSize().height
									* s->getImage()->depth.getSize().width
									* sizeof(float),
							cudaMemcpyHostToDevice);

					t->estimatePose(d_input_depth);
					depth *= 1.0 / 5.0f;
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}

int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	OpenNIDriver& driver = OpenNIDriver::getInstance();

	std::cout << "Looking for " << driver.getName() << " devices..."
			<< std::endl;
	driver.refresh();

	std::cout << "Number of devices found: " << driver.getCount() << std::endl;
	if (driver.getCount() > 0) {
		std::vector<std::string> devices = driver.populate();
		std::vector<std::string>::iterator it = devices.begin();
		while (devices.end() != it) {
			std::cout << "* " << *it << std::endl;
			++it;
		}

		std::cout << "Access the first device..." << std::endl;
		try {
			OpenNIGrabber sensor;
			if (false == sensor.init(0, &driver)) {
				return EXIT_FAILURE;
			}

			boost::signals2::connection c;
			if (false
					== sensor.connect("updated", boost::bind(displayImage, _1),
							c)) {
				std::cerr << "Error: Cannot connect to the event of the sensor"
						<< std::endl;
			}

			Log::add().info(sensor.getName() + " Connected");

			sensor.play();

			bool running = true;
			while (true == running) {

				char k = cv::waitKey(40);
				if (k == 27 || k == 'q') {
					running = false;
				}
				if (k == 'i') {
					sensor.trigger(OPENNI_GRAB_INTENSITY);
				}
				if (k == 'c') {
					sensor.trigger(OPENNI_GRAB_COLOR);
				}

				if (intensity.data != 0) {
					boost::mutex::scoped_lock l(mutex);
					cv::imshow("Intensity", intensity);
				}
				if (color.data != 0) {
					boost::mutex::scoped_lock l(mutex);
					cv::imshow("Color", color);
				}
				if (depth.data != 0) {
					boost::mutex::scoped_lock l(mutex);
					cv::imshow("Depth", depth);
				}
			}

			std::cout << "Stop the capture..." << std::endl;
			sensor.stop();
			c.disconnect();
			cv::destroyAllWindows();

		} catch (Exception e) {
			std::cerr << e << std::endl;
		}
	}
	return EXIT_SUCCESS;
}
