/***************************************************************************\
 * Copyright (C) by Keio University
 * main.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of HVRL Engine Library.
 *
 * The HVRL Engine Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/imageprocessing/undistort.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

using namespace gedeon;


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	if(argc != 2){
		Log::add().error("main","Execution context: undistortcuda [image file]");
		return EXIT_FAILURE;
	}

	/*
	 * Intrinsic parameters and distortion coefficients for the image "distorted.png"
	 */

	float K[9] = {748.3295675, 0., 317.649034635, 0.,
       750.31471157, 238.02775248, 0., 0., 1.};

	float distortion[5] = {-0.383842459, 0.373372748,
       0.002892372, .00071957048, -0.874445979};

	/*
	 * Load the image
	 */
	cv::Mat image = cv::imread(argv[1], cv::IMREAD_COLOR);
	Size s(image.size().width,image.size().height);

	RGBImage input_image(s,image.data);

	/*
	 * Create the output images
	 */
	cv::Mat ouput_image(image.size(),image.type());
	cv::Mat ouput_image_bilinear(image.size(),image.type());

	/*
	 * Compute the undistort map
	 */
	boost::shared_array<float> undistort_map = Undistort::computeDistortionMap(distortion,K,s);

	/*
	 * Compute the bilinear interpolation map
	 */
	boost::shared_array<float> bilinear_map = Undistort::computeBilinearInterpolation(undistort_map.get(), s);

	/*
	 * Apply the undistortion without bilinear interpolation
	 */
	RGBImage output_image_tmp;
	if(false == Undistort::undistort(output_image_tmp, input_image, undistort_map)){
		return EXIT_FAILURE;
	}
	memcpy(ouput_image.data,output_image_tmp.getData().get(),image.size().width*image.size().height*3*sizeof(unsigned char));

	/*
	 * Apply the undistortion with bilinear interpolation
	 */

	if(false == Undistort::undistortBilinear(output_image_tmp, input_image,
			undistort_map, bilinear_map)){
		return EXIT_FAILURE;
	}
	memcpy(ouput_image_bilinear.data,output_image_tmp.getData().get(),image.size().width*image.size().height*3*sizeof(unsigned char));


	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}

		cv::imshow("Input image",image);
		cv::imshow("Ouput image - Undistort",ouput_image);
		cv::imshow("Ouput image - Undistort Bilinear",ouput_image_bilinear);
	}

	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

