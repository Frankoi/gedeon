/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/imageprocessing/bilateralfiltering.cuh"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cuda.h>
#include <cuda_runtime.h>

#include <cstdlib>

using namespace gedeon;


int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	if(argc != 2){
		Log::add().error("main","Execution context: bilateralfilteringcuda [image file]");
		return EXIT_FAILURE;
	}

	/*
	 * Load the image in grayscale
	 */
	cv::Mat image = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
	int size = image.size().width * image.size().height;
	cv::Mat input_image(image.size(),CV_32F);
	cv::Mat ouput_image(image.size(),CV_32F);
	for(int i = 0; i < image.size().height; ++i){
		for(int j = 0; j < image.size().width; ++j){
			input_image.at<float>(i,j) = image.at<unsigned char>(i,j) / 255.0f;
		}
	}

	/*
	 * copy data onto the device and create memory space
	 */
	float * input_image_device = 0;
	float * output_image_device = 0;
	cudaMalloc((void**) &(input_image_device), size * sizeof(float));
	cudaMalloc((void**) &(output_image_device), size * sizeof(float));
	cudaMemcpy(input_image_device, input_image.data, size * sizeof(float), cudaMemcpyHostToDevice);
	unsigned int radius = 3;
	float space_coefficient = 18.0f;
	float range_coefficient = 0.1f;
	unsigned int block_x = 16;
	unsigned int block_y = 16;

	if(false == GPU::BilateralFiltering::bilateralFilterGray(output_image_device, input_image_device,
			radius, space_coefficient, range_coefficient,
			input_image.size().width, input_image.size().height,
			block_x, block_y)){
		return EXIT_FAILURE;
	}

	/*
	 * Copy the result from the GPU to the memory
	 */
	cudaMemcpy(ouput_image.data, output_image_device, size * sizeof(float), cudaMemcpyDeviceToHost);
	cudaFree(input_image_device);
	cudaFree(output_image_device);

	bool running = true;
	while (true == running) {

		char k = cv::waitKey(40);
		if (k == 27 || k == 'q') {
			running = false;
		}

		cv::imshow("Input image",input_image);
		cv::imshow("Ouput image",ouput_image);
	}

	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

