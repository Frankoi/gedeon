#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * CMakeLists.txt is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

PROJECT(GEDEON_SAMPLE_OPENCV)

   	INCLUDE_DIRECTORIES(
        	"${GEDEON_SOURCE_DIR}/modules/core/include/"
        	"${GEDEON_SOURCE_DIR}/modules/datatypes/include/"
        	"${GEDEON_SOURCE_DIR}/modules/grabber/include/"
        	${Boost_INCLUDE_DIRS}
        	${OpenCV_INCLUDE_DIRS}
        	${ZLIB_INCLUDE_DIRS}
    	)
		
	LINK_DIRECTORIES(
        	${Boost_LIBRARY_DIRS}
	)
	SET(the_target "grabber-opencv")

	SET(cpp_samples main.cpp)

    ADD_EXECUTABLE(${the_target} ${cpp_samples})
    
    SET_TARGET_PROPERTIES(${the_target} PROPERTIES
            OUTPUT_NAME "${the_target}"
            PROJECT_LABEL "(SAMPLE) ${the_target}")
            
    TARGET_LINK_LIBRARIES(${the_target} gedeon_grabber gedeon_core gedeon_datatypes ${Boost_LIBRARIES} ${ZLIB_LIBRARIES} ${OpenCV_LIBS})

 	INSTALL(TARGETS ${the_target}
                RUNTIME DESTINATION "${GEDEON_BIN_INSTALL_PATH}/samples" COMPONENT main)
                

