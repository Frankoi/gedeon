/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * main.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * main.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/tigereyedriver.hpp"
#include "gedeon/grabber/tigereyegrabber.hpp"
#include "gedeon/datatypes/io.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

using namespace gedeon;
cv::Mat intensity;
cv::Mat color;
cv::Mat depth;
boost::mutex mutex;

TigerEyeGrabber sensor;
IO::DataSaver saver;
IO::DataExporter exporter;
const std::string filename_base("tigereye-output");
const unsigned int compression_level = 4;

void displayImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				if (true == s->hasIntensity()) {
					if (0 == intensity.data) {
						intensity = cv::Mat(
								s->getImage()->intensity.getSize().height,
								s->getImage()->intensity.getSize().width,
								CV_8UC1);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(intensity.data, imgptr->intensity.getData().get(),
							s->getImage()->intensity.getSize().width
									* s->getImage()->intensity.getSize().height
									* sizeof(unsigned char));
				}
				if (true == s->hasColor()) {
					if (0 == color.data) {
						color = cv::Mat(s->getImage()->color.getSize().height,
								s->getImage()->color.getSize().width, CV_8UC3);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(color.data, imgptr->color.getData().get(),
							s->getImage()->color.getSize().width
									* s->getImage()->color.getSize().height * 3
									* sizeof(unsigned char));
					cvtColor(color, color, cv::COLOR_RGB2BGR);
				}
				if (true == s->hasDepth()) {
					if (0 == depth.data) {
						depth = cv::Mat(s->getImage()->depth.getSize().height,
								s->getImage()->depth.getSize().width, CV_32FC1);
					}
					RGBDIImage *imgptr = s->getImage().get();
					boost::mutex::scoped_lock l(mutex);
					memcpy(depth.data, imgptr->depth.getData().get(),
							s->getImage()->depth.getSize().height
									* s->getImage()->depth.getSize().width
									* sizeof(float));
					depth *= 1.0 / 5.0f;
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}

void recordImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				RGBDIImagePtr imgptr = s->getImage();

				if (true == exporter.isOpened()) {
					float fr = 30.0f;
					sensor.getParameter(TIGEREYE_FRAMERATE, fr);
					exporter.write(imgptr, fr);
				}

				if (true == saver.isOpened()) {
					std::string timestamp = IO::numberToString(getTimeStamp());
					saver.save(timestamp, imgptr);
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}

int main(int argc, char **argv) {

#ifdef DEBUG
	Log::getInstance().setLevel(COMPLETE_WITH_DEBUG);
#endif

	TigerEyeDriver &driver = TigerEyeDriver::getInstance();
#ifdef WIN32
	driver.setInterface("Local Area Connection");
#else
	driver.setInterface("eth0");
#endif

	std::cout << "Looking for " << driver.getName() << " devices..."
			<< std::endl;
	driver.refresh();

	if (2 == argc) {
		if (false == sensor.init(argv[1], &driver)) {
			return EXIT_FAILURE;
		}
	} else {
		std::cout << "Number of devices found: " << driver.getCount()
				<< std::endl;
		if (driver.getCount() == 0) {
			std::cerr << "No device available" << std::endl;
			return EXIT_FAILURE;
		}

		std::vector<std::string> devices = driver.populate();
		std::vector<std::string>::iterator it = devices.begin();
		while (devices.end() != it) {
			std::cout << "* " << *it << std::endl;
			++it;
		}
		std::cout << "Access the first device..." << std::endl;
		if (false == sensor.init(0, &driver)) {
			return EXIT_FAILURE;
		}
		std::cout << "ok..." << std::endl;

	}

	boost::signals2::connection c;
	boost::signals2::connection c2;
	try {
		if (false
				== sensor.connect("updated", boost::bind(displayImage, _1),
						c)) {
			std::cerr << "Error: Cannot connect to the event of the sensor"
					<< std::endl;
		}

		if (false
				== sensor.connect("updated", boost::bind(recordImage, _1),
						c2)) {
			std::cerr << "Error: Cannot connect to the event of the sensor"
					<< std::endl;
		}

	} catch (Exception e) {
		std::cerr << e << std::endl;
	}

	std::cout << "Set the framerate to 15Hz" << std::endl;
	sensor.setParameter(TIGEREYE_FRAMERATE, 15.0f);
	std::cout << "Activate the NUC" << std::endl;
	sensor.trigger(TIGEREYE_NUC);

	std::cout << "Start the capture..." << std::endl;
	sensor.play();

	secSleep(1);

	/*float value = 0.0f;
	sensor.getParameter(TIGEREYE_THRESHOLD_LEVEL, value);
	std::cout << value << std::endl;
	sensor.getParameter(TIGEREYE_THRESHOLD_FILTER, value);
	std::cout << value << std::endl;
	sensor.getParameter(TIGEREYE_DETECTOR_GAIN, value);
	std::cout << value << std::endl;
	sensor.getParameter(TIGEREYE_AMPLIFIER_GAIN, value);
	std::cout << value << std::endl;*/

	/*std::cout << "Set the threshold level to 90%" << std::endl;
	sensor.setParameter(TIGEREYE_THRESHOLD_LEVEL, 90.0f);
	std::cout << "Set the detector threshold filter to 90%" << std::endl;
	sensor.setParameter(TIGEREYE_THRESHOLD_FILTER, 90.0f);
	std::cout << "Set the detector gain to 80%" << std::endl;
	sensor.setParameter(TIGEREYE_DETECTOR_GAIN, 80.0f);
	std::cout << "Set the amplifier gain to 80%" << std::endl;
	sensor.setParameter(TIGEREYE_AMPLIFIER_GAIN, 80.0f);*/
	
	bool running = true;
	while (true == running) {

		int k = cv::waitKey(40);

		if (k == 27 || k == 'q') {
			running = false;
		}
		if (k == 'y') {
			std::cout << "Reseting the device" << std::endl;
			sensor.trigger(TIGEREYE_RESET);
			float value = 0.0f;
			sensor.getParameter(TIGEREYE_RESET_COUNTDOWN, value);
			std::cout << value;
			while (value > 0.0) {
				sensor.getParameter(TIGEREYE_RESET_COUNTDOWN, value);
				std::cout << "\r" << value << "    ";
				millisecSleep(10);
			}
			std::cout << "\rDevice ready for capture" << std::endl;
		}
		if( k == 'h'){
			std::cout << "Help" << std::endl;
			std::cout << "------------" << std::endl;
			std::cout << "q: quit the program" << std::endl;
			std::cout << "p: pause" << std::endl;
			std::cout << "g: export in a gmv file" << std::endl;
			std::cout << "d: export in directories" << std::endl;
			std::cout << "s: save images as png files" << std::endl;
			std::cout << "k: save depth and intensity as txt files" << std::endl;
		}
		if (k == 'p') {
			sensor.pause();
		}
		if ('g' == k) {
			if (false == exporter.isOpened()) {
				exporter.open(filename_base + ".gmv", compression_level);
			} else {
				exporter.close();
			}
		}
		if ('d' == k) {
			if (false == saver.isOpened()) {
				saver.open(filename_base);
			} else {
				saver.close();
			}
		}

		if ('i' == k) {
			sensor.trigger(TIGEREYE_INTERNAL_RECORD);
		}
		if ('s' == k) {
			unsigned long long ts = getTimeStamp();
			if (intensity.data != 0) {
				cv::imwrite("intensity-" + IO::numberToString(ts) + ".png",
						intensity);
			}
			if (color.data != 0) {
				cv::imwrite("color-" + IO::numberToString(ts) + ".png", color);
			}
			if (depth.data != 0) {
				cv::imwrite("depth-" + IO::numberToString(ts) + ".png", depth);
			}
		}
		if (intensity.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Intensity", intensity);
		}
		if (color.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Color", color);
		}
		if (depth.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Depth", depth);
		}
	}
	
	std::cout << "Stop the capture..." << std::endl;
	sensor.stop();
	c.disconnect();
	c2.disconnect();
	cv::destroyAllWindows();

	return EXIT_SUCCESS;
}

