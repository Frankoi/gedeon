#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * CMakeLists.txt is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

ADD_SUBDIRECTORY(opencv)
ADD_SUBDIRECTORY(gedeonfile)

IF(OPENNI_FOUND)
ADD_SUBDIRECTORY(openni)
ENDIF(OPENNI_FOUND)

IF(OPENNI2_FOUND)
ADD_SUBDIRECTORY(openni2)
ENDIF(OPENNI2_FOUND)

IF(DEPTHSENSE_FOUND)
ADD_SUBDIRECTORY(depthsense)
ENDIF(DEPTHSENSE_FOUND)

IF(FLYCAPTURE_FOUND)
ADD_SUBDIRECTORY(flycapture)
ENDIF(FLYCAPTURE_FOUND)

IF(V4L2_FOUND)
ADD_SUBDIRECTORY(v4l2)
ENDIF(V4L2_FOUND)

IF(TIGEREYE_FOUND AND PCAP_FOUND)
ADD_SUBDIRECTORY(tigereye)
ENDIF(TIGEREYE_FOUND AND PCAP_FOUND)
