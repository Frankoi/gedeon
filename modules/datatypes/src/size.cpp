/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * size.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * size.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/datatypes/size.hpp"

namespace gedeon{


Size::Size(void):
	width(0), height(0){}

Size::Size(const unsigned int& w, const unsigned int& h):
	width(w), height(h){}

Size::Size(const Size& s):
	width(s.width), height(s.height){}

Size::~Size(void){};

unsigned int Size::getWidth(void)const { return this->width; }

unsigned int Size::getHeight(void)const { return this->height; }

Size Size::operator+(const Size& s1) {
	unsigned int w = this->width + s1.width;
	unsigned int h= this->height + s1.height;
	return Size(w,h);
}

Size& Size::operator+=(const Size& s) {
	this->width += s.width;
	this->height += s.height;
	return *this;
}

Size Size::operator*(const unsigned int& v) {
	unsigned int w = this->width * v;
	unsigned int h= this->height * v;
	return Size(w,h);
}

Size& Size::operator*=(const unsigned int& v) {
	this->width *= v;
	this->height *= v;
	return *this;
}

Size Size::operator/(const unsigned int& v) {
	assert(v != 0);
	unsigned int w = this->width / v;
	unsigned int h= this->height / v;
	return Size(w,h);
}

Size& Size::operator/=(const unsigned int& v) {
	assert(v != 0);
	this->width /= v;
	this->height /= v;
	return *this;
}

Size Size::operator-(const Size& s) {
	unsigned int w = this->width - s.width;
	unsigned int h= this->height - s.height;
	return Size(w,h);
}

Size& Size::operator-=(const Size& s) {
	this->width -= s.width;
	this->height -= s.height;
	return *this;
}

Size& Size::operator=(const Size& s) {
	this->width = s.width;
	this->height = s.height;
	return *this;
}

bool Size::operator==(const Size& s) {
	return (this->width == s.width) && (this->height == s.height);
}

bool Size::operator!=(const Size& s) {
	return (this->width != s.width) || (this->height != s.height);
}




}
