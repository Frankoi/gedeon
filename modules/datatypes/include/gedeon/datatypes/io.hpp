/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * io.hpp created in 11 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * io.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_DATATYPES__IO_HPP__
#define GEDEON_DATATYPES__IO_HPP__

#include <fstream>
#include "gedeon/datatypes/rgbdimage.hpp"

namespace gedeon {

namespace IO {

extern const char* IMPORTEXPORT_FILENAME_EXTENSION;

/**
 * \brief Class for saving gedeon images in the gmv (Gedeon MoVie) format
 *
 * Each frame is saved as follows:
 * 3 - width color - height color - width depth - height depth - width intensity - height intensity - framerate - size of original data - size of compressed data - compressed data
 *
 * \author Francois de Sorbier
 */
class DataExporter {

public:

	/**
	 * \brief Default constructor
	 */
	DataExporter(void);

	/**
	 * \brief Destructor
	 */
	~DataExporter(void);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] filename The destination filename (.gmv)
	 * \param[in] level The level of compression of data (0 < level < 9). Higher means more compressed but slower processing.
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool open(const std::string& filename, const int& level = 4);

	/**
	 * \brief Close the file and stop the export
	 */
	void close(void);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the RGB + Depth + Intensity image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const RGBDIImagePtr& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the RGB + Depth image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const RGBDImagePtr& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the RGB + Intensity image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const RGBIImagePtr& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the Depth + Intensity image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const DIImagePtr& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the RGB image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const RGBImage& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the Depth image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const DepthImage& img, const float& framerate = 30.0f);

	/**
	 * \brief Open the destination file for the export
	 *
	 * \param[in] img the Intensity image to save
	 * \param[in] framerate The framerate at which the image was captured
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool write(const IntensityImage& img, const float& framerate = 30.0f);

	/**
	 * \brief Check if the file is open or not
	 *
	 * \return true or false if the export file is available or not
	 */
	bool isOpened(void) const {
		return this->output_file.is_open();
	}

private:
	void compressAndStore(const unsigned char* source,
			const size_t& source_size);

private:
	std::ofstream output_file;
	int compression_level;
	uint64_t nb_frames;
};

/**
 * \brief Class for reading gedeon images from a gmv (Gedeon MoVie) file
 *
 * Each frame is saved as follows:
 * 3 - width color - height color - width depth - height depth - width intensity - height intensity - framerate - size of original data - size of compressed data - compressed data
 *
 * \author Francois de Sorbier
 */
class DataImporter {

public:

	/**
	 * \brief Default constructor
	 */
	DataImporter(void);

	/**
	 * \brief Destructor
	 */
	~DataImporter(void);

	/**
	 * \brief Open the destination file for the reading data
	 *
	 * \param[in] filename The source filename (.gmv)
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool open(const std::string& filename);

	/**
	 * \brief Close the file and stop the loading
	 */
	void close(void);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the RGB + Depth + Intensity that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(RGBDIImagePtr& img, float& framerate);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the RGB + Depth that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(RGBDImagePtr& img, float& framerate);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the RGB + Intensity that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(RGBIImagePtr& img, float& framerate);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the Depth + Intensity that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(DIImagePtr& img, float& framerate);


	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the RGB image that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(RGBImage& img, float& framerate);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the Depth image that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(DepthImage& img, float& framerate);

	/**
	 * \brief Grab the next available image in the file
	 *
	 * \param[out] img the Intensity image that will be filled
	 * \param[out] framerate The framerate at which the image was recorder
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool read(IntensityImage& img, float& framerate);

	/**
	 * \brief Check if the file is open or not
	 *
	 * \return true or false if the streaming file is available or not
	 */
	bool isOpened(void) const {
		return this->input_file.is_open();
	}

	/**
	 * \brief Set the stream at the first available frame
	 */
	void reset(void) {
		this->input_file.clear();
		this->input_file.seekg(this->frame_start, this->input_file.beg);
		this->current_frame = 0;
		this->finished = false;
	}

	/**
	 * \brief Return the total number of frames in the stream
	 * \return The total number of frames
	 */
	uint64_t getTotalFrames(void) const {
		return this->nb_frames;
	}

	/**
	 * \brief Return the current frame of the stream
	 * \return The number of the current frames
	 */
	uint64_t getCurrentFrame(void) const {
		return this->current_frame;
	}

	/**
	 * \brief Activate the looping of the stream or not
	 */
	void triggerLoop(void);

private:

	bool getHeaderData(Size& color_size, Size& depth_size, Size& intensity_size,
			float& framerate, size_t& size_data,
			size_t& size_stored);
	void decompressAndStore(unsigned char* source, size_t& stored_size,
			size_t& size_data);

private:
	std::ifstream input_file;
	uint64_t nb_frames;
	uint64_t current_frame;
	std::ifstream::pos_type frame_start;
	bool loop;
	bool finished;
};

class DataLoaderPimpl;

/**
 * \brief Class for reading gedeon images recorded in directories
 *
 * Frames are loaded from corresponding subdirectories color/ depth/ intensity/
 * color folder contains RGB png images
 * depth folder contains yml objects
 * intensity folder contains grayscale images
 *
 * \author Francois de Sorbier
 */
class DataLoader {

public:

	/**
	 * \brief Default constructor
	 */
	DataLoader(void);

	/**
	 * \brief Destructor
	 */
	~DataLoader(void);

	/**
	 * \brief Open the destination directory for the reading data
	 *
	 * \param[in] directory The source directory
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool open(const std::string& directory);

	/**
	 * \brief Stop the loading
	 */
	void close(void);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the RGB + Depth + Intensity that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(RGBDIImagePtr& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the RGB + Depth that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(RGBDImagePtr& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the RGB + Intensity that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(RGBIImagePtr& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the Depth + Intensity that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(DIImagePtr& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the RGB that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(RGBImage& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the Depth that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(DepthImage& img);

	/**
	 * \brief Grab the next available images in directories
	 *
	 * \param[out] img the Intensity that will be filled
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool load(IntensityImage& img);

	/**
	 * \brief Return the total number of frames in the stream
	 * \return The total number of frames
	 */
	unsigned int getTotalFrames(void) const;

	/**
	 * \brief Return the current frame of the stream
	 * \return The number of the current frames
	 */
	unsigned int getCurrentFrame(void) const;

	/**
	 * \brief Set the stream at a given index
	 * \param[in] frame_index The frame index to move the stream at
	 */
	void setCurrentFrame(const unsigned int& frame_index);

	/**
	 * \brief Activate the looping of the stream or not
	 */
	void triggerLoop(void);

	/**
	 * \brief Set the stream at the first available frame
	 */
	void reset(void);

	/**
	 * \brief Return if the stream is open or not
	 * \return True if the stream is open, otherwise false
	 */
	bool isOpened(void) const;

private:

	DataLoaderPimpl *pimpl;

};

/**
 * \brief Class for Saving gedeon images in directories
 *
 * Frames are recorded in corresponding subdirectories color/ depth/ intensity/
 * color folder contains RGB png images
 * depth folder contains yml objects
 * intensity folder contains grayscale images
 *
 * \author Francois de Sorbier
 */
class DataSaver {
public:

	/**
	 * \brief Default constructor
	 */
	DataSaver(void);

	/**
	 * \brief Destructor
	 */
	~DataSaver(void);

	/**
	 * \brief Open/create the destination directory for the saving data
	 *
	 * \param[in] directory The destination directory
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool open(const std::string& directory);

	/**
	 * \brief Stop the saving
	 */
	void close(void);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the RGB + Depth + Intensity that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const RGBDIImagePtr& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the RGB + Depth that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const RGBDImagePtr& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the RGB + Intensity that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const RGBIImagePtr& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the Depth + Intensity that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const DIImagePtr& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the RGB that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const RGBImage& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the Depth that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const DepthImage& img);

	/**
	 * \brief Save the content of the input image in the corresponding directories
	 *
	 * \param[in] filename The filename used to save the sub images
	 * \param[in] img the Intensity that will be saved
	 *
	 * \return true if everything was fine, otherwise false
	 */
	bool save(const std::string& filename, const IntensityImage& img);

	/**
	 * \brief Return if the stream is recording or not
	 * \return True if the stream is recording, otherwise false
	 */
	bool isOpened(void) const {
		return this->open_stream;
	}

private:

	bool checkSubDirectories(const bool& color, const bool& depth,
			const bool& intensity);

private:
	std::string destination_directory;
	bool created_directory;
	bool created_color_directory;
	bool created_depth_directory;
	bool created_intensity_directory;
	bool open_stream;

};

bool saveColorImage(const RGBImage& input, const std::string& filename);
bool saveDepthImage(const DepthImage& input, const std::string& filename);
bool saveIntensityImage(const IntensityImage& input,
		const std::string& filename);

bool loadColorImage(RGBImage& input, const std::string& filename);
bool loadDepthImage(DepthImage& input, const std::string& filename);
bool loadIntensityImage(IntensityImage& input, const std::string& filename);

}

}

#endif
