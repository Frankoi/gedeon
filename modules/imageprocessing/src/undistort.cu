/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * undistort.cu created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * undistort.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/imageprocessing/undistort.cuh"
#include "gedeon/core/log.hpp"
#include "gedeon/core/core.cuh"

namespace gedeon {

namespace GPU {

namespace Undistort {

bool updateUndistortMap(float** d_undistort_map, const float* input_undistort_map, const Size& s){

	if(0 == *d_undistort_map){
		if(false == checkStatus( cudaMalloc((void**) d_undistort_map,
								2 * s.width * s.height * sizeof(float)))){
			return false;
		}
	}

	if(false == checkStatus(
				cudaMemcpy(*d_undistort_map, input_undistort_map,
						2 * s.width * s.height * sizeof(float), cudaMemcpyHostToDevice))){
			cudaFree(*d_undistort_map);
			*d_undistort_map = 0;
			return false;
		}

	return true;
}


bool updateBilinearInterpolationMap(float** d_bilinear_map, const float* input_bilinear_map, const Size& s){

	if(0 == *d_bilinear_map){
		if(false == checkStatus( cudaMalloc((void**) d_bilinear_map,
								4 * s.width * s.height * sizeof(float)))){
			return false;
		}
	}

	if(false == checkStatus(
				cudaMemcpy(*d_bilinear_map, input_bilinear_map,
						4 * s.width * s.height * sizeof(float), cudaMemcpyHostToDevice))){
			cudaFree(*d_bilinear_map);
			*d_bilinear_map = 0;
			return false;
		}

	return true;
}


__global__
void undistortKernel(unsigned char *d_output, const unsigned char *d_input,
		const float* d_undistort_map,
		const unsigned int width, const unsigned int height) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width && x >= 0 && y < height && y >= 0) {

		int offsetx = int(d_undistort_map[(y * width + x)*2]);
		int offsety = int(d_undistort_map[(y * width + x)*2+1]);

		if(offsetx < width && offsetx >= 0 && offsety < height && offsety >= 0){
			d_output[(y*width+x)*3] = d_input[(offsety*width+offsetx)*3];
			d_output[(y*width+x)*3+1] = d_input[(offsety*width+offsetx)*3+1];
			d_output[(y*width+x)*3+2] = d_input[(offsety*width+offsetx)*3+2];
		}

	}
}

bool undistort(unsigned char *d_output, const unsigned char *d_input, const float* d_undistort_map,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y){
	if (0 == d_output || 0 == d_input || 0 == d_undistort_map){
			return false;
	}
	dim3 blockSize(block_x, block_y);
	dim3 gridSize((d_width + blockSize.x - 1) / blockSize.x,
			(d_height + blockSize.y - 1) / blockSize.y);
	undistortKernel<<< gridSize, blockSize>>>(d_output, d_input, d_undistort_map, d_width, d_height);
	return true;
}

__global__
void undistortBilinearKernel(unsigned char *d_output, const unsigned char *d_input,
		const float* d_undistort_map, const float* d_bilinear_map,
		const unsigned int width, const unsigned int height) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width && x >= 0 && y < height && y >= 0) {

		int offsetx = int(d_undistort_map[(y * width + x)*2]);
		int offsety = int(d_undistort_map[(y * width + x)*2+1]);

		if(offsetx < width -1 && offsetx >= 0 && offsety < height - 1 && offsety >= 0){
			float bi0 = d_bilinear_map[(y*width+x)*4];
			float bi1 = d_bilinear_map[(y*width+x)*4+1];
			float bi2 = d_bilinear_map[(y*width+x)*4+2];
			float bi3 = d_bilinear_map[(y*width+x)*4+3];

			d_output[(y*width+x)*3] =
					d_input[(offsety*width+offsetx)*3]*bi0+
							d_input[(offsety*width+offsetx+1)*3]*bi1+
							d_input[((offsety+1)*width+offsetx)*3]*bi2+
							d_input[((offsety+1)*width+offsetx+1)*3]*bi3;

			d_output[(y*width+x)*3+1] =
					d_input[(offsety*width+offsetx)*3+1]*bi0+
							d_input[(offsety*width+offsetx+1)*3+1]*bi1+
							d_input[((offsety+1)*width+offsetx)*3+1]*bi2+
							d_input[((offsety+1)*width+offsetx+1)*3+1]*bi3;
			d_output[(y*width+x)*3+2] =
					d_input[(offsety*width+offsetx)*3+2]*bi0+
							d_input[(offsety*width+offsetx+1)*3+2]*bi1+
							d_input[((offsety+1)*width+offsetx)*3+2]*bi2+
							d_input[((offsety+1)*width+offsetx+1)*3+2]*bi3;
		}else{
			if(offsetx == width -1 || offsety == height - 1){
				d_output[(y*width+x)*3] = d_input[(offsety*width+offsetx)*3];
				d_output[(y*width+x)*3+1] = d_input[(offsety*width+offsetx)*3+1];
				d_output[(y*width+x)*3+2] = d_input[(offsety*width+offsetx)*3+2];
			}
		}

	}
}


bool undistortBilinear(unsigned char *d_output, const unsigned char *d_input,
		const float* d_undistort_map, const float* d_bilinear_map,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y){
	if (0 == d_output || 0 == d_input || 0 == d_undistort_map || 0 == d_bilinear_map ){
			return false;
	}
	dim3 blockSize(block_x, block_y);
	dim3 gridSize((d_width + blockSize.x - 1) / blockSize.x,
			(d_height + blockSize.y - 1) / blockSize.y);
	undistortBilinearKernel<<< gridSize, blockSize>>>(d_output, d_input, d_undistort_map, d_bilinear_map, d_width, d_height);
	return true;
}


}

}

}

