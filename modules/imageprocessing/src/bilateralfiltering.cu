/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * bilateralfiltering.cu created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * bilateralfiltering.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/imageprocessing/bilateralfiltering.cuh"
#include "gedeon/core/maths.hpp"
#include "gedeon/core/core.cuh"

#include <cuda.h>
#include <cuda_runtime.h>

namespace gedeon {

namespace GPU {

namespace BilateralFiltering {

__global__
void bilateralFilterKernelGray(float* input, float* output,
		const float* domain_kernel, const int radius, const float range_coeff,
		const float range_divider, const unsigned int width,
		const unsigned int height) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width && x >= 0 && y < height && y >= 0) {
		float sum = 0.0f;
		float t = 0.0f;

		// Get the center gray value
		float center = input[y * width + x];

		for (int i = -radius; i <= radius; i++) {
			int posy = y + i;
			if (posy >= 0 && posy < height) {

				for (int j = -radius; j <= radius; j++) {
					int posx = x + j;
					if (posx >= 0 && posx < width) {

						float curPix = input[posy * width + posx];
						float eulen = (center - curPix) * (center - curPix)
								* range_divider;
						float g = domain_kernel[abs(i) * (radius + 1) + abs(j)]
								* range_coeff * __expf(eulen);
						t += g * curPix;
						sum += g;
					}
				}
			}
		}
		output[y * width + x] = t / sum;
	}

}

float* getDomainKernel(const float& space_coefficient,
		const unsigned int& radius) {
	int width = radius + 1;
	float *domain_kernel_local = new float[width * width];
	if(0 == domain_kernel_local){
		return 0;
	}
	float space_divider = -0.5f / (space_coefficient * space_coefficient);
	float space_coeff = 1.0f / (sqrtf(2.0f * Maths::PI) * space_coefficient);

	/*
	 * Precompute the 2D spatial kernel
	 * Since the kernel contains the same values in every corners, we compute only one
	 */
	for (int i = 0; i < width; ++i) {
		int ii = i * width;
		for (int j = 0; j < width; ++j) {
			domain_kernel_local[ii + j] = space_coeff * exp((i * i + j * j) * space_divider);
		}
	}

	/*
	 * Allocate and copy the kernel on the device
	 */
	float *domain_kernel_device = 0;
	if(false == checkStatus(
			cudaMalloc((void**) &(domain_kernel_device),
					width * width * sizeof(float)))){
		delete[] domain_kernel_local;
		return 0;
	}
	if(false == checkStatus(
			cudaMemcpy(domain_kernel_device, domain_kernel_local,
					width * width * sizeof(float), cudaMemcpyHostToDevice))){
		cudaFree(domain_kernel_device);
		domain_kernel_device = 0;
		delete[] domain_kernel_local;
		return 0;
	}

	delete[] domain_kernel_local;

	return domain_kernel_device;

}

bool bilateralFilterGray(float *d_output, float *d_input,
		const int& radius, const float& space_coefficient,
		const float& range_coefficient, const unsigned int& d_width,
		const unsigned int& d_height, const unsigned int& block_x,
		const unsigned int& block_y) {

	float *domain_kernel_device = getDomainKernel(space_coefficient, radius);
	if (0 == domain_kernel_device) {
		return false;
	}
	float range_divider = -0.5f / (range_coefficient * range_coefficient);
	float range_coeff = 1.0f / (sqrtf(2.0f * Maths::PI) * range_coefficient);
	dim3 blockSize(block_x, block_y);
	dim3 gridSize((d_width + blockSize.x - 1) / blockSize.x,
			(d_height + blockSize.y - 1) / blockSize.y);
bilateralFilterKernelGray<<< gridSize, blockSize>>>(d_input, d_output, domain_kernel_device, radius, range_coeff, range_divider, d_width, d_height);
	return true;
}

}

}

}

