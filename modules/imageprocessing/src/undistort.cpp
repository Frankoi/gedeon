/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * undistort.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * undistort.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/imageprocessing/undistort.hpp"

namespace gedeon {

namespace Undistort {

boost::shared_array<float> computeDistortionMap(const float distortion_coeffs[5],
		const float intrinsic[9], const Size& s) {

	boost::shared_array<float> map_ptr(new float[s.width * s.height * 2]);

	float * map = map_ptr.get();

	for (unsigned int i = 0; i < s.height; ++i) {
		for (unsigned int j = 0; j < s.width; ++j) {
			float xxc = (static_cast<float>(j) - intrinsic[2]) / intrinsic[0];
			float yyc = (static_cast<float>(i) - intrinsic[5]) / intrinsic[4];
			float rr = xxc * xxc + yyc * yyc;
			float L = 1.0f + distortion_coeffs[0] * rr
					+ distortion_coeffs[1] * rr * rr
					+ distortion_coeffs[4] * rr * rr * rr;
			float x = L * xxc + 2.0f * distortion_coeffs[2] * xxc * yyc
					+ distortion_coeffs[3] * (rr + 2.0f * xxc * xxc);
			float y = L * yyc + 2.0f * distortion_coeffs[3] * xxc * yyc
					+ distortion_coeffs[2] * (rr + 2.0f * yyc * yyc);
			map[(i * s.width + j) * 2] = x * intrinsic[0] + intrinsic[2];
			map[(i * s.width + j) * 2 + 1] = y * intrinsic[4] + intrinsic[5];
		}
	}

	return map_ptr;
}

boost::shared_array<float> computeBilinearInterpolation(const float *map, const Size& s) {

	boost::shared_array<float> bimap_ptr(new float[s.width * s.height * 4]);
	float *bimap = bimap_ptr.get();

	for (unsigned int i = 0; i < s.height; ++i) {
		for (unsigned int j = 0; j < s.width; ++j) {
			float x = map[(i * s.width + j) * 2]
					- std::floor(map[(i * s.width + j) * 2]);
			float y = map[(i * s.width + j) * 2 + 1]
					- std::floor(map[(i * s.width + j) * 2 + 1]);
			bimap[(i * s.width + j) * 4] = (1.0f - x) * (1.0f - y);
			bimap[(i * s.width + j) * 4 + 1] = x * (1.0f - y);
			bimap[(i * s.width + j) * 4 + 2] = (1.0f - x) * y;
			bimap[(i * s.width + j) * 4 + 3] = x * y;
		}
	}

	return bimap_ptr;
}

bool undistort(RGBImage& output_img, const RGBImage& input_img,
		const boost::shared_array<float>& undistort_map) {

	if(0==input_img.getData().get() || 0 == undistort_map.get()){
		return false;
	}

	Size s = input_img.getSize();
	if(s != output_img.getSize()){
		output_img.setSize(s);
	}
	unsigned char* output = output_img.getData().get();
	unsigned char* input = input_img.getData().get();

	for (unsigned int y = 0; y < s.height; ++y) {
		for (unsigned int x = 0; x < s.width; ++x) {

			unsigned int offsetx = static_cast<unsigned int>(undistort_map[(y
					* s.width + x) * 2]);
			unsigned int offsety = static_cast<unsigned int>(undistort_map[(y
					* s.width + x) * 2 + 1]);

			if (offsetx < s.width && offsety < s.height) {
				output[(y * s.width + x) * 3] = input[(offsety * s.width
						+ offsetx) * 3];
				output[(y * s.width + x) * 3 + 1] = input[(offsety * s.width
						+ offsetx) * 3 + 1];
				output[(y * s.width + x) * 3 + 2] = input[(offsety * s.width
						+ offsetx) * 3 + 2];
			}
		}
	}
	return true;
}

bool undistortBilinear(RGBImage& output_img, const RGBImage& input_img,
		const boost::shared_array<float>& undistort_map, const boost::shared_array<float>& bilinear_map) {

	if(0==input_img.getData().get() || 0 == undistort_map.get() || 0 == bilinear_map.get()){
		return false;
	}

	Size s = input_img.getSize();
	if(s != output_img.getSize()){
		output_img.setSize(s);
	}
	unsigned char* output = output_img.getData().get();
	unsigned char* input = input_img.getData().get();

	for (unsigned int y = 0; y < s.height; ++y) {
		for (unsigned int x = 0; x < s.width; ++x) {

			unsigned int offsetx = static_cast<unsigned int>(undistort_map[(y
					* s.width + x) * 2]);
			unsigned int offsety = static_cast<unsigned int>(undistort_map[(y
					* s.width + x) * 2 + 1]);

			if (offsetx < s.width - 1 && offsety < s.height - 1) {
				float bi0 = bilinear_map[(y * s.width + x) * 4];
				float bi1 = bilinear_map[(y * s.width + x) * 4 + 1];
				float bi2 = bilinear_map[(y * s.width + x) * 4 + 2];
				float bi3 = bilinear_map[(y * s.width + x) * 4 + 3];

				output[(y * s.width + x) * 3] = input[(offsety * s.width
						+ offsetx) * 3] * bi0
						+ input[(offsety * s.width + offsetx + 1) * 3] * bi1
						+ input[((offsety + 1) * s.width + offsetx) * 3] * bi2
						+ input[((offsety + 1) * s.width + offsetx + 1) * 3]
								* bi3;

				output[(y * s.width + x) * 3 + 1] = input[(offsety * s.width
						+ offsetx) * 3 + 1] * bi0
						+ input[(offsety * s.width + offsetx + 1) * 3 + 1] * bi1
						+ input[((offsety + 1) * s.width + offsetx) * 3 + 1]
								* bi2
						+ input[((offsety + 1) * s.width + offsetx + 1) * 3 + 1]
								* bi3;
				output[(y * s.width + x) * 3 + 2] = input[(offsety * s.width
						+ offsetx) * 3 + 2] * bi0
						+ input[(offsety * s.width + offsetx + 1) * 3 + 2] * bi1
						+ input[((offsety + 1) * s.width + offsetx) * 3 + 2]
								* bi2
						+ input[((offsety + 1) * s.width + offsetx + 1) * 3 + 2]
								* bi3;
			} else {
				if (offsetx == s.width - 1 || offsety == s.height - 1) {
					output[(y * s.width + x) * 3] = input[(offsety * s.width
							+ offsetx) * 3];
					output[(y * s.width + x) * 3 + 1] = input[(offsety * s.width
							+ offsetx) * 3 + 1];
					output[(y * s.width + x) * 3 + 2] = input[(offsety * s.width
							+ offsetx) * 3 + 2];
				}
			}

		}
	}
	return true;
}

}

}

