/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * medianfiltering.cpp created in 09 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * medianfiltering.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/imageprocessing/medianfiltering.hpp"

#include <iostream>

namespace gedeon {

namespace {

void insert_sort_element(float *kernel, const float& value,
		const unsigned int& nb_elements) {

	/*
	 * Binary search insertion
	 */
	int min = 0;
	int max = nb_elements - 1;

	int partition = 0;
	while (min <= max) {
		partition = (max - min) / 2 + min;
		if (value < kernel[partition]) {
			max = partition - 1;
		} else {
			if (value > kernel[partition]) {
				min = partition + 1;
			} else {
				break;
			}
		}
	}
	int pos = partition;
	if (value > kernel[pos]) {
		pos += 1;
	}

	/*
	 * Shift higher value elements
	 */
	for (int i = nb_elements; i > pos; --i) {
		kernel[i] = kernel[i - 1];
	}
	kernel[pos] = value;

}

}

namespace MedianFiltering {

bool apply(float *d_output, float *d_input, const unsigned int& radius,
		const unsigned int& width, const unsigned int& height,
		const unsigned int& channels) {

	if (channels == 0) {
		return false;
	}

	/*
	 * Allocate the memory for the kernel
	 */
	unsigned int size_kernel = radius * 2 + 1;
	float *kernel = new float[size_kernel * size_kernel * channels];
	unsigned int counter = 0;

	for (int i = 0; i < int(height); ++i) {
		for (int j = 0; j < int(width); ++j) {
			counter = 0;
			for (int k = i - int(radius); k <= i + int(radius); ++k) {
				for (int l = j - int(radius); l <= j + int(radius); ++l) {
					if (0 <= k && int(height) > k && 0 <= l && int(width) > l) {
						for (unsigned int c = 0; c < channels; ++c) {
							insert_sort_element(
									kernel + size_kernel * size_kernel * c,
									d_input[(k * width + l) * channels + c],
									counter);
						}
						++counter;
					}
				}
			}
			for (unsigned int c = 0; c < channels; ++c) {
				d_output[(i * width + j) * channels + c] = kernel[size_kernel
						* size_kernel * c + counter / 2];
			}
		}
	}
	delete kernel;
	return true;
}

}

}
