/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * undistort.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * undistort.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__UNDISTORT_HPP__
#define GEDEON_IMAGE_PROCESSING__UNDISTORT_HPP__

#include "gedeon/datatypes/datatypes.hpp"
#include <boost/shared_array.hpp>

namespace gedeon {

namespace Undistort {


/**
 * \brief Compute the undistortion map
 *
 * The size of the data is width x height x 2
 *
 * Description of the bilateral filter :
 *
 * \f$ x = \frac{(u - c_x)}{f_x}\f$
 *
 * \f$ y = \frac{(v - c_y)}{f_y}\f$
 *
 * \f$ r^2 = (u - c_x)^2 + (v - c_y)^2\f$
 *
 * \f$ x' = x(1+k_1r^2+k_2r^4+k_3r^6)+2p_1xy+p_2(r^2+2x^2)\f$
 *
 * \f$ y' = y(1+k_1r^2+k_2r^4+k_3r^6)+2p_2xy+p_1(r^2+2y^2)\f$
 *
 * \f$ u' = x'f_x+c_x\f$
 *
 * \f$ v' = y'f_y+c_y\f$
 *
 * \param distortion_coeffs Distortion coefficients given as \f$(k_1,k_2,p_1,p_2,k_3)\f$
 * \param intrinsic Instrinsic parameters of the camera
 * \param s The resolution of the distortion map
 *
 * \return A smart pointer on the distortion map (Need to be deleted by the user)
 *
 * \author Francois de Sorbier
 */
boost::shared_array<float> computeDistortionMap(const float distortion_coeffs[5],
		const float intrinsic[9], const Size& s);

/**
 * \brief Compute the bilinear interpolation map
 *
 * The size of the data is width x height x 4
 *
 * Description of the bilinear interpolation :
 *
 * \f$f(R_1) \approx \frac{x_2-x}{x_2-x_1} f(Q_{11}) + \frac{x-x_1}{x_2-x_1} f(Q_{21})\f$
 *
 * \f$f(R_2) \approx \frac{x_2-x}{x_2-x_1} f(Q_{12}) + \frac{x-x_1}{x_2-x_1} f(Q_{22})\f$
 *
 * \f$f(P) \approx \frac{y_2-y}{y_2-y_1} f(R_1) + \frac{y-y_1}{y_2-y_1} f(R_2)\f$
 *
 * \param distortion_map The distortion map
 * \param s The resolution of the distortion map
 *
 * \sa computeDistortionMap
 *
 * \return A smart pointer on the distortion map (Need to be deleted by the user)
 *
 * \author Francois de Sorbier
 */
boost::shared_array<float> computeBilinearInterpolation(const float *distortion_map, const Size& s);


/**
 * \brief Apply the undistortion map
 *
 * \param output The undistorted input
 * \param input The input RGB image that will be undistorted
 * \param undistort_map The map for the undistortion
 *
 * The undistort map should be of the same size than the input image
 *
 * \sa computeDistortionMap
 * \sa undistortBilinear
 *
 * \return A pointer on the distortion map (Need to be deleted by the user)
 *
 * \author Francois de Sorbier
 */
bool undistort(RGBImage& output, const RGBImage& input,
		const boost::shared_array<float>& undistort_map);

/**
 * \brief Apply the undistortion map with bilinear interpolation
 *
 * \param output The undistorted input
 * \param input The input RGB image that will be undistorted
 * \param undistort_map The map for the undistortion
 * \param bilinear_map The map for the bilinear interpolation
 *
 * The undistort map should be of the same size than the input image
 *
 * \sa computeDistortionMap
 * \sa computeBilinearInterpolation
 * \sa undistort
 *
 * \return A pointer on the distortion map (Need to be deleted by the user)
 *
 * \author Francois de Sorbier
 */
bool undistortBilinear(RGBImage& output, const RGBImage& input,
		const boost::shared_array<float>& undistort_map, const boost::shared_array<float>& bilinear_map);

}

}

#endif
