/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * bilateralfiltering.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * bilateralfiltering.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__BILATERAL_FILTERING_CUH__
#define GEDEON_IMAGE_PROCESSING__BILATERAL_FILTERING_CUH__

namespace gedeon {

namespace GPU {

namespace BilateralFiltering {

/**
 * \brief Apply the bilateral filter kernel on GPU.
 *
 * Description of the bilateral filter :
 *
 * \f$I_p = \frac{1}{W_p} \sum_{q \in S}{G_{\sigma_s}(\|p-q\|)G_{\sigma_r}(|I_p-I_q|)I_q}\f$
 *
 * with \f$W_p = \sum_{q \in S}{G_{\sigma_s}(\|p-q\|)G_{\sigma_r}(|I_p-I_q|)}\f$
 *
 * \f$G_{\sigma}(x) = \frac{1}{\sigma \sqrt{2\pi}}\exp({{-}{x^2}\over{2\sigma^2}})\f$
 *
 * \f$G_{\sigma_s}\f$ is named the space weight (distance from center) and \f$G_{\sigma_r}\f$ is the range weight (difference of intensity)
 *
 * \param[out] d_output the result of the filter on the device
 * \param d_input the input grayscale image already allocated on the device
 * \param radius The radius of the filter
 * \param space_weight the standard deviation parameter of the Gaussian in the spatial part
 * \param range_weight the standard deviation parameter of the Gaussian in the range part
 * \param width the width of the image
 * \param height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 *
 * \author Francois de Sorbier
 */
bool bilateralFilterGray(float *d_output, float *d_input,
		const int& radius, const float& space_weight, const float& range_weight,
		const unsigned int& width, const unsigned int& height,
		const unsigned int& block_x, const unsigned int& block_y);


}

}

}

#endif
