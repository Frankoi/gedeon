/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * imageprocessing.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * imageprocessing.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__IMAGEPROCESSING_CUH__
#ifndef GEDEON_IMAGE_PROCESSING__IMAGEPROCESSING_CUH__

#include "gedeon/imageprocessing/bilateralfiltering.cuh"
#include "gedeon/imageprocessing/undistort.cuh"

#endif

