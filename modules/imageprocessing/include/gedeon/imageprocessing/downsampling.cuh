/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * downsampling.cuh created in 08 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * downsampling.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_IMAGE_PROCESSING__DOWNSAMPLING_CUH__
#define GEDEON_IMAGE_PROCESSING__DOWNSAMPLING_CUH__

namespace gedeon {

namespace GPU {

namespace DownSample {

/**
 * \brief Perform a downsampling (width/2,height/2) of the input image
 *
 * \param d_output The down sampled input on the device
 * \param d_input The input RGB image that will be down sampled on the device
 * \param d_width the width of the image
 * \param d_height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 *
 * The undistort map should be of the same size than the input image
 *
 * \sa computeDistortionMap
 * \sa computeBilinearInterpolation
 * \sa undistort
 *
 * \return true if everything went right, false otherwise
 *
 * \author Francois de Sorbier
 */
bool apply(float *d_output, const float *d_input,
		const unsigned int& d_width, const unsigned int& d_height,
		const unsigned int& block_x, const unsigned int& block_y);

}

}

}

#endif
