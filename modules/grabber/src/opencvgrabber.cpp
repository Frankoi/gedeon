/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opencvgrabber.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opencvgrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/opencvdriver.hpp"
#include "gedeon/grabber/opencvgrabber.hpp"

namespace gedeon {

class OpenCVGrabberParams {
public:
	OpenCVGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~OpenCVGrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;

};

class OpenCVGrabberIO {
public:
	OpenCVGrabberIO(void) :
			local_streaming(false), stream_filename("opencv-output.avi"), codec(
					-1) {
	}

	~OpenCVGrabberIO(void) {
		recorder.release();
	}

	bool local_streaming;
	std::string stream_filename;
	std::string source_filename;
	int codec;

	cv::VideoWriter recorder;
};

class OpenCVGrabberCapture {
public:
	OpenCVGrabberCapture(void) :
			devicename(""), device_id(0) {
	}
	~OpenCVGrabberCapture(void) {
		this->vc.release();
	}
	cv::VideoCapture vc;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

};

class OpenCVGrabberThread {
public:
	OpenCVGrabberThread(void) :
			pthread(0) {
	}
	~OpenCVGrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

OpenCVGrabber::OpenCVGrabber(void) :
		Grabber(), capturepimpl(new OpenCVGrabberCapture()), paramspimpl(
				new OpenCVGrabberParams()), threadpimpl(
				new OpenCVGrabberThread()), iopimpl(new OpenCVGrabberIO()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool OpenCVGrabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("OpenCVGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->capturepimpl->device_id = id;
	bool created = false;

	OpenCVDriver *driver = 0;

	try {
		driver = dynamic_cast<OpenCVDriver *>(d);
	} catch (const std::bad_cast& e) {
		Log::add().error("OpenCVGrabber::init", "The driver is not compatible");
		return false;
	}

	if (0 != driver->getCount() && id < driver->getCount()) {

		this->capturepimpl->devicename = driver->populate()[id];

		this->capturepimpl->vc.open(id);
		this->capturepimpl->rgbdi.reset(new RGBDIImage);

		if (true == this->capturepimpl->vc.isOpened()) {
			millisecSleep(250);
			if (true == this->capturepimpl->vc.grab()) {
				int width = static_cast<int>(this->capturepimpl->vc.get(
						CV_CAP_PROP_FRAME_WIDTH));
				int height = static_cast<int>(this->capturepimpl->vc.get(
						CV_CAP_PROP_FRAME_HEIGHT));
				this->capturepimpl->rgbdi.get()->color.setSize(
						Size(width, height));

				this->paramspimpl->hascolor = true;
				created = true;
			}
			this->paramspimpl->framerate = this->capturepimpl->vc.get(
					CV_CAP_PROP_FPS);
			if (-1 == this->paramspimpl->framerate
					|| 0 == this->paramspimpl->framerate) {
				this->paramspimpl->framerate = 30.0f;
			}

			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;

			millisecSleep(250);
			this->capturepimpl->vc.release();
		}
		if (false == created) {
			Log::add().error("OpenCVGrabber::init",
					"Failed finding the opencv device "
							+ IO::numberToString(id));
			return false;
		}

		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->paramspimpl->initialized = true;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("OpenCVGrabber::init", "No device found");
	return false;
}

bool OpenCVGrabber::init(const std::string& source, Driver* d) {
	if (true == this->paramspimpl->initialized) {
		Log::add().warning("DepthSenseGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->paramspimpl->hascolor = false;
	this->paramspimpl->hasdepth = false;
	this->paramspimpl->hasintensity = false;

	this->capturepimpl->vc.open(source);
	if (true == this->capturepimpl->vc.isOpened()) {
		millisecSleep(250);
		if (true == this->capturepimpl->vc.grab()) {
			cv::Mat img;
			this->capturepimpl->vc >> img;

			this->capturepimpl->rgbdi.reset(new RGBDIImage);
			int width = static_cast<int>(this->capturepimpl->vc.get(
					CV_CAP_PROP_FRAME_WIDTH));
			int height = static_cast<int>(this->capturepimpl->vc.get(
					CV_CAP_PROP_FRAME_HEIGHT));
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
			this->paramspimpl->framerate = this->capturepimpl->vc.get(
					CV_CAP_PROP_FPS);
			this->paramspimpl->hascolor = true;
			this->iopimpl->source_filename = source;
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		}
	} else {
		Log::add().error("OpenCVGrabber::init",
				"Failed to open the file " + source);
		return false;
	}

	this->iopimpl->local_streaming = true;

	this->capturepimpl->devicename = std::string("File (" + source + ")");

	this->threadpimpl->updated->sender = this;
	this->threadpimpl->ready->sender = this;
	this->paramspimpl->initialized = true;
	emitEvent(this->threadpimpl->ready);

	return true;
}

OpenCVGrabber::~OpenCVGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
}

std::string OpenCVGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int OpenCVGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& OpenCVGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool OpenCVGrabber::getParameter(const GrabberParameter& param, float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (OPENCV_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}

	return false;
}

bool OpenCVGrabber::getParameter(const GrabberParameter& param, float* values, unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool OpenCVGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (OPENCV_FRAMERATE == param) {
		return setFramerate(value);
	}
	return false;
}

void OpenCVGrabber::setCodecFourCC(const char& c1, const char& c2,
		const char& c3, const char& c4) {
	this->iopimpl->codec = CV_FOURCC(c1, c2, c3, c4);
}

bool OpenCVGrabber::setFramerate(const double& fr) {
	bool r = this->paramspimpl->running;
	if (true == r) {
		stop();
	}
	bool ok = this->capturepimpl->vc.set(CV_CAP_PROP_FPS, fr);
	if (ok == true) {
		Log::add().error("OpenCVGrabber::setResolution",
				"Unable to change the resolution");
		this->paramspimpl->framerate = fr;
	} else {
		double frtmp = this->capturepimpl->vc.get(CV_CAP_PROP_FPS);
		this->paramspimpl->framerate = frtmp;
	}
	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
	return ok;
}

bool OpenCVGrabber::setResolution(const unsigned int& w,
		const unsigned int& h) {
	bool r = this->paramspimpl->running;
	if (true == r) {
		stop();
	}
	bool ok = this->capturepimpl->vc.set(CV_CAP_PROP_FRAME_WIDTH, w);
	ok &= this->capturepimpl->vc.set(CV_CAP_PROP_FRAME_HEIGHT, h);
	int pwidth = static_cast<int>(this->capturepimpl->vc.get(
			CV_CAP_PROP_FRAME_WIDTH));
	int pheight = static_cast<int>(this->capturepimpl->vc.get(
			CV_CAP_PROP_FRAME_HEIGHT));
	if (ok == false || (pwidth == int(w) && pheight == int(h)) || (0 == pwidth)
			|| (0 == pheight)) {
		Log::add().error("OpenCVGrabber::setResolution",
				"Unable to change the resolution");
		this->capturepimpl->vc.set(CV_CAP_PROP_FRAME_WIDTH,
				this->capturepimpl->rgbdi.get()->color.getSize().width);
		this->capturepimpl->vc.set(CV_CAP_PROP_FRAME_HEIGHT,
				this->capturepimpl->rgbdi.get()->color.getSize().height);
		ok = false;
	} else {
		this->capturepimpl->rgbdi.get()->color.setSize(Size(pwidth, pheight));
		OpenCVGrabber::setFramerate(
				this->capturepimpl->vc.get(CV_CAP_PROP_FPS));
	}
	if (true == r) {
		play();
	}
	return ok;
}

bool OpenCVGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (OPENCV_INTERNAL_RECORD == trig) {
		if (this->iopimpl->recorder.isOpened() == false) {
			if (false
					== this->iopimpl->recorder.open(
							this->iopimpl->stream_filename,
							this->iopimpl->codec,
							this->paramspimpl->adaptative_framerate,
							cv::Size(
									this->capturepimpl->rgbdi.get()->color.getSize().width,
									this->capturepimpl->rgbdi.get()->color.getSize().height))) {
				Log::add().error("OpenCVGrabber::trigger",
						"Unable to start the capture");
				return false;
			}
		} else {
			this->iopimpl->recorder.release();
		}
		return true;
	}

	if (this->iopimpl->local_streaming == false) {
		if (OPENCV_QQVGA == trig) {
			return setResolution(160, 120);
		}
		if (OPENCV_QVGA == trig) {
			return setResolution(320, 240);
		}
		if (OPENCV_VGA == trig) {
			return setResolution(640, 480);
		}
		if (OPENCV_SVGA == trig) {
			return setResolution(800, 600);
		}
		if (OPENCV_XGA == trig) {
			return setResolution(1024, 768);
		}
		if (OPENCV_SXGA == trig) {
			return setResolution(1280, 1024);
		}
		if (OPENCV_HD == trig) {
			return setResolution(1280, 720);
		}
		if (OPENCV_FHD == trig) {
			return setResolution(1920, 1080);
		}
		if (OPENCV_DV == trig) {
			return setResolution(720, 576);
		}
	}
	return false;
}

bool OpenCVGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool OpenCVGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool OpenCVGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool OpenCVGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void OpenCVGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if(true == this->paramspimpl->paused){
		emitEvent(this->threadpimpl->paused);
	}
}

void OpenCVGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&OpenCVGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void OpenCVGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		this->iopimpl->recorder.release();
		emitEvent(this->threadpimpl->stopped);
	}
}

void OpenCVGrabber::grabOneFrame(void){
	Log::add().warning("OpenCVGrabber::grabOneFrame", "Not available");
}

void OpenCVGrabber::run(void) {

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;
	if (this->iopimpl->local_streaming == false) {
		this->capturepimpl->vc.open(this->capturepimpl->device_id);
	} else {
		this->capturepimpl->vc.open(this->iopimpl->source_filename);
	}

	if (false == this->capturepimpl->vc.isOpened()) {
		Log::add().error("OpenCVGrabber::run",
				"Unable to open the stream " + this->iopimpl->source_filename);
		return;
	}

	cv::Mat img;

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();
		{

			if (false == this->paramspimpl->paused) {
				this->capturepimpl->vc >> img;
				if (img.empty() == false) {
					if (true == this->paramspimpl->hascolor) {
						boost::mutex::scoped_lock l(this->threadpimpl->mutex);
						int local_size =
								this->capturepimpl->rgbdi.get()->color.getSize().width
										* this->capturepimpl->rgbdi.get()->color.getSize().height;
						unsigned char *color =
								this->capturepimpl->rgbdi.get()->color.getData().get();
						for (int i = 0; i < local_size; ++i) {
							color[i * 3] = img.data[i * 3 + 2];
							color[i * 3 + 1] = img.data[i * 3 + 1];
							color[i * 3 + 2] = img.data[i * 3];
						}
					}
				}
				if (this->iopimpl->source_filename.empty() == false) {
					int nb_frames = this->capturepimpl->vc.get(
							CV_CAP_PROP_FRAME_COUNT);
					int current_frame = this->capturepimpl->vc.get(
							CV_CAP_PROP_POS_FRAMES);
					if (current_frame >= nb_frames) {
						this->capturepimpl->vc.set(CV_CAP_PROP_POS_FRAMES, 2);
					}
				}

				if (true == this->iopimpl->recorder.isOpened()) {
					if (img.empty() == false) {
						this->iopimpl->recorder.write(img);
					}
				}

				/*
				 * Emit a message to confirm the update
				 */
				emitEvent(this->threadpimpl->updated);
			}
		}

		current_time = (1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}
	timer.stop();

	this->capturepimpl->vc.release();

	this->paramspimpl->finished = true;

}

void OpenCVGrabber::setStreamFilename(const std::string& filename) {
	this->iopimpl->stream_filename = filename;
}

std::string OpenCVGrabber::getStreamFilename(void) const {
	return this->iopimpl->stream_filename;
}

}
