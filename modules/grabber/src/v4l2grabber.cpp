/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * v4l2grabber.cpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * v4l2grabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "gedeon/grabber/v4l2driver.hpp"
#include "gedeon/grabber/v4l2grabber.hpp"
#include "gedeon/datatypes/io.hpp"

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <libv4l2.h>
#include <sys/mman.h>
#include <jpeglib.h>

namespace gedeon {

#define MAX_BUFFERS 4

class V4L2GrabberParams {
public:
	V4L2GrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~V4L2GrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;
};

class V4L2GrabberThread {
public:
	V4L2GrabberThread(void) :
			pthread(0) {
	}
	~V4L2GrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;
};

enum grabMethod {
	IO_READ, IO_MMAP
};

enum encodingMethod {
	FMT_YUYV, FMT_MJPEG,
};

class V4L2GrabberCapture {
public:
	V4L2GrabberCapture(void) :
			n_buffers(MAX_BUFFERS), buff_length(0), buff_offset(0), mem(0), fd(
					-1), method(IO_MMAP), encoding(FMT_YUYV), devicename(""), device_id(
					0) {
	}
	~V4L2GrabberCapture(void) {
	}
	unsigned int n_buffers;
	unsigned int *buff_length;
	unsigned int *buff_offset;
	void **mem;
	int fd;
	int method;
	int encoding;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;
};

static boolean mem_fill_input_buffer(j_decompress_ptr cinfo);
static void mem_skip_input_data(j_decompress_ptr cinfo, long num_bytes);
static void mem_init_source(j_decompress_ptr cinfo);
static void mem_term_source(j_decompress_ptr cinfo);
static void jpeg_mem_src(j_decompress_ptr cinfo, jpeg_source_mgr* src,
		void* buffer, long nbytes);

V4L2Grabber::V4L2Grabber(void) :
		Grabber(), capturepimpl(new V4L2GrabberCapture()), paramspimpl(
				new V4L2GrabberParams()), threadpimpl(new V4L2GrabberThread()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool V4L2Grabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("V4L2Grabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->capturepimpl->device_id = id;
	bool created = false;

	V4L2Driver *driver = 0;

	try {
		driver = dynamic_cast<V4L2Driver *>(d);
	} catch (const std::bad_cast& e) {
		Log::add().error("V4L2Grabber::init", "The driver is not compatible");
		return false;
	}

	if (0 != driver->getCount() && id < driver->getCount()) {

		this->capturepimpl->devicename = driver->populate()[id];
		struct stat st;
		if (-1 == stat(driver->getDeviceName(id), &st)) {
			Log::add().error("V4L2Grabber::init", "Unable to open the device");
			return false;
		}
		if (!S_ISCHR(st.st_mode)) {
			Log::add().error("V4L2Grabber::init", "Unable to open the device");
			return false;
		}

		this->capturepimpl->fd = open(driver->getDeviceName(id),
				O_RDWR | O_NONBLOCK, 0);
		if (-1 == this->capturepimpl->fd) {
			Log::add().error("V4L2Grabber::init", "Unable to open the device");
			return false;
		}

		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		fmt.fmt.pix.width = 640;
		fmt.fmt.pix.height = 480;
		fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
		this->capturepimpl->encoding = FMT_MJPEG;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_FMT, &fmt)) {
			fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
			this->capturepimpl->encoding = FMT_YUYV;
			if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_FMT, &fmt)) {
				Log::add().error("V4L2Grabber::init",
						std::string("Unable to set format. ")
								+ strerror(errno));
				return false;
			}
		}
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_FMT, &fmt)) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to get format. ") + strerror(errno));
			return false;
		}
		if (false == setCaptureMode()) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to set the capture mode. "));
			return false;
		}

		int width = fmt.fmt.pix.width;
		int height = fmt.fmt.pix.height;

		this->capturepimpl->rgbdi.reset(new RGBDIImage);
		this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
		this->paramspimpl->hascolor = true;
		created = true;

		/*
		 * Setup framerate
		 */
		getFramerate(this->paramspimpl->framerate);
		if (-1 == this->paramspimpl->framerate
				|| 0 == this->paramspimpl->framerate) {
			this->paramspimpl->framerate = 30.0f;
		}
		this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
		if (false == created) {
			Log::add().error("V4L2Grabber::init",
					"Failed finding the v4l2 device " + IO::numberToString(id));
			return false;
		}

		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		this->paramspimpl->initialized = true;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("V4L2Grabber::init", "No device found");
	return false;
}

bool V4L2Grabber::init(const std::string& source, Driver* d) {

	Log::add().warning("V4L2Grabber::init", "Not internal format available");
	return false;
}

V4L2Grabber::~V4L2Grabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
	clearCaptureMode();
	if (-1 != this->capturepimpl->fd) {
		v4l2_close(this->capturepimpl->fd);
	}
	delete capturepimpl;
	delete paramspimpl;
	delete threadpimpl;
}

std::string V4L2Grabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int V4L2Grabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& V4L2Grabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool V4L2Grabber::getParameter(const GrabberParameter& param, float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (V4L2_EXPOSURE == param) {
		getControl(V4L2_CID_EXPOSURE_ABSOLUTE, value);
	}
	if (V4L2_BRIGHTNESS == param) {
		getControl(V4L2_CID_BRIGHTNESS, value);
	}
	if (V4L2_SHARPNESS == param) {
		getControl(V4L2_CID_SHARPNESS, value);
	}
	if (V4L2_SATURATION == param) {
		getControl(V4L2_CID_SATURATION, value);
	}
	if (V4L2_CONTRAST == param) {
		getControl(V4L2_CID_CONTRAST, value);
	}
	if (V4L2_FOCUS == param) {
		getControl(V4L2_CID_FOCUS_ABSOLUTE, value);
	}
	if (V4L2_GAIN == param) {
		getControl(V4L2_CID_GAIN, value);
	}
	if (V4L2_GAMMA == param) {
		getControl(V4L2_CID_GAMMA, value);
	}
	if (V4L2_HUE == param) {
		getControl(V4L2_CID_HUE, value);
	}
	if (V4L2_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}
	return false;
}

bool V4L2Grabber::getParameter(const GrabberParameter& param, float* values, unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool V4L2Grabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (V4L2_EXPOSURE == param) {
		setControl(V4L2_CID_EXPOSURE_ABSOLUTE, value);
	}
	if (V4L2_BRIGHTNESS == param) {
		setControl(V4L2_CID_BRIGHTNESS, value);
	}
	if (V4L2_SHARPNESS == param) {
		setControl(V4L2_CID_SHARPNESS, value);
	}
	if (V4L2_SATURATION == param) {
		setControl(V4L2_CID_SATURATION, value);
	}
	if (V4L2_CONTRAST == param) {
		setControl(V4L2_CID_CONTRAST, value);
	}
	if (V4L2_FOCUS == param) {
		setControl(V4L2_CID_FOCUS_ABSOLUTE, value);
	}
	if (V4L2_GAIN == param) {
		setControl(V4L2_CID_GAIN, value);
	}
	if (V4L2_GAMMA == param) {
		setControl(V4L2_CID_GAMMA, value);
	}
	if (V4L2_HUE == param) {
		setControl(V4L2_CID_HUE, value);
	}

	return false;
}

bool V4L2Grabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}
	if (V4L2_QQVGA == trig) {
		return setResolution(160, 120);
	}
	if (V4L2_QVGA == trig) {
		return setResolution(320, 240);
	}
	if (V4L2_VGA == trig) {
		return setResolution(640, 480);
	}
	if (V4L2_SVGA == trig) {
		return setResolution(800, 600);
	}
	if (V4L2_XGA == trig) {
		return setResolution(1024, 768);
	}
	if (V4L2_SXGA == trig) {
		return setResolution(1280, 1024);
	}
	if (V4L2_HD == trig) {
		return setResolution(1280, 720);
	}
	if (V4L2_FHD == trig) {
		return setResolution(1920, 1080);
	}
	if (V4L2_DV == trig) {
		return setResolution(720, 576);
	}
	if (V4L2_EXPOSURE_AUTOMANUAL == trig) {
		return triggerControl(V4L2_CID_EXPOSURE_AUTO);
	}
	if (V4L2_WHITE_BALANCE_AUTOMANUAL == trig) {
		return triggerControl(V4L2_CID_AUTO_WHITE_BALANCE);
	}
	if (V4L2_HUE_AUTOMANUAL == trig) {
		return triggerControl(V4L2_CID_HUE_AUTO);
	}
	if (V4L2_FOCUS_AUTOMANUAL == trig) {
		return triggerControl(V4L2_CID_FOCUS_AUTO);
	}
	if (FLYCAPTURE_EXPOSURE_PRIORITY_AUTOMANUAL == trig) {
		return triggerControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY);
	}
	if (V4L2_5FPS == trig) {
		return setFramerate(5);
	}
	if (V4L2_7FPS == trig) {
		return setFramerate(15, 2);
	}
	if (V4L2_10FPS == trig) {
		return setFramerate(10);
	}
	if (V4L2_15FPS == trig) {
		return setFramerate(15);
	}
	if (V4L2_20FPS == trig) {
		return setFramerate(20);
	}
	if (V4L2_24FPS == trig) {
		return setFramerate(24);
	}
	if (V4L2_30FPS == trig) {
		return setFramerate(30);
	}
	if (V4L2_60FPS == trig) {
		return setFramerate(60);
	}
	if (V4L2_120FPS == trig) {
		return setFramerate(120);
	}

	return false;
}

bool V4L2Grabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool V4L2Grabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool V4L2Grabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool V4L2Grabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void V4L2Grabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if(true == this->paramspimpl->paused){
		emitEvent(this->threadpimpl->paused);
	}
}

void V4L2Grabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&V4L2Grabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void V4L2Grabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		emitEvent(this->threadpimpl->stopped);
	}
}

void V4L2Grabber::grabOneFrame(void){
	Log::add().warning("V4L2Grabber::grabOneFrame", "Not available");
}

void V4L2Grabber::run(void) {

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	struct v4l2_buffer buf;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	struct jpeg_source_mgr src_mem;
	JSAMPROW row_pointer[1];

	if (this->capturepimpl->method == IO_MMAP) {
		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_STREAMON, &type)) {
			Log::add().error("V4L2Grabber::run",
					std::string("Unable to start the stream. ")
							+ strerror(errno));
			return;
		}
		boost::this_thread::sleep(boost::posix_time::milliseconds(300));
	}

	if (FMT_MJPEG == this->capturepimpl->encoding) {
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_create_decompress(&cinfo);
	}

	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();

		if (false == this->paramspimpl->paused) {

			unsigned char *buf_tmp = 0;
			size_t buf_tmp_size = 0;

			if (this->capturepimpl->method == IO_READ) {

				boost::mutex::scoped_lock l(this->threadpimpl->mutex);
				if (-1
						== v4l2_read(this->capturepimpl->fd,
								this->capturepimpl->mem[0],
								this->capturepimpl->buff_length[0])) {
#ifdef DEBUG
					Log::add().error("V4L2Grabber::run",
							std::string("Unable to grab the image (READ). ")
							+ strerror(errno));
#endif
				}
				buf_tmp =
						reinterpret_cast<unsigned char*>(this->capturepimpl->mem[0]);
				buf_tmp_size = this->capturepimpl->buff_length[0];
			} else {
				memset(&buf, 0, sizeof(v4l2_buffer));
				buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
				buf.memory = V4L2_MEMORY_MMAP;
				while(-1
						== v4l2_ioctl(this->capturepimpl->fd, VIDIOC_DQBUF,
								&buf));/* {

#ifdef DEBUG
					Log::add().error("V4L2Grabber::run",
							std::string("Unable to dequeue buffer. ")
							+ strerror(errno));
#endif
				}*/
				buf_tmp =
						reinterpret_cast<unsigned char*>(this->capturepimpl->mem[buf.index]);
				buf_tmp_size = this->capturepimpl->buff_length[buf.index];

				while (-1
						== v4l2_ioctl(this->capturepimpl->fd, VIDIOC_QBUF,
								&buf));/* {
#ifdef DEBUG
					Log::add().error("V4L2Grabber::run",
							std::string("Unable to queue buffer. ")
							+ strerror(errno));
#endif
				}*/
			}

			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			unsigned int local_size =
					this->capturepimpl->rgbdi.get()->color.getSize().width
							* this->capturepimpl->rgbdi.get()->color.getSize().height;
			unsigned char *color =
					this->capturepimpl->rgbdi.get()->color.getData().get();

			if (FMT_YUYV == this->capturepimpl->encoding) {
				local_size /= 2;
				for (unsigned int i = 0; i < local_size; ++i) {
					Converter::yuv422ToRgb24(buf_tmp[i * 4], buf_tmp[i * 4 + 1],
							buf_tmp[i * 4 + 2], buf_tmp[i * 4 + 3],
							color[i * 6], color[i * 6 + 1], color[i * 6 + 2],
							color[i * 6 + 3], color[i * 6 + 4],
							color[i * 6 + 5]);
				}
			} else {
				jpeg_mem_src(&cinfo, &src_mem, (void*) buf_tmp, buf_tmp_size);
				jpeg_read_header(&cinfo, true);
				jpeg_start_decompress(&cinfo);
				row_pointer[0] = new unsigned char[cinfo.output_width
						* cinfo.num_components];
				unsigned long location = 0;
				while (cinfo.output_scanline < cinfo.image_height) {
					jpeg_read_scanlines(&cinfo, row_pointer, 1);
					memcpy(color + location, row_pointer[0],
							cinfo.image_width * cinfo.num_components);
					location += cinfo.image_width * cinfo.num_components;
				}
				jpeg_finish_decompress(&cinfo);
				delete[] row_pointer[0];
			}

		}

		/*
		 * Emit a message to confirm the update
		 */
		emitEvent(this->threadpimpl->updated);

		current_time = (1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	if (FMT_MJPEG == this->capturepimpl->encoding) {
		jpeg_destroy_decompress(&cinfo);
	}

	if (IO_MMAP == this->capturepimpl->method) {
		enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_STREAMOFF, &type)) {
			Log::add().error("V4L2Grabber::run",
					std::string("Unable to stop the stream. ")
							+ strerror(errno));
		}
	}

	timer.stop();
	this->paramspimpl->finished = true;

}

bool V4L2Grabber::requestBuffers(void) {
	struct v4l2_requestbuffers req;
	memset(&req, 0, sizeof(v4l2_requestbuffers));
	req.count = this->capturepimpl->n_buffers;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_REQBUFS, &req)) {
		Log::add().error("V4L2Grabber::requestBuffers",
				std::string("Unable to allocate buffers. ") + strerror(errno));
		return false;
	}
	return true;
}

bool V4L2Grabber::unrequestBuffers(void) {
	if (this->capturepimpl->fd != -1) {
		struct v4l2_requestbuffers req;
		memset(&req, 0, sizeof(v4l2_requestbuffers));
		req.count = 0;
		req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		req.memory = V4L2_MEMORY_MMAP;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_REQBUFS, &req)) {
			Log::add().error("V4L2Grabber::unrequestBuffers",
					std::string("Unable to allocate buffers. ")
							+ strerror(errno));
			return false;
		}
	}
	return true;
}

bool V4L2Grabber::mapBuffers(void) {
	this->capturepimpl->mem = new void*[this->capturepimpl->n_buffers];
	for (unsigned int i = 0; i < this->capturepimpl->n_buffers; i++) {
		this->capturepimpl->mem[i] = v4l2_mmap(
				NULL, // start anywhere
				this->capturepimpl->buff_length[i], PROT_READ | PROT_WRITE,
				MAP_SHARED, this->capturepimpl->fd,
				this->capturepimpl->buff_offset[i]);
		if (this->capturepimpl->mem[i] == MAP_FAILED
				|| this->capturepimpl->buff_length[i] == 0) {
			Log::add().error("V4L2Grabber::map_buffers",
					std::string("Unable to map a buffer. ") + strerror(errno));
			return false;
		}
	}
	return true;
}

bool V4L2Grabber::unmapBuffers(void) {
	for (unsigned int i = 0; i < this->capturepimpl->n_buffers; i++) {
		if (MAP_FAILED != this->capturepimpl->mem[i]
				&& this->capturepimpl->buff_length[i] > 0) {
			if (-1
					== v4l2_munmap(this->capturepimpl->mem[i],
							this->capturepimpl->buff_length[i])) {
				Log::add().error("V4L2Grabber::ummap_buffers",
						std::string("Unable to unmap a buffer. ")
								+ strerror(errno));
			}
		}
	}
	if (0 != this->capturepimpl->mem) {
		delete[] this->capturepimpl->mem;
		this->capturepimpl->mem = 0;
	}
	return true;
}

bool V4L2Grabber::queryBuffers(void) {
	if (this->capturepimpl->n_buffers < 2) {
		Log::add().error("V4L2Grabber::query_buffers",
				std::string("number of available buffers is too small"));
		return false;
	}
	this->capturepimpl->buff_length =
			new unsigned int[this->capturepimpl->n_buffers];
	this->capturepimpl->buff_offset =
			new unsigned int[this->capturepimpl->n_buffers];
	for (unsigned int i = 0; i < this->capturepimpl->n_buffers; ++i) {
		struct v4l2_buffer buf;
		memset(&buf, 0, sizeof(struct v4l2_buffer));
		buf.index = i;
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_QUERYBUF, &buf)) {
			Log::add().error("V4L2Grabber::query_buffers",
					std::string("Unable to query buffers. ") + strerror(errno));
			return false;
		}
		if (buf.length <= 0) {
			Log::add().error("V4L2Grabber::query_buffers",
					std::string("Incorrect buffer length"));
			return false;
		}
		this->capturepimpl->buff_length[i] = buf.length;
		this->capturepimpl->buff_offset[i] = buf.m.offset;
	}
	return true;
}

bool V4L2Grabber::unqueryBuffers(void) {
	if (0 != this->capturepimpl->buff_length) {
		delete[] this->capturepimpl->buff_length;
	}
	if (0 != this->capturepimpl->buff_offset) {
		delete[] this->capturepimpl->buff_offset;
	}
	return true;
}

bool V4L2Grabber::queueBuffers(void) {
	for (unsigned int i = 0; i < this->capturepimpl->n_buffers; ++i) {
		struct v4l2_buffer buf;
		memset(&buf, 0, sizeof(struct v4l2_buffer));
		buf.index = i;
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_QBUF, &buf)) {
			Log::add().error("V4L2Grabber::queue_buffers",
					std::string("Unable to queue buffers. ") + strerror(errno));
			return false;
		}
	}
	return true;
}

bool V4L2Grabber::getFramerate(float& f) {
	struct v4l2_streamparm my_parm;
	memset(&my_parm, 0, sizeof(v4l2_streamparm));
	my_parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_PARM, &my_parm)) {
		Log::add().error("V4L2Grabber::getFramerate",
				std::string("Unable to access stream parameters. ")
						+ strerror(errno));
		return false;
	}
	if (0 != (V4L2_CAP_TIMEPERFRAME & my_parm.parm.capture.capability)) {
		f = float(my_parm.parm.capture.timeperframe.denominator)
				/ my_parm.parm.capture.timeperframe.numerator;
	} else {
		Log::add().error("V4L2Grabber::getFramerate",
				std::string(
						"Unable to access stream parameters V4L2_CAP_TIMEPERFRAME. ")
						+ strerror(errno));
		return false;
	}
	return true;
}

bool V4L2Grabber::setFramerate(const int& f, const int& d) {
	bool r = this->paramspimpl->running;
	if (r) {
		stop();
	}
	struct v4l2_streamparm my_parm;
	memset(&my_parm, 0, sizeof(v4l2_streamparm));
	my_parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_PARM, &my_parm)) {
		Log::add().error("V4L2Grabber::setFramerate",
				std::string("Unable to access stream parameters. ")
						+ strerror(errno));
		if (r) {
			play();
		}
		return false;
	}
	if (0 != (V4L2_CAP_TIMEPERFRAME & my_parm.parm.capture.capability)) {

		clearCaptureMode();
		my_parm.parm.capture.timeperframe.numerator = d;
		my_parm.parm.capture.timeperframe.denominator = f;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_PARM, &my_parm)) {
			Log::add().error("V4L2Grabber::setFramerate",
					std::string("Unable to set stream parameters. ")
							+ strerror(errno));
			setCaptureMode();
			if (r) {
				play();
			}
			return false;
		}
		this->paramspimpl->framerate = float(f) / float(d);
		setCaptureMode();
	} else {
		Log::add().error("V4L2Grabber::setFramerate",
				std::string(
						"Unable to access stream parameters V4L2_CAP_TIMEPERFRAME. ")
						+ strerror(errno));

		if (r) {
			play();
		}
		return false;
	}
	if (r) {
		play();
	}
	this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
	return true;
}

bool V4L2Grabber::setResolution(const unsigned int& w, const unsigned int& h) {

	bool r = this->paramspimpl->running;
	if (r) {
		stop();
	}
	struct v4l2_format fmt;
	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_FMT, &fmt)) {
		Log::add().error("V4L2Grabber::init",
				std::string("Unable to get format. ") + strerror(errno));
		return false;
	}
	clearCaptureMode();
	fmt.fmt.pix.width = w;
	fmt.fmt.pix.height = h;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_FMT, &fmt)) {
		Log::add().error("V4L2Grabber::init",
				std::string("Unable to set format. ") + strerror(errno));
		setCaptureMode();
		if (r) {
			play();
		}
		return false;
	}
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_FMT, &fmt)) {
		Log::add().error("V4L2Grabber::init",
				std::string("Unable to get format. ") + strerror(errno));
		setCaptureMode();
		if (r) {
			play();
		}
		return false;
	}
	int width = fmt.fmt.pix.width;
	int height = fmt.fmt.pix.height;
	this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
	setCaptureMode();
	getFramerate(this->paramspimpl->framerate);
	if (r) {
		play();
	}
	return true;
}

bool V4L2Grabber::clearCaptureMode(void) {
	if (this->capturepimpl->fd != -1) {
		if (this->capturepimpl->method == 1) {
			unmapBuffers();
			unqueryBuffers();
			unrequestBuffers();
		} else {
			if (0 != this->capturepimpl->mem) {
				delete[] this->capturepimpl->mem;
				this->capturepimpl->mem = 0;
			}
			if (0 != this->capturepimpl->buff_length) {
				delete[] this->capturepimpl->buff_length;
				this->capturepimpl->buff_length = 0;
			}
			if (0 != this->capturepimpl->buff_offset) {
				delete[] this->capturepimpl->buff_offset;
				this->capturepimpl->buff_offset = 0;
			}
		}
	}
	return true;
}

bool V4L2Grabber::setCaptureMode(void) {
	struct v4l2_capability cap;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_QUERYCAP, &cap)) {
		Log::add().error("V4L2Grabber::setCaptureMode",
				std::string("Unable to access device capabilities ")
						+ strerror(errno));
		return false;
	}

	if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		Log::add().error("V4L2Grabber::setCaptureMode",
				std::string("Device is not a capture device"));
		return false;
	}

	if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_FMT, &fmt)) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to get format. ") + strerror(errno));
			return false;
		}
		this->capturepimpl->mem = new void*[1];
		this->capturepimpl->buff_length = new unsigned int[1];
		this->capturepimpl->buff_offset = new unsigned int[1];
		this->capturepimpl->buff_length[0] = fmt.fmt.pix.sizeimage;
		this->capturepimpl->buff_offset[0] = fmt.fmt.pix.sizeimage;
		this->capturepimpl->method = IO_READ;
	} else {
		this->capturepimpl->n_buffers = MAX_BUFFERS;
		if (false == requestBuffers()) {
			return false;
		}
		if (false == queryBuffers()) {
			unqueryBuffers();
			return false;
		}
		if (false == mapBuffers()) {
			unmapBuffers();
			unqueryBuffers();
			unrequestBuffers();
			return false;
		}
		if (false == queueBuffers()) {
			unmapBuffers();
			unqueryBuffers();
			unrequestBuffers();
			return false;
		}

		this->capturepimpl->method = IO_MMAP;

	}
	return true;
}

bool V4L2Grabber::triggerControl(const int& request) {
	struct v4l2_control control;
	memset(&control, 0, sizeof(v4l2_control));
	control.id = request;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_CTRL, &control)) {
		Log::add().error("V4L2Grabber::triggerControl",
				std::string("Unable to access control. ") + strerror(errno));
		return false;
	}

	if (request == V4L2_CID_EXPOSURE_AUTO) {
		if (V4L2_EXPOSURE_APERTURE_PRIORITY == control.value) {
			control.value = V4L2_EXPOSURE_MANUAL;
		} else {
			control.value = V4L2_EXPOSURE_APERTURE_PRIORITY;
		}
	} else {
		control.value = !control.value;
	}

	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_CTRL, &control)) {
		Log::add().error("V4L2Grabber::triggerControl",
				std::string("Unable to set control. ") + strerror(errno));
		return false;
	}
	return true;
}

bool V4L2Grabber::setControl(const int& request, const float& value) {
	struct v4l2_control control;
	memset(&control, 0, sizeof(v4l2_control));
	control.id = request;
	control.value = (int) value;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_S_CTRL, &control)) {
		Log::add().error("V4L2Grabber::setControl",
				std::string("Unable to set control. ") + strerror(errno));
		return false;
	}
	return true;
}

bool V4L2Grabber::getControl(const int& request, float& value) {
	struct v4l2_control control;
	memset(&control, 0, sizeof(v4l2_control));
	control.id = request;
	control.value = value;
	if (-1 == v4l2_ioctl(this->capturepimpl->fd, VIDIOC_G_CTRL, &control)) {
		Log::add().error("V4L2Grabber::getControl",
				std::string("Unable to get control. ") + strerror(errno));
		return false;
	}
	value = (float) control.value;
	return true;
}

void V4L2Grabber::setStreamFilename(const std::string& filename) {
}

std::string V4L2Grabber::getStreamFilename(void) const {
	return "";
}

/*
 * Libjpeg related stuffs
 */
static boolean mem_fill_input_buffer(j_decompress_ptr cinfo) {
#ifdef PROCESS_TRUNCATED_IMAGES
	jpeg_source_mgr* src = cinfo->src;

	static const JOCTET EOI_BUFFER[ 2 ] = {(JOCTET)0xFF, (JOCTET)JPEG_EOI};
	src->next_input_byte = EOI_BUFFER;
	src->bytes_in_buffer = sizeof( EOI_BUFFER );
#else
	return false;
#endif
	return true;
}
static void mem_skip_input_data(j_decompress_ptr cinfo, long num_bytes) {
	jpeg_source_mgr* src = (jpeg_source_mgr*) cinfo->src;

	if (1 > num_bytes)
		return;

	if (num_bytes < (long) src->bytes_in_buffer) {
		src->next_input_byte += (size_t) num_bytes;
		src->bytes_in_buffer -= (size_t) num_bytes;
	} else {
#ifdef PROCESS_TRUNCATED_IMAGES
		src->bytes_in_buffer = 0;
#else
		return;
#endif
	}
}
static void mem_init_source(j_decompress_ptr cinfo) {
}

static void mem_term_source(j_decompress_ptr cinfo) {
}
static void jpeg_mem_src(j_decompress_ptr cinfo, jpeg_source_mgr* src,
		void* buffer, long nbytes) {
	src->init_source = mem_init_source;
	src->fill_input_buffer = mem_fill_input_buffer;
	src->skip_input_data = mem_skip_input_data;
	src->resync_to_restart = jpeg_resync_to_restart;
	src->term_source = mem_term_source;
	src->bytes_in_buffer = nbytes;
	src->next_input_byte = (JOCTET*) buffer;
	cinfo->src = src;
}

}
