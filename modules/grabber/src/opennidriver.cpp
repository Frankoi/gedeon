/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opennidriver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opennidriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/opennidriver.hpp"
/*
 #if (XN_PLATFORM == XN_PLATFORM_LINUX_X86 || XN_PLATFORM == XN_PLATFORM_LINUX_ARM)
 #define UNIX
 #define GLX_GLXEXT_LEGACY
 #endif

 #if (XN_PLATFORM == XN_PLATFORM_MACOSX)
 #define MACOS
 #endif*/

#include <XnLog.h>
#include <XnTypes.h>

namespace gedeon {

OpenNIDriver OpenNIDriver::driver_singleton;

OpenNIDriver::OpenNIDriver(void) :
		initialized(false) {

	/*xnLogSetConsoleOutput(false);
	 xnLogSetFileOutput(true);
	 xnLogSetMaskMinSeverity("OpenNIDriver", XN_LOG_ERROR);
	 xnLogInitSystem();*/

}

OpenNIDriver::~OpenNIDriver(void) {

	this->names.clear();

}

bool OpenNIDriver::refresh(void) {

	this->initialized = false;

	XnStatus nRetVal = XN_STATUS_OK;
	nRetVal = this->context.Init();
	if (XN_STATUS_OK != nRetVal) {
		Log::add().error("OpenNIDriver::init", xnGetStatusString(nRetVal));
		return false;
	}

	/*
	 * Add the license key to the context
	 */
	XnLicense m_license;
	strcpy(m_license.strVendor, "PrimeSense");
	strcpy(m_license.strKey, "0KOIk2JeIBYClPWVnMoRKn5cdY4=");
	this->context.AddLicense(m_license);

	/*
	 * Get the list of the devices
	 */
	xn::NodeInfoList list;
	xn::EnumerationErrors errors;
	nRetVal = this->context.EnumerateProductionTrees(XN_NODE_TYPE_DEVICE, NULL,
			list, &errors);
	if (XN_STATUS_OK != nRetVal) {
		Log::add().error("OpenNIDriver::init", xnGetStatusString(nRetVal));
		return false;
	}

	/*
	 * Get the available OpenNI devices
	 */
	std::vector<std::string> local_names;
	for (xn::NodeInfoList::Iterator it = list.Begin(); it != list.End(); ++it) {
		xn::NodeInfo deviceNodeInfo = *it;
		const XnProductionNodeDescription& description =
				deviceNodeInfo.GetDescription();
		const XnChar * c = deviceNodeInfo.GetCreationInfo();
		std::string name(
				std::string(description.strVendor) + " "
						+ std::string(description.strName));
		if (c != 0) {
			name += std::string("[") + c + "]";
		}
		local_names.push_back(name);
	}

	for (std::vector<std::string>::iterator it = this->names.begin();
			it != this->names.end(); ++it) {
		// The device is no more connected, so we remove it
		if (local_names.end()
				== std::find(local_names.begin(), local_names.end(), *it)) {
			it = this->names.erase(it);
		}
	}
	for (std::vector<std::string>::iterator it = local_names.begin();
			it != local_names.end(); ++it) {
		// Add new devices
		if (this->names.end()
				== std::find(this->names.begin(), this->names.end(), *it)) {
			this->names.push_back(*it);
		}
	}

	local_names.clear();
	list.Clear();

	this->initialized = true;

	return true;
}

}
