/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * openni2grabber.cpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * openni2grabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/openni2driver.hpp"
#include "gedeon/grabber/openni2grabber.hpp"
#include <PS1080.h>

#include <boost/algorithm/string/predicate.hpp>

namespace gedeon {

class OpenNI2GrabberParams {
public:
	OpenNI2GrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE) {
	}
	~OpenNI2GrabberParams(void) {
	}
	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;

};

class OpenNI2GrabberIO {
public:
	OpenNI2GrabberIO(void) :
			local_streaming(false), stream_filename("openni2-output.oni") {
	}

	~OpenNI2GrabberIO(void) {
		recorder.stop();
		recorder.destroy();
	}

	bool local_streaming;
	std::string stream_filename;

	openni::Recorder recorder;
};

class OpenNI2GrabberCapture {
public:
	OpenNI2GrabberCapture(void) :
			device(0), devicename(""), device_id(0) {
	}
	~OpenNI2GrabberCapture(void) {
		if (ir.isValid())
			ir.destroy();
		if (color.isValid())
			color.destroy();
		if (depth.isValid())
			depth.destroy();
		/*if(device->isValid())
		 device->close();*/
		//delete device;
	}
	openni::Device *device;
	openni::VideoStream depth;
	openni::VideoStream color;
	openni::VideoStream ir;
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

};

class OpenNI2GrabberThread {
public:
	OpenNI2GrabberThread(void) :
			pthread(0) {
	}
	~OpenNI2GrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

class ColorCallback: public openni::VideoStream::NewFrameListener {
public:
	ColorCallback(OpenNI2GrabberCapture * c, OpenNI2GrabberThread * t,
			OpenNI2GrabberParams * p) :
			cap(c), thd(t), params(p) {
	}
	void onNewFrame(openni::VideoStream& stream) {

		boost::mutex::scoped_lock l(thd->mutex);
		stream.readFrame(&m_frame);
		if (false == params->paused) {
			const unsigned char * tmp =
					(const unsigned char*) m_frame.getData();
			cap->rgbdi.get()->color.setData(tmp);
		}
	}
private:
	openni::VideoFrameRef m_frame;
	OpenNI2GrabberCapture * cap;
	OpenNI2GrabberThread * thd;
	OpenNI2GrabberParams * params;
};

class DepthCallback: public openni::VideoStream::NewFrameListener {
public:
	DepthCallback(OpenNI2GrabberCapture * c, OpenNI2GrabberThread * t,
			OpenNI2GrabberParams * p) :
			cap(c), thd(t), params(p) {
	}
	void onNewFrame(openni::VideoStream& stream) {

		stream.readFrame(&m_frame);
		if (false == params->paused) {
			const openni::DepthPixel * ref_src =
					(const openni::DepthPixel*) m_frame.getData();
			unsigned int s = cap->rgbdi.get()->depth.getSize().height
					* cap->rgbdi.get()->depth.getSize().width;
			boost::mutex::scoped_lock l(thd->mutex);
			float* ref_dst = cap->rgbdi->depth.getData().get();
			for (unsigned int i = 0; i < s; ++i) {
				ref_dst[i] = static_cast<float>(ref_src[i] * 0.001f);
			}
		}
	}
private:
	openni::VideoFrameRef m_frame;
	OpenNI2GrabberCapture * cap;
	OpenNI2GrabberThread * thd;
	OpenNI2GrabberParams * params;
};

class IRCallback: public openni::VideoStream::NewFrameListener {
public:
	IRCallback(OpenNI2GrabberCapture * c, OpenNI2GrabberThread * t,
			OpenNI2GrabberParams * p) :
			cap(c), thd(t), params(p) {
	}
	void onNewFrame(openni::VideoStream& stream) {
		unsigned char* ref_dst = cap->rgbdi.get()->intensity.getData().get();
		unsigned int s = cap->rgbdi.get()->intensity.getSize().height
				* cap->rgbdi.get()->intensity.getSize().width;
		boost::mutex::scoped_lock l(thd->mutex);
		stream.readFrame(&m_frame);
		if (false == params->paused) {
			const openni::Grayscale16Pixel * ref_src =
					(const openni::Grayscale16Pixel*) m_frame.getData();
			for (unsigned int i = 0; i < s; ++i) {
				ref_dst[i] = static_cast<unsigned char>(ref_src[i]
						* 0.2491234275);
			}
		}
	}
private:
	openni::VideoFrameRef m_frame;
	OpenNI2GrabberCapture * cap;
	OpenNI2GrabberThread * thd;
	OpenNI2GrabberParams * params;
};

OpenNI2Grabber::OpenNI2Grabber(void) :
		Grabber(), capturepimpl(new OpenNI2GrabberCapture()), paramspimpl(
				new OpenNI2GrabberParams()), threadpimpl(
				new OpenNI2GrabberThread()), iopimpl(new OpenNI2GrabberIO()), color_callback(
				0), depth_callback(0), ir_callback(0) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool OpenNI2Grabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("OpenNI2Grabber::init",
				"The grabber is already initialized");
		return false;
	}

	openni::Status rc = openni::STATUS_OK;
	rc = openni::OpenNI::initialize();
	if (openni::STATUS_OK != rc) {
		Log::add().error("OpenNI2Grabber::init",
				std::string("Could not initialize openni ")
						+ openni::OpenNI::getExtendedError());
		return false;
	}

	/*
	 * Get the list of the devices
	 */
	openni::Array<openni::DeviceInfo> deviceList;
	openni::OpenNI::enumerateDevices(&deviceList);

	if (int(id) < deviceList.getSize()) {
		bool created = false;
		const openni::DeviceInfo& di = deviceList[id];
		this->capturepimpl->device = new openni::Device();
		rc = this->capturepimpl->device->open(di.getUri());
		if (openni::STATUS_OK != rc) {
			Log::add().error("OpenNI2Grabber::init",
					std::string("Could not open the device ")
							+ openni::OpenNI::getExtendedError());
			return false;
		}
		rc = this->capturepimpl->depth.create(*this->capturepimpl->device,
				openni::SENSOR_DEPTH);
		if (openni::STATUS_OK == rc) {
			this->paramspimpl->hasdepth = true;
		}

		this->capturepimpl->rgbdi.reset(new RGBDIImage);

		int width, height;
		if (true == this->paramspimpl->hasdepth) {
			openni::VideoMode vm = this->capturepimpl->depth.getVideoMode();
			vm = this->capturepimpl->depth.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));
			this->paramspimpl->framerate = vm.getFps();
			this->capturepimpl->depth.setMirroringEnabled(
					!this->capturepimpl->depth.getMirroringEnabled());
		}

		if (false == setColorMode()) {
			return false;
		}

		//this->capturepimpl->device->setDepthColorSyncEnabled(true);
		this->capturepimpl->device->setImageRegistrationMode(
				openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

		if (true == this->paramspimpl->hascolor) {
			this->paramspimpl->hasintensity = false;
		}

		created = true;

		if (created == false) {
			Log::add().error("OpenNI2Grabber::init",
					"Failed finding the openni2 device "
							+ IO::numberToString(id));
			return false;
		}
		this->paramspimpl->adaptative_framerate = this->paramspimpl->framerate;
		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		this->paramspimpl->initialized = true;
		emitEvent(this->threadpimpl->ready);

		this->color_callback = new ColorCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);
		this->depth_callback = new DepthCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);
		this->ir_callback = new IRCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);

		return true;

	}

	Log::add().error("OpenNI2Grabber::init", "No device found");
	return false;
}

bool OpenNI2Grabber::init(const std::string& source, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("OpenNI2Grabber::init",
				"The grabber is already initialized");
		return false;
	}

	this->paramspimpl->hascolor = false;
	this->paramspimpl->hasdepth = false;
	this->paramspimpl->hasintensity = false;

	this->capturepimpl->rgbdi.reset(new RGBDIImage);

	std::string namelower = source;
	std::transform(namelower.begin(), namelower.end(), namelower.begin(),
			::tolower);
	if (true == boost::algorithm::ends_with(namelower, ".oni")) {
		openni::Status rc = openni::STATUS_OK;

		this->capturepimpl->device = new openni::Device();
		rc = this->capturepimpl->device->open(source.c_str());
		if (openni::STATUS_OK != rc) {
			Log::add().error("OpenNI2Grabber::init",
					std::string("Could not open the device ")
							+ openni::OpenNI::getExtendedError());
			return false;
		}
		rc = this->capturepimpl->depth.create(*this->capturepimpl->device,
				openni::SENSOR_DEPTH);
		if (openni::STATUS_OK == rc) {
			this->paramspimpl->hasdepth = true;
			openni::VideoMode vm = this->capturepimpl->depth.getVideoMode();
			unsigned int width = vm.getResolutionX();
			unsigned int height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));
			this->paramspimpl->framerate = vm.getFps();
		}
		rc = this->capturepimpl->color.create(*this->capturepimpl->device,
				openni::SENSOR_COLOR);
		if (openni::STATUS_OK == rc) {
			setColorMode();
		}
		rc = this->capturepimpl->ir.create(*this->capturepimpl->device,
				openni::SENSOR_IR);
		if (openni::STATUS_OK == rc) {
			setIntensityMode();
		}
		this->capturepimpl->device->setImageRegistrationMode(
				openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

		this->color_callback = new ColorCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);
		this->depth_callback = new DepthCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);
		this->ir_callback = new IRCallback(this->capturepimpl,
				this->threadpimpl, this->paramspimpl);

	} else {
		Log::add().warning("OpenNI2Grabber::init", "Format not supported");
		return false;
	}

	this->iopimpl->local_streaming = true;

	this->capturepimpl->devicename = std::string("File (" + source + ")");

	this->threadpimpl->updated->sender = this;
	this->threadpimpl->ready->sender = this;
	this->threadpimpl->stopped->sender = this;
	this->threadpimpl->playing->sender = this;
	this->threadpimpl->paused->sender = this;
	this->paramspimpl->initialized = true;
	emitEvent(this->threadpimpl->ready);

	return true;
}

OpenNI2Grabber::~OpenNI2Grabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}
	if (this->color_callback) {
		delete this->color_callback;
	}
	if (this->depth_callback) {
		delete this->depth_callback;
	}
	if (this->ir_callback) {
		delete this->ir_callback;
	}
	delete paramspimpl;
	delete threadpimpl;
	delete iopimpl;
	delete capturepimpl;
}

std::string OpenNI2Grabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int OpenNI2Grabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& OpenNI2Grabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool OpenNI2Grabber::getParameter(const GrabberParameter& param, float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (OPENNI2_FRAMERATE == param) {
		value = this->paramspimpl->framerate;
		return true;
	}

	if (OPENNI2_FOCAL_LENGTH_X_COLOR == param) {
		if (false == this->paramspimpl->hascolor) {
			return false;
		}
		value =
				(float) this->capturepimpl->rgbdi.get()->color.getSize().width
						/ (2.0f
								* tan(
										this->capturepimpl->color.getHorizontalFieldOfView()
												/ 2.0f));
		return true;
	}
	if (OPENNI2_FOCAL_LENGTH_Y_COLOR == param) {
		if (false == this->paramspimpl->hascolor) {
			return false;
		}
		value =
				(float) this->capturepimpl->rgbdi.get()->color.getSize().height
						/ (2.0f
								* tan(
										this->capturepimpl->color.getVerticalFieldOfView()
												/ 2.0f));
		return true;
	}
	if (OPENNI2_FOCAL_LENGTH_X_DEPTH == param) {
		if (false == this->paramspimpl->hasdepth) {
			return false;
		}
		value =
				(float) this->capturepimpl->rgbdi.get()->depth.getSize().width
						/ (2.0f
								* tan(
										this->capturepimpl->depth.getHorizontalFieldOfView()
												/ 2.0f));
		return true;
	}
	if (OPENNI2_FOCAL_LENGTH_Y_DEPTH == param) {
		if (false == this->paramspimpl->hasdepth) {
			return false;
		}
		value =
				(float) this->capturepimpl->rgbdi.get()->depth.getSize().height
						/ (2.0f
								* tan(
										this->capturepimpl->depth.getVerticalFieldOfView()
												/ 2.0f));
		return true;
	}

	if (OPENNI2_PP_X_DEPTH == param) {
		if (false == this->paramspimpl->hasdepth) {
			return false;
		}
		value = (float) this->capturepimpl->rgbdi.get()->depth.getSize().width
				/ 2.0f;
		return true;
	}

	if (OPENNI2_PP_Y_DEPTH == param) {
		if (false == this->paramspimpl->hasdepth) {
			return false;
		}
		value = (float) this->capturepimpl->rgbdi.get()->depth.getSize().height
				/ 2.0f;
		return true;
	}

	if (OPENNI2_PP_X_COLOR == param) {
		if (false == this->paramspimpl->hascolor) {
			return false;
		}
		value = (float) this->capturepimpl->rgbdi.get()->color.getSize().width
				/ 2.0f;
		return true;
	}

	if (OPENNI2_PP_Y_COLOR == param) {
		if (false == this->paramspimpl->hascolor) {
			return false;
		}
		value = (float) this->capturepimpl->rgbdi.get()->color.getSize().height
				/ 2.0f;
		return true;
	}

	return false;
}

bool OpenNI2Grabber::getParameter(const GrabberParameter& param, float* values,
		unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool OpenNI2Grabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool OpenNI2Grabber::trigger(const GrabberTrigger& trig) {
	openni::Status rc = openni::STATUS_OK;

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (OPENNI2_INTERNAL_RECORD == trig) {
		if (false == this->iopimpl->recorder.isValid()) {
			std::string namelower = this->iopimpl->stream_filename.c_str();
			std::transform(namelower.begin(), namelower.end(),
					namelower.begin(), ::tolower);
			if (true == boost::algorithm::ends_with(namelower, ".oni")) {
				this->iopimpl->recorder.create(
						this->iopimpl->stream_filename.c_str());
			} else {
				this->iopimpl->recorder.create(
						(this->iopimpl->stream_filename + ".oni").c_str());
			}
			this->iopimpl->recorder.attach(this->capturepimpl->depth);
			this->iopimpl->recorder.attach(this->capturepimpl->color);
			this->iopimpl->recorder.attach(this->capturepimpl->ir);
			this->iopimpl->recorder.start();
		} else {
			this->iopimpl->recorder.stop();
			this->iopimpl->recorder.destroy();
		}
		return true;
	}

	if (false == this->iopimpl->local_streaming) {
		if (OPENNI2_GRAB_INTENSITY == trig) {
			return setIntensityMode();
		}

		if (OPENNI2_GRAB_COLOR == trig) {
			return setColorMode();
		}

		if (OPENNI2_AUTO_WHITE_BALANCE == trig) {
			openni::CameraSettings* cs =
					this->capturepimpl->color.getCameraSettings();
			cs->setAutoWhiteBalanceEnabled(!cs->getAutoWhiteBalanceEnabled());
			return true;
		}

		if (OPENNI2_AUTO_EXPOSURE == trig) {
			openni::CameraSettings* cs =
					this->capturepimpl->color.getCameraSettings();
			cs->setAutoExposureEnabled(!cs->getAutoExposureEnabled());
			return true;
		}

		if (OPENNI2_30HZ_FRAMERATE == trig) {
			openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
			vm.setFps(30);
			this->capturepimpl->ir.setVideoMode(vm);
			vm = this->capturepimpl->color.getVideoMode();
			vm.setFps(30);
			this->capturepimpl->color.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			vm.setFps(30);
			this->capturepimpl->depth.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			if (30 != vm.getFps()) {
				Log::add().error("OpenNI2Grabber::trigger",
						std::string("Could not change the framerate ")
								+ openni::OpenNI::getExtendedError());
				return false;
			}
			this->paramspimpl->framerate = 30;

			return true;
		}

		if (OPENNI2_60HZ_FRAMERATE == trig) {
			openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
			vm.setFps(60);
			this->capturepimpl->ir.setVideoMode(vm);
			vm = this->capturepimpl->color.getVideoMode();
			vm.setFps(60);
			this->capturepimpl->color.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			vm.setFps(60);
			this->capturepimpl->depth.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			if (60 != vm.getFps()) {
				Log::add().error("OpenNI2Grabber::trigger",
						std::string("Could not change the framerate ")
								+ openni::OpenNI::getExtendedError());
				return false;
			}
			this->paramspimpl->framerate = 60;

			return true;
		}

		if (OPENNI2_QVGA == trig) {
			bool r = this->paramspimpl->running;
			if (true == r) {
				stop();
			}
			int width, height;
			openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
			vm.setResolution(320, 240);
			this->capturepimpl->ir.setVideoMode(vm);
			vm = this->capturepimpl->ir.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->intensity.setSize(
					Size(width, height));

			vm = this->capturepimpl->color.getVideoMode();
			vm.setResolution(320, 240);
			this->capturepimpl->color.setVideoMode(vm);
			vm = this->capturepimpl->color.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));

			vm = this->capturepimpl->depth.getVideoMode();
			vm.setResolution(320, 240);
			this->capturepimpl->depth.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));

			if (true == r) {
				play();
			}

			return true;
		}

		if (OPENNI2_VGA == trig) {
			bool r = this->paramspimpl->running;
			if (true == r) {
				stop();
			}
			int width, height;

			openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
			vm.setResolution(640, 480);
			this->capturepimpl->ir.setVideoMode(vm);
			vm = this->capturepimpl->ir.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->intensity.setSize(
					Size(width, height));

			vm = this->capturepimpl->color.getVideoMode();
			vm.setResolution(640, 480);
			this->capturepimpl->color.setVideoMode(vm);
			vm = this->capturepimpl->color.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));

			vm = this->capturepimpl->depth.getVideoMode();
			vm.setResolution(640, 480);
			this->capturepimpl->depth.setVideoMode(vm);
			vm = this->capturepimpl->depth.getVideoMode();
			width = vm.getResolutionX();
			height = vm.getResolutionY();
			this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));

			if (true == r) {
				play();
			}
		}

		/*if (OPENNI2_SXGA == trig) {
		 bool r = this->paramspimpl->running;
		 if(true == r){
		 stop();
		 }
		 int width,height;
		 openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
		 vm.setResolution(1280,1024);
		 rc = this->capturepimpl->ir.setVideoMode(vm);
		 if (openni::STATUS_OK != rc) {
		 Log::add().error("OpenNI2Grabber::trigger", std::string("Could not change the resolution ") +
		 openni::OpenNI::getExtendedError());
		 return false;
		 }
		 vm = this->capturepimpl->ir.getVideoMode();
		 width = vm.getResolutionX();
		 height = vm.getResolutionY();
		 this->capturepimpl->rgbdi.get()->intensity.setSize(Size(width, height));

		 vm = this->capturepimpl->color.getVideoMode();
		 vm.setResolution(1280,1024);
		 rc = this->capturepimpl->color.setVideoMode(vm);
		 if (openni::STATUS_OK != rc) {
		 Log::add().error("OpenNI2Grabber::trigger", std::string("Could not change the resolution ") +
		 openni::OpenNI::getExtendedError());
		 return false;
		 }
		 vm = this->capturepimpl->color.getVideoMode();
		 width = vm.getResolutionX();
		 height = vm.getResolutionY();
		 this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));

		 vm = this->capturepimpl->depth.getVideoMode();
		 vm.setResolution(1280,1024);
		 rc = this->capturepimpl->depth.setVideoMode(vm);
		 if (openni::STATUS_OK != rc) {
		 Log::add().error("OpenNI2Grabber::trigger", std::string("Could not change the resolution ") +
		 openni::OpenNI::getExtendedError());
		 return false;
		 }
		 vm = this->capturepimpl->depth.getVideoMode();
		 width = vm.getResolutionX();
		 height = vm.getResolutionY();
		 this->capturepimpl->rgbdi.get()->depth.setSize(Size(width, height));
		 if(true == r){
		 play();
		 }
		 return true;
		 }
		 }*/
	}

	return false;
}

bool OpenNI2Grabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool OpenNI2Grabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool OpenNI2Grabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool OpenNI2Grabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void OpenNI2Grabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void OpenNI2Grabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&OpenNI2Grabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void OpenNI2Grabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(5);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		this->iopimpl->recorder.stop();
		this->iopimpl->recorder.destroy();
		emitEvent(this->threadpimpl->stopped);
	}
}

void OpenNI2Grabber::grabOneFrame(void) {
	/*if(true == this->iopimpl->local_streaming){
		if (false == this->paramspimpl->paused) {
			stop();
			bool sendupdate = false;
			boost::mutex::scoped_lock l(this->threadpimpl->mutex);
			openni::VideoFrameRef m_frame;
			if (true == this->paramspimpl->hasdepth) {
				this->capturepimpl->depth.start(); // Might do nothing if already started
				this->capturepimpl->depth.readFrame(&m_frame);
				const openni::DepthPixel * ref_src = (const openni::DepthPixel*) m_frame.getData();
				unsigned int s = this->capturepimpl->rgbdi.get()->depth.getSize().height * this->capturepimpl->rgbdi.get()->depth.getSize().width;
				float* ref_dst = this->capturepimpl->rgbdi->depth.getData().get();
				for (unsigned int i = 0; i < s; ++i) {
					ref_dst[i] = static_cast<float>(ref_src[i] * 0.001f);
				}
				sendupdate = true;
			}
			if (true == this->paramspimpl->hascolor) {
				this->capturepimpl->color.start(); // Might do nothing if already started
				this->capturepimpl->color.readFrame(&m_frame);
				const unsigned char * tmp = (const unsigned char*) m_frame.getData();
				this->capturepimpl->rgbdi.get()->color.setData(tmp);
				sendupdate = true;
			}
			if (true == this->paramspimpl->hasintensity) {
				this->capturepimpl->ir.start(); // Might do nothing if already started
				this->capturepimpl->ir.readFrame(&m_frame);
				unsigned int s = this->capturepimpl->rgbdi.get()->intensity.getSize().height * this->capturepimpl->rgbdi.get()->intensity.getSize().width;
				unsigned char* ref_dst = this->capturepimpl->rgbdi->intensity.getData().get();
				const openni::Grayscale16Pixel * ref_src = (const openni::Grayscale16Pixel*) m_frame.getData();
				for (unsigned int i = 0; i < s; ++i) {
					ref_dst[i] = static_cast<unsigned char>(ref_src[i]* 0.2491234275);
				}
				sendupdate = true;
			}
			if(true == sendupdate){
				emitEvent(this->threadpimpl->updated);
			}
		}
	}*/
	Log::add().warning("OpenNIGrabber2::grabOneFrame", "Not available");
}

void OpenNI2Grabber::run(void) {
	openni::Status rc = openni::STATUS_OK;

	if (true == this->paramspimpl->hascolor) {
		rc = this->capturepimpl->color.start();
		this->capturepimpl->color.addNewFrameListener(this->color_callback);
		if (openni::STATUS_OK != rc) {
			Log::add().error("OpenNI2Grabber::run",
					std::string("Could not start the color stream ")
							+ openni::OpenNI::getExtendedError());
		}
	}
	if (true == this->paramspimpl->hasdepth) {
		rc = this->capturepimpl->depth.start();
		this->capturepimpl->depth.addNewFrameListener(this->depth_callback);
		if (openni::STATUS_OK != rc) {
			Log::add().error("OpenNI2Grabber::run",
					std::string("Could not start the depth stream ")
							+ openni::OpenNI::getExtendedError());
		}
	}
	if (true == this->paramspimpl->hasintensity) {
		rc = this->capturepimpl->ir.start();
		this->capturepimpl->ir.addNewFrameListener(this->ir_callback);
		if (openni::STATUS_OK != rc) {
			Log::add().error("OpenNI2Grabber::run",
					std::string("Could not start the ir stream ")
							+ openni::OpenNI::getExtendedError());
		}
	}

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;
	Timer timer;
	timer.start();
	long int current_time;

	while (true == this->paramspimpl->running) {
		current_time = timer.get();

		/*
		 * Emit a message to confirm the update
		 */
		emitEvent(this->threadpimpl->updated);
		current_time = (1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}
	}

	if (true == this->paramspimpl->hascolor) {
		this->capturepimpl->color.stop();
		this->capturepimpl->ir.removeNewFrameListener(this->color_callback);
	}
	if (true == this->paramspimpl->hasintensity) {
		this->capturepimpl->ir.stop();
		this->capturepimpl->ir.removeNewFrameListener(this->ir_callback);
	}
	if (true == this->paramspimpl->hasdepth) {
		this->capturepimpl->depth.stop();
		this->capturepimpl->ir.removeNewFrameListener(this->depth_callback);
	}

	timer.stop();
	this->paramspimpl->finished = true;
}

bool OpenNI2Grabber::setIntensityMode(void) {

	if (true == this->paramspimpl->hasintensity) {
		return true;
	}

	openni::Status rc = openni::STATUS_OK;
	rc = this->capturepimpl->ir.create(*this->capturepimpl->device,
			openni::SENSOR_IR);
	if (openni::STATUS_OK == rc) {
		boost::mutex::scoped_lock l(this->threadpimpl->mutex);
		if (true == this->paramspimpl->hascolor) {
			this->paramspimpl->hascolor = false;
			this->capturepimpl->color.removeNewFrameListener(
					this->color_callback);
			this->capturepimpl->color.stop();
			this->capturepimpl->color.destroy();
			this->capturepimpl->rgbdi.get()->color.release();
		}
		unsigned int width, height;
		openni::VideoMode vm = this->capturepimpl->ir.getVideoMode();
		width = vm.getResolutionX();
		height = vm.getResolutionY();
		this->capturepimpl->rgbdi.get()->intensity.setSize(Size(width, height));
		this->capturepimpl->ir.setMirroringEnabled(
				!this->capturepimpl->ir.getMirroringEnabled());

		if (true == this->paramspimpl->running) {
			this->capturepimpl->ir.addNewFrameListener(this->ir_callback);
			rc = this->capturepimpl->ir.start();
			if (openni::STATUS_OK != rc) {
				Log::add().error("OpenNI2Grabber::setIntensityMode",
						openni::OpenNI::getExtendedError());
				return false;
			}
		}
		this->paramspimpl->hasintensity = true;
		return true;
	}
	Log::add().error("OpenNI2Grabber::setIntensityMode",
			openni::OpenNI::getExtendedError());
	return false;
}

bool OpenNI2Grabber::setColorMode(void) {

	if (true == this->paramspimpl->hascolor) {
		return true;
	}
	openni::Status rc = openni::STATUS_OK;
	rc = this->capturepimpl->color.create(*this->capturepimpl->device,
			openni::SENSOR_COLOR);
	if (openni::STATUS_OK == rc) {
		boost::mutex::scoped_lock l(this->threadpimpl->mutex);
		if (true == this->paramspimpl->hasintensity) {
			this->paramspimpl->hasintensity = false;
			this->capturepimpl->ir.removeNewFrameListener(this->ir_callback);
			this->capturepimpl->ir.stop();
			this->capturepimpl->ir.destroy();
			this->capturepimpl->rgbdi.get()->intensity.release();
		}
		unsigned int width, height;
		openni::VideoMode vm = this->capturepimpl->color.getVideoMode();
		width = vm.getResolutionX();
		height = vm.getResolutionY();
		this->capturepimpl->rgbdi.get()->color.setSize(Size(width, height));
		this->capturepimpl->color.setMirroringEnabled(
				!this->capturepimpl->color.getMirroringEnabled());

		if (true == this->paramspimpl->running) {
			rc = this->capturepimpl->color.start();
			if (openni::STATUS_OK != rc) {
				Log::add().error("OpenNI2Grabber::setColorMode",
						openni::OpenNI::getExtendedError());
				return false;
			}
			this->capturepimpl->color.addNewFrameListener(this->color_callback);
		}
		this->paramspimpl->hascolor = true;
		return true;
	}
	Log::add().error("OpenNI2Grabber::setColorMode",
			openni::OpenNI::getExtendedError());
	return false;
}

void OpenNI2Grabber::setStreamFilename(const std::string& filename) {
	this->iopimpl->stream_filename = filename;
}

std::string OpenNI2Grabber::getStreamFilename(void) const {
	return this->iopimpl->stream_filename;
}

}
