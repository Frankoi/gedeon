/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereyedriver.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereyedriver.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/log.hpp"
#include "gedeon/core/time.hpp"
#include "gedeon/grabber/tigereyedriver.hpp"

#ifdef WIN32
#define _WINSOCKAPI_
#include <windows.h>
#include <winsock2.h>
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")
#include <iphlpapi.h>
 #define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))
#else
	#include <net/if.h>
	#include <arpa/inet.h>
	#include <sys/socket.h>
	#include <sys/ioctl.h>
#endif

#include <cassert>
#include <pcap.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>



namespace gedeon {

#define CAPTURE_TIMEOUT 1000
#define MAX_INVALID_PACKETS 150
#define SIZE_BROADCAST_PACKET 18
#define SIZE_ADDRESS 6

#define MAX_SIZE_BUF 2048

TigerEyeDriver TigerEyeDriver::driver_singleton;

TigerEyeDriver::TigerEyeDriver(void) :
		network_device(""), broadcast_packet(0), local_mac_address(0),
		initialized(false) {

}

bool TigerEyeDriver::setInterface(const std::string& inter){

	if(inter == ""){
		Log::add().error("TigerEyeDriver::setInterface","Interface's name is empty");
		return false;
	}

	this->local_mac_address = new unsigned char[SIZE_ADDRESS];
	this->broadcast_packet = new unsigned char[SIZE_BROADCAST_PACKET];
	this->network_device = inter;

	/*
	 * Get the local mac address
	 */
#ifdef WIN32
	DWORD dwSize = 0;
	DWORD dwRetVal = 0;
	int i = 0;
	ULONG flags = 0, outBufLen = 0, family;
	LPVOID lpMsgBuf;
	PIP_ADAPTER_ADDRESSES pAddresses;
	PIP_ADAPTER_ADDRESSES pCurrAddresses;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicast;
	PIP_ADAPTER_ANYCAST_ADDRESS pAnycast;
	PIP_ADAPTER_MULTICAST_ADDRESS pMulticast;
	IP_ADAPTER_DNS_SERVER_ADDRESS *pDnServer = NULL;

	IP_ADAPTER_PREFIX *pPrefix = NULL;
	family = AF_UNSPEC;
	outBufLen = sizeof (IP_ADAPTER_ADDRESSES);
	pAddresses = (IP_ADAPTER_ADDRESSES *) MALLOC(outBufLen);
	if (GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen) == ERROR_BUFFER_OVERFLOW){
		FREE(pAddresses);
		pAddresses = (IP_ADAPTER_ADDRESSES *) MALLOC(outBufLen);
	}
	if (GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen)== NO_ERROR ) {
		bool not_found = true;
		pCurrAddresses = pAddresses;
		
		while(pCurrAddresses && not_found) {
			std::wstring ws( pCurrAddresses->FriendlyName);
			std::string test( ws.begin(), ws.end() );

			std::wstring ws2( pCurrAddresses->Description);
			std::string test2( ws2.begin(), ws2.end() );
			if(test == inter) {
				for(unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
					broadcast_packet[i] = '\xff';
					broadcast_packet[i+SIZE_ADDRESS] = (unsigned char) pCurrAddresses->PhysicalAddress[i];
					this->local_mac_address[i] = (unsigned char) pCurrAddresses->PhysicalAddress[i];
				}
#ifdef DEBUG
		std::stringstream ss;
		for(unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			ss << std::hex << std::uppercase << std::setw(2) << (int)(*(unsigned char*)(&broadcast_packet[i+SIZE_ADDRESS])) << std::dec;
			if(i<5) ss << ":";
		}
		Log::add().debug("TigerEyeDriver::setInterface","Local MAC Address: " + ss.str());
#endif
				/*
				 * Find the correct interface
				 */
				this->network_device = std::string("");
				struct pcap_if *interfaces;
				char error[256];
				if(pcap_findalldevs(&interfaces, error) != -1) {
					while(interfaces != NULL) {
						std::string target(interfaces->name);
						if(target.find( pCurrAddresses->AdapterName) != std::string::npos ) {

							this->network_device = interfaces->name;
						}
						interfaces = interfaces->next;
					}
					if(network_device=="") {
						delete[] this->local_mac_address;
						this->local_mac_address = 0;
						delete[] this->broadcast_packet;
						this->broadcast_packet = 0;
						this->network_device = "";
						Log::add().error("TigerEyeDriver::setInterface","Problem with the capture interface");
						return false;
					}
				}

				not_found = false;
			}
			pCurrAddresses=pCurrAddresses->Next;
		}

		FREE(pAddresses);
		assert(not_found == false);

		/*
		 * ether proto 0x8040
		 */
		broadcast_packet[SIZE_ADDRESS*2] = '\x80';
		broadcast_packet[SIZE_ADDRESS*2+1] = '\x40';

		broadcast_packet[SIZE_ADDRESS*2+2] = '\x04';
		broadcast_packet[SIZE_ADDRESS*2+3] = '\x00';
		broadcast_packet[SIZE_ADDRESS*2+4] = '\x00';
		broadcast_packet[SIZE_ADDRESS*2+5] = '\x04';

	}else{
		delete[] this->local_mac_address;
		this->local_mac_address = 0;
		delete[] this->broadcast_packet;
		this->broadcast_packet = 0;
		this->network_device = "";
		Log::add().error("TigerEyeDriver::setInterface","Problem with the capture interface");
		return false;
	}

#else
	struct ifreq s;
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	strcpy(s.ifr_name, inter.c_str());
	if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {

		/*
		 * Create the broadcast command packet
		 */
		for (unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			broadcast_packet[i] = '\xff';
			broadcast_packet[i + SIZE_ADDRESS] =
					(unsigned char) s.ifr_addr.sa_data[i];
			this->local_mac_address[i] = (unsigned char) s.ifr_addr.sa_data[i];
		}

		/*
		 * ether proto 0x8040
		 */
		broadcast_packet[SIZE_ADDRESS * 2] = '\x80';
		broadcast_packet[SIZE_ADDRESS * 2 + 1] = '\x40';

		broadcast_packet[SIZE_ADDRESS * 2 + 2] = '\x04';
		broadcast_packet[SIZE_ADDRESS * 2 + 3] = '\x00';
		broadcast_packet[SIZE_ADDRESS * 2 + 4] = '\x00';
		broadcast_packet[SIZE_ADDRESS * 2 + 5] = '\x04';

#ifdef DEBUG
		std::stringstream ss;
		for(unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			ss << std::hex << std::uppercase << std::setw(2) << (int)(*(unsigned char*)(&broadcast_packet[i+SIZE_ADDRESS])) << std::dec;
			if(i<5) ss << ":";
		}
		Log::add().debug("TigerEyeDriver::setInterface","Local MAC Address: " + ss.str());
#endif
	}else{
		delete[] this->local_mac_address;
		this->local_mac_address = 0;
		delete[] this->broadcast_packet;
		this->broadcast_packet = 0;
		this->network_device = "";
		Log::add().error("TigerEyeDriver::setInterface","Problem with the capture interface");
		return false;
	}
#endif

	return true;

}


unsigned char* TigerEyeDriver::getLocalMacAddress(void) const {
	return this->local_mac_address;
}

std::string TigerEyeDriver::getNetworkDevice(void) const {
	return this->network_device;
}

TigerEyeDriver::~TigerEyeDriver(void) {

	if (0 != this->local_mac_address) {
		delete[] this->local_mac_address;
	}

	if (0 != this->broadcast_packet) {
		delete[] this->broadcast_packet;
	}

	this->devices_mac_addresses.clear();

}

bool TigerEyeDriver::refresh(void) {

	if(0 == this->local_mac_address || this->network_device == ""){
		Log::add().error("TigerEyeDriver::refresh",
						"Capture interface has not been defined yet");
		return false;
	}

	this->initialized = false;

	char error_buffer[PCAP_ERRBUF_SIZE];
	pcap_t *descr;

	descr = pcap_open_live(this->network_device.c_str(), MAX_SIZE_BUF, 1,
			CAPTURE_TIMEOUT, error_buffer);
	if (0 == descr) {
		Log::add().error("TigerEyeDriver::refresh",
				"Couldn't open the device interface. "
						+ std::string(error_buffer));
		return false;
	}

	/*
	 * Filter the packet reception to the only ethernet protocol 0x8040
	 */
	struct bpf_program fp;
	bpf_u_int32 maskp, netp;
	if (-1
			== pcap_lookupnet(this->network_device.c_str(), &netp, &maskp,
					error_buffer)) {
		Log::add().error("TigerEyeDriver::refresh",
				"Unable to get information from the network device."
						+ std::string(error_buffer));
		return false;
	}
	char* filter = (char*) "ether proto 0x8040";
	if (0 > pcap_compile(descr, &fp, filter, 0, netp)) {
		Log::add().error("TigerEyeDriver::refresh",
				"Unable to compile the packet filter. Check the syntax");
		return false;
	}
	if (-1 == pcap_setfilter(descr, &fp)) {
		Log::add().error("TigerEyeDriver::refresh",
				"Error while setting the filter");
		return false;
	}

	pcap_freecode(&fp);

	if (-1 == pcap_sendpacket(descr, broadcast_packet, SIZE_BROADCAST_PACKET)) {
		Log::add().error("TigerEyeDriver::refresh",
				"Couldn't send the packet");
		return false;
	}

	/*
	 * Check for the packets
	 * Iterations stop if a timeout occurs or the number of incorrect packets is exceeded
	 */
	struct pcap_pkthdr header;
	const u_char *packet = 0;
	unsigned int invalid_packets = 0;
	do {
		packet = pcap_next(descr, &header);
		if (packet != 0) {
			if (header.len == 60) {

				std::stringstream ss;
				for (unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
					ss << std::hex << std::uppercase << std::setw(2)
							<< (int) packet[SIZE_ADDRESS + i] << std::dec;
					if (i < 5)
						ss << ":";
				}
				std::string device_mac_address = ss.str();
				std::replace(device_mac_address.begin(),
						device_mac_address.end(), ' ', '0');
				devices_mac_addresses.push_back(device_mac_address);
			} else {
				++invalid_packets;
			}
		}
	} while (0 != packet && MAX_INVALID_PACKETS > invalid_packets);

	if (0 == devices_mac_addresses.size()) {
		Log::add().warning("TigerEyeDriver::refresh",
				"No Asc Tiger Eye found");
	}

	pcap_close(descr);

	this->initialized = true;

	return true;

}

}
