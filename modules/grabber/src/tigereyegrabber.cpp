/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereyegrabber.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereyegrabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/grabber/tigereyedriver.hpp"
#include "gedeon/grabber/tigereyegrabber.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace gedeon {

#define SIZE_DWORD 4
#define SIZE_ADDRESS 6
#define SIZE_PROTOCOL 2
#define SIZE_HEADER SIZE_ADDRESS*2 + SIZE_PROTOCOL
#define SIZE_OPCODE 4
#define SIZE_COMMAND SIZE_DWORD*2
#define SIZE_LONG_READ 8
#define SIZE_PACKET SIZE_HEADER + SIZE_OPCODE + SIZE_COMMAND + SIZE_LONG_READ

#define MAX_SIZE_BUF 1510

#define COMMAND_POSITION SIZE_HEADER + SIZE_OPCODE

#define CAPTURE_TIMEOUT 1000
#define SIZE_REPLY_PACKET 60
#define SIZE_DATA_PACKET 1510

#define NUMBER_OF_PACKETS 45
#define PIXELS_PER_PACKET 372

static const unsigned char protocol[SIZE_PROTOCOL] = { '\x80', '\x40' };
static const unsigned char opcode_write_dma[SIZE_OPCODE] = { '\x0c', '\x00',
		'\x10', '\x02' };
static const unsigned char opcode_command[SIZE_OPCODE] = { '\x0c', '\x02',
		'\x00', '\x02' };
static const unsigned char long_read[SIZE_LONG_READ] = { '\x08', '\x00', '\x00',
		'\x02', '\x00', '\x00', '\x00', '\x10' };
static const unsigned char long_read_reply[SIZE_LONG_READ] = { '\x18', '\x00',
		'\x00', '\x02', '\x00', '\x00', '\x00', '\x10' };

static const unsigned char take_one_image_command[SIZE_COMMAND] = { '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x01' };
static const unsigned char reset_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x01', '\x00', '\x00', '\x00', '\x00', '\x00' };
static const unsigned char run_at_rate_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x00', '\x00', '\x00', '\x01', '\x00', '\x02' };
static const unsigned char stop_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x02' };
static const unsigned char update_voltages_command[SIZE_COMMAND] = { '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x80' };
static const unsigned char NUC_command[SIZE_COMMAND] = { '\x00', '\x02', '\x01',
		'\x12', '\x00', '\x01', '\x00', '\x02' };

static const unsigned char set_framerate_command_ref[SIZE_COMMAND] = { '\x19',
		'\xf9', '\x01', '\x31', '\x19', '\xf9', '\x00', '\x04' };
static const unsigned char set_detector_gain_command_ref[SIZE_COMMAND] = {
		'\x04', '\xeb', '\x01', '\x20', '\x04', '\xeb', '\x00', '\x20' };
static const unsigned char set_amplifier_gain_command_ref[SIZE_COMMAND] = {
		'\x2a', '\x3f', '\x01', '\x22', '\x2a', '\x3f', '\x00', '\x20' };
static const unsigned char set_threshold_filter_command_ref[SIZE_COMMAND] = {
		'\x33', '\x44', '\x01', '\x23', '\x33', '\x44', '\x00', '\x20' };
static const unsigned char set_threshold_level_command_ref[SIZE_COMMAND] = {
		'\xf6', '\xb9', '\x01', '\x2f', '\xf6', '\xb9', '\x00', '\x20' };

class TigerEyeGrabberParams {
public:
	TigerEyeGrabberParams(void) :
			paused(false), hascolor(false), hasdepth(false), hasintensity(
					false), running(false), finished(true), initialized(false), framerate(
					DEFAULT_FRAMERATE), adaptative_framerate(DEFAULT_FRAMERATE), header(
					0), set_framerate_command(0), set_detector_gain_command(0), detector_gain(
					40.0f), set_amplifier_gain_command(0), amplifier_gain(1.6f), set_threshold_filter_command(
					0), threshold_filter(0.51f), set_threshold_level_command(0), threshold_level(
					1.05f), reset_device(false), laser_drive(0.0f), sensor_temperature(
					0.0f), laser_temperature(0.0f), sensor_voltage(0.0f), sensor_heat_sink_temperature(
					0.0f), laser_heat_sink_temperature(0.0f), laser_diod_temperature(
					0.0f), laser_capacity_voltage(0.0f), process_board_temperature(
					0.0f), resetcount(-1) {
	}
	~TigerEyeGrabberParams(void) {
		if (0 != this->set_framerate_command) {
			delete[] this->set_framerate_command;
		}

		if (0 != this->set_detector_gain_command) {
			delete[] this->set_detector_gain_command;
		}

		if (0 != this->set_amplifier_gain_command) {
			delete[] this->set_amplifier_gain_command;
		}

		if (0 != this->set_threshold_filter_command) {
			delete[] this->set_threshold_filter_command;
		}

		if (0 != this->set_threshold_level_command) {
			delete[] this->set_threshold_level_command;
		}
		if (0 != this->header) {
			delete[] this->header;
		}
	}

	bool paused;
	bool hascolor;
	bool hasdepth;
	bool hasintensity;
	bool running;
	bool finished;
	bool initialized;
	float framerate;
	float adaptative_framerate;

	unsigned char *header;
	unsigned char *set_framerate_command;
	unsigned char *set_detector_gain_command;
	float detector_gain;
	unsigned char *set_amplifier_gain_command;
	float amplifier_gain;
	unsigned char *set_threshold_filter_command;
	float threshold_filter;
	unsigned char *set_threshold_level_command;
	float threshold_level;
	bool reset_device;
	float laser_drive;
	float sensor_temperature;
	float laser_temperature;
	float sensor_voltage;
	float sensor_heat_sink_temperature;
	float laser_heat_sink_temperature;
	float laser_diod_temperature;
	float laser_capacity_voltage;
	float process_board_temperature;
	int resetcount;

};

class TigerEyeGrabberIO {
public:
	TigerEyeGrabberIO(void) :
			local_streaming(false), stream_filename("tigereye-output-internal"), recording(
					false), frame_counter(0), nb_frames(0) {
	}

	~TigerEyeGrabberIO(void) {
		file_list_local.clear();
	}

	bool local_streaming;
	std::string stream_filename;
	std::vector<boost::filesystem::path> file_list_local;

	bool recording;
	unsigned int frame_counter;
	unsigned int nb_frames;
};

class TigerEyeGrabberCapture {
public:
	TigerEyeGrabberCapture(void) :
			devicename(""), device_id(0), packet_command(0), packet_command_without_long_read(
					0), local_mac_address(0), descr(0) {
	}
	~TigerEyeGrabberCapture(void) {
		if (0 != this->local_mac_address) {
			delete[] this->local_mac_address;
		}
		if (0 != this->descr) {
			pcap_close(this->descr);
		}
		if (0 != this->packet_command) {
			delete[] this->packet_command;
		}
		if (0 != this->packet_command_without_long_read) {
			delete[] this->packet_command_without_long_read;
		}

	}
	RGBDIImagePtr rgbdi;
	std::string devicename;
	unsigned int device_id;

	unsigned char *packet_command;
	unsigned char *packet_command_without_long_read;
	unsigned char *local_mac_address;

	pcap_t *descr;
	bpf_u_int32 maskp, netp;

};

class TigerEyeGrabberThread {
public:
	TigerEyeGrabberThread(void) :
			pthread(0) {
	}
	~TigerEyeGrabberThread(void) {
		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}
	}
	boost::thread *pthread;
	boost::mutex mutex;
	boost::mutex mutex_command_without_long_read;
	EventDataPtr updated;
	EventDataPtr ready;
	EventDataPtr paused;
	EventDataPtr stopped;
	EventDataPtr playing;

};

int getAddress(const unsigned char * packet);

void getIntensityDepth(const unsigned char * packet, float& intensity,
		float& depth);

bool checkPacket(const unsigned char* sent, const unsigned char* received);

TigerEyeGrabber::TigerEyeGrabber(void) :
		Grabber(), capturepimpl(new TigerEyeGrabberCapture()), paramspimpl(
				new TigerEyeGrabberParams()), threadpimpl(
				new TigerEyeGrabberThread()), iopimpl(new TigerEyeGrabberIO()) {

	this->threadpimpl->updated.reset(new EventData("updated"));
	this->threadpimpl->ready.reset(new EventData("ready"));
	this->threadpimpl->paused.reset(new EventData("paused"));
	this->threadpimpl->stopped.reset(new EventData("stopped"));
	this->threadpimpl->playing.reset(new EventData("playing"));

	addEventName("updated");
	addEventName("ready");
	addEventName("paused");
	addEventName("stopped");
	addEventName("playing");
}

bool TigerEyeGrabber::init(const unsigned int & id, Driver* d) {

	if (true == this->paramspimpl->initialized) {
		Log::add().warning("TigerEyeGrabber::init",
				"The grabber is already initialized");
		return false;
	}

	bool created = false;
	TigerEyeDriver *driver = 0;

	try {
		driver = dynamic_cast<TigerEyeDriver *>(d);
	} catch (const std::bad_cast& e) {
		Log::add().error("TigerEyeGrabber::init",
				"The driver is not compatible");
		return false;
	}

	this->capturepimpl->device_id = id;
	/*
	 * For communication with the device we only need its MAC address
	 */
	if (0 != driver->getCount() && id < driver->getCount()) {

		std::vector<std::string> devices = driver->populate();
		this->capturepimpl->devicename = devices[id];

		std::string tomac = this->capturepimpl->devicename;
		std::replace(tomac.begin(), tomac.end(), ':', ' ');

		/*
		 * Convert the name of the device into a MAC address
		 */
		std::istringstream hex_chars_stream(tomac);
		unsigned int c;
		unsigned int i = 0;
		unsigned char device_mac_address[SIZE_ADDRESS];
		while (hex_chars_stream >> std::hex >> c && SIZE_ADDRESS > i) {
			device_mac_address[i] = static_cast<unsigned char>(c);
			++i;
		}

		/*
		 * create header of the write packet
		 */
		this->paramspimpl->header = new unsigned char[SIZE_ADDRESS * 2 + 2]; // 2 mac addresses + protocol

		for (unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			this->paramspimpl->header[i] = device_mac_address[i];
			this->paramspimpl->header[i + SIZE_ADDRESS] =
					(driver->getLocalMacAddress())[i];
		}

		this->capturepimpl->local_mac_address = new unsigned char[SIZE_COMMAND];
		memset(this->capturepimpl->local_mac_address, 0,
				SIZE_COMMAND * sizeof(unsigned char));
		for (unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			this->capturepimpl->local_mac_address[i + 2] =
					this->paramspimpl->header[i + SIZE_ADDRESS];
		}

		memcpy(this->paramspimpl->header + 2 * SIZE_ADDRESS, protocol,
				SIZE_PROTOCOL * sizeof(unsigned char));

		this->capturepimpl->packet_command = new unsigned char[SIZE_PACKET];
		this->capturepimpl->packet_command_without_long_read =
				new unsigned char[SIZE_PACKET];
		for (unsigned int i = 0; i < SIZE_ADDRESS * 2 + 2; ++i) {
			this->capturepimpl->packet_command[i] =
					this->paramspimpl->header[i];
			this->capturepimpl->packet_command_without_long_read[i] =
					this->paramspimpl->header[i];
		}

		/*
		 * Create the command packet
		 */
		memcpy(this->capturepimpl->packet_command + SIZE_HEADER, opcode_command,
				SIZE_OPCODE * sizeof(unsigned char));
		memset(this->capturepimpl->packet_command + SIZE_HEADER + SIZE_OPCODE,
				0, SIZE_COMMAND * sizeof(unsigned char));
		memcpy(
				this->capturepimpl->packet_command + SIZE_HEADER + SIZE_OPCODE
						+ SIZE_COMMAND, long_read,
				SIZE_LONG_READ * sizeof(unsigned char));

		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ SIZE_HEADER, opcode_command,
				SIZE_OPCODE * sizeof(unsigned char));
		memset(
				this->capturepimpl->packet_command_without_long_read
						+ SIZE_HEADER + SIZE_OPCODE, 0,
				(SIZE_COMMAND + SIZE_LONG_READ) * sizeof(unsigned char));

		/*
		 * Setup PCAP interface
		 */
		char error_buffer[PCAP_ERRBUF_SIZE];
		this->capturepimpl->descr = pcap_open_live(
				driver->getNetworkDevice().c_str(), MAX_SIZE_BUF, 1,
				CAPTURE_TIMEOUT, error_buffer);
		if (0 == this->capturepimpl->descr) {
			throw Exception("TigerEyeGrabber::init",
					"Couldn't open the device interface. "
							+ std::string(error_buffer));
		}

		if (-1
				== pcap_lookupnet(driver->getNetworkDevice().c_str(),
						&(this->capturepimpl->netp),
						&(this->capturepimpl->maskp), error_buffer)) {
			throw Exception("TigerEyeGrabber::init",
					"Unable to get information from the network device."
							+ std::string(error_buffer));
		}

		/*
		 * Create a filter based on the protocol and the source/destination addresses of the packets
		 */
		std::stringstream ss;
		for (unsigned int i = 0; i < SIZE_ADDRESS; ++i) {
			ss << std::hex << std::uppercase << std::setw(2)
					<< (int) this->capturepimpl->local_mac_address[i + 2];
			if (i < 5) {
				ss << ":";
			}
		}

		std::string local_mac_address_str = ss.str();
		std::replace(local_mac_address_str.begin(), local_mac_address_str.end(),
				' ', '0');
		std::string filter("ether proto 0x8040");
		filter += " and wlan src " + driver->populate()[id] + " and wlan dst "
				+ local_mac_address_str;

		struct bpf_program fp;
		if (0
				> pcap_compile(this->capturepimpl->descr, &fp,
						(char*) filter.c_str(), 0, this->capturepimpl->netp)) {
			throw Exception("TigerEyeGrabber::init",
					"Unable to compile the packet filter. Check the syntax");
		}
		if (-1 == pcap_setfilter(this->capturepimpl->descr, &fp)) {
			throw Exception("TigerEyeGrabber::init",
					"Error while setting the filter");
		}
		pcap_freecode(&fp);

		if (false == connectCommand()) {
			return false;
		}

		this->paramspimpl->set_framerate_command =
				new unsigned char[SIZE_COMMAND];
		assert(0!=this->paramspimpl->set_framerate_command);
		memcpy(this->paramspimpl->set_framerate_command,
				set_framerate_command_ref,
				SIZE_COMMAND * sizeof(unsigned char));

		this->paramspimpl->set_detector_gain_command =
				new unsigned char[SIZE_COMMAND];
		assert(0!=this->paramspimpl->set_detector_gain_command);
		memcpy(this->paramspimpl->set_detector_gain_command,
				set_detector_gain_command_ref,
				SIZE_COMMAND * sizeof(unsigned char));

		this->paramspimpl->set_amplifier_gain_command =
				new unsigned char[SIZE_COMMAND];
		assert(0!=this->paramspimpl->set_amplifier_gain_command);
		memcpy(this->paramspimpl->set_amplifier_gain_command,
				set_amplifier_gain_command_ref,
				SIZE_COMMAND * sizeof(unsigned char));

		this->paramspimpl->set_threshold_filter_command =
				new unsigned char[SIZE_COMMAND];
		assert(0!=this->paramspimpl->set_threshold_filter_command);
		memcpy(this->paramspimpl->set_threshold_filter_command,
				set_threshold_filter_command_ref,
				SIZE_COMMAND * sizeof(unsigned char));

		this->paramspimpl->set_threshold_level_command =
				new unsigned char[SIZE_COMMAND];
		assert(0!=this->paramspimpl->set_threshold_level_command);
		memcpy(this->paramspimpl->set_threshold_level_command,
				set_threshold_level_command_ref,
				SIZE_COMMAND * sizeof(unsigned char));

		bool valid_packet = takeOneImage();

		this->capturepimpl->rgbdi.reset(new RGBDIImage);
		this->capturepimpl->rgbdi->intensity.setSize(Size(128, 128));
		this->capturepimpl->rgbdi->depth.setSize(Size(128, 128));
		this->paramspimpl->hasintensity = true;
		this->paramspimpl->hasdepth = true;

		if (true == valid_packet) {
			created = true;
		}

		if (false == created) {
			Log::add().error("TigerEyeGrabber::init",
					"Failed to create the device");
			return false;
		}

		this->threadpimpl->updated->sender = this;
		this->threadpimpl->ready->sender = this;
		this->paramspimpl->initialized = true;
		this->threadpimpl->stopped->sender = this;
		this->threadpimpl->playing->sender = this;
		this->threadpimpl->paused->sender = this;
		emitEvent(this->threadpimpl->ready);
		return true;

	}

	Log::add().error("TigerEyeGrabber::init", "No device found");
	return false;

}

bool TigerEyeGrabber::init(const std::string& source, Driver* d) {

	if (false == is_directory(source)) {
		Log::add().error("TigerEyeGrabber::init", "Not a valid directory");
		return false;
	} else {
		path my_dir(source);
		directory_iterator end;
		for (directory_iterator iter(my_dir); iter != end; ++iter) {
			if (true
					== boost::algorithm::ends_with(iter->path().string(),
							".txt")) {
				this->iopimpl->file_list_local.push_back(*iter);
			}
		}
		if (this->iopimpl->file_list_local.size() > 0) {
			sort(this->iopimpl->file_list_local.begin(),
					this->iopimpl->file_list_local.end());
			this->iopimpl->nb_frames = this->iopimpl->file_list_local.size();
		} else {
			Log::add().error("TigerEyeGrabber::init", "The directory is empty");
			return false;
		}
	}
	this->capturepimpl->rgbdi.reset(new RGBDIImage);
	this->capturepimpl->rgbdi->intensity.setSize(Size(128, 128));
	this->capturepimpl->rgbdi->depth.setSize(Size(128, 128));
	this->paramspimpl->hasintensity = true;
	this->paramspimpl->hasdepth = true;
	this->threadpimpl->updated->sender = this;
	this->threadpimpl->ready->sender = this;
	this->threadpimpl->stopped->sender = this;
	this->threadpimpl->playing->sender = this;
	this->threadpimpl->paused->sender = this;
	this->paramspimpl->initialized = true;
	emitEvent(this->threadpimpl->ready);
	this->iopimpl->local_streaming = true;

	return true;
}

TigerEyeGrabber::~TigerEyeGrabber(void) {
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(100);
	}

}

std::string TigerEyeGrabber::getName(void) const {
	return this->capturepimpl->devicename;
}

unsigned int TigerEyeGrabber::getID(void) const {
	return this->capturepimpl->device_id;
}

const RGBDIImagePtr& TigerEyeGrabber::getImage(void) const {
	return this->capturepimpl->rgbdi;
}

bool TigerEyeGrabber::getParameter(const GrabberParameter& param,
		float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (TIGEREYE_FRAMERATE == param) {
		value = this->paramspimpl->adaptative_framerate;
		return true;
	}

	if (this->iopimpl->local_streaming == false) {

		if (TIGEREYE_DETECTOR_GAIN == param) {
			value = this->paramspimpl->detector_gain;
			return true;
		}

		if (TIGEREYE_AMPLIFIER_GAIN == param) {
			value = this->paramspimpl->amplifier_gain;
			return true;
		}

		if (TIGEREYE_THRESHOLD_FILTER == param) {
			value = this->paramspimpl->threshold_filter;
			return true;
		}

		if (TIGEREYE_THRESHOLD_LEVEL == param) {
			value = this->paramspimpl->threshold_level;
			return true;
		}

		if (TIGEREYE_LASER_DRIVE == param) {
			value = this->paramspimpl->laser_drive;
			return true;
		}

		if (TIGEREYE_SENSOR_TEMPERATURE == param) {
			value = this->paramspimpl->sensor_temperature;
			return true;
		}

		if (TIGEREYE_LASER_TEMPERATURE == param) {
			value = this->paramspimpl->laser_temperature;
			return true;
		}

		if (TIGEREYE_SENSOR_VOLTAGE == param) {
			value = this->paramspimpl->sensor_voltage;
			return true;
		}

		if (TIGEREYE_SENSOR_HEAT_SINK_TEMPERATURE == param) {
			value = this->paramspimpl->sensor_heat_sink_temperature;
			return true;
		}

		if (TIGEREYE_LASER_HEAT_SINK_TEMPERATURE == param) {
			value = this->paramspimpl->laser_heat_sink_temperature;
			return true;
		}

		if (TIGEREYE_LASER_DIODE_TEMPERATURE == param) {
			value = this->paramspimpl->laser_diod_temperature;
			return true;
		}

		if (TIGEREYE_LASER_CAPACITY_VOLTAGE == param) {
			value = this->paramspimpl->laser_capacity_voltage;
			return true;
		}

		if (TIGEREYE_PROCESS_BOARD_TEMPERATURE == param) {
			value = this->paramspimpl->process_board_temperature;
			return true;
		}

		if (TIGEREYE_RESET_COUNTDOWN == param) {
			value = static_cast<float>(this->paramspimpl->resetcount);
			return true;
		}
	}

	return false;
}

bool TigerEyeGrabber::getParameter(const GrabberParameter& param, float* values,
		unsigned int& size) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	return false;
}

bool TigerEyeGrabber::setParameter(const GrabberParameter& param,
		const float& value) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (this->iopimpl->local_streaming == false) {

		if (TIGEREYE_FRAMERATE == param) {
			return this->setFrameRate(value);
		}

		if (TIGEREYE_DETECTOR_GAIN == param) {
			return this->setDetectorGain(value);
		}

		if (TIGEREYE_AMPLIFIER_GAIN == param) {
			return this->setAmplifierGain(value);
		}

		if (TIGEREYE_THRESHOLD_FILTER == param) {
			return this->setThresholdFilter(value);
		}

		if (TIGEREYE_THRESHOLD_LEVEL == param) {
			return this->setThresholdLevel(value);
		}
	} else {
		if (TIGEREYE_FRAMERATE == param) {
			this->paramspimpl->framerate = value;
			return true;
		}
	}
	return false;
}

bool TigerEyeGrabber::trigger(const GrabberTrigger& trig) {

	if (false == this->paramspimpl->initialized) {
		return false;
	}

	if (this->iopimpl->local_streaming == false) {
		if (TIGEREYE_NUC == trig) {
			return this->setNUC();
		}

		if (TIGEREYE_RESET == trig) {
			this->reset();
			return true;
		}
	}

	if (TIGEREYE_INTERNAL_RECORD == trig) {
		if (this->iopimpl->recording == false) {
			if (false == is_directory(this->iopimpl->stream_filename)) {
				if (false
						== boost::algorithm::ends_with(
								this->iopimpl->stream_filename, "/")) {
					this->iopimpl->stream_filename.append("/");
				}
				if (false == create_directory(this->iopimpl->stream_filename)) {
					return false;
				}
			}
			this->iopimpl->recording = true;
		} else {
			this->iopimpl->recording = false;
		}
		return true;
	}

	return false;
}

bool TigerEyeGrabber::isGrabbing(void) const {
	return this->paramspimpl->running;
}

bool TigerEyeGrabber::hasColor(void) const {
	return this->paramspimpl->hascolor;
}

bool TigerEyeGrabber::hasIntensity(void) const {
	return this->paramspimpl->hasintensity;
}

bool TigerEyeGrabber::hasDepth(void) const {
	return this->paramspimpl->hasdepth;
}

void TigerEyeGrabber::pause(void) {
	this->paramspimpl->paused = !this->paramspimpl->paused;
	if (true == this->paramspimpl->paused) {
		emitEvent(this->threadpimpl->paused);
	}
}

void TigerEyeGrabber::play(void) {

	if (true == this->paramspimpl->initialized) {
		this->paramspimpl->paused = false;
		if (false == this->paramspimpl->running) {
			if (0 != this->threadpimpl->pthread) {
				this->threadpimpl->pthread->interrupt();
				delete this->threadpimpl->pthread;
				this->threadpimpl->pthread = 0;
			}
			this->threadpimpl->pthread = new boost::thread(
					boost::bind(&TigerEyeGrabber::run, this));
			millisecSleep(200);
			emitEvent(this->threadpimpl->playing);
		}
	}
}

void TigerEyeGrabber::stop(void) {
	if (true == this->paramspimpl->running) {
		this->paramspimpl->running = false;

		while (false == this->paramspimpl->finished) {
			millisecSleep(40);
		}
		if (0 != this->threadpimpl->pthread) {
			this->threadpimpl->pthread->interrupt();
			delete this->threadpimpl->pthread;
			this->threadpimpl->pthread = 0;
		}
		this->iopimpl->recording = false;
		emitEvent(this->threadpimpl->stopped);
	}
}

void TigerEyeGrabber::grabOneFrame(void) {
	if (true == this->iopimpl->local_streaming) {
		stop();
		readLocalFile();
		emitEvent(this->threadpimpl->updated);
	}
}

void TigerEyeGrabber::readLocalFile(void) {
	if (false == this->paramspimpl->paused) {
		std::string filename(
				this->iopimpl->file_list_local[this->iopimpl->frame_counter].string());
		std::ifstream import_file(filename.c_str(), std::ifstream::in);
		int x, y;
		float intensity = 0.0f;
		float depth = 0.0f;
		unsigned char* dest_intensity =
				this->capturepimpl->rgbdi->intensity.getData().get();
		float* dest_depth = this->capturepimpl->rgbdi->depth.getData().get();
		if (true == import_file.is_open()) {
			std::string line;
			while (getline(import_file, line)) {
				std::istringstream input(line);
				input >> x >> y >> depth >> intensity;
				int pos = y * 128 + x;
				dest_depth[pos] = Converter::feetToMeters(depth);
				dest_intensity[pos] = static_cast<unsigned char>(intensity
						* 0.062271062f);
			}
			import_file.close();
		}
		++this->iopimpl->frame_counter;
		if (this->iopimpl->frame_counter >= this->iopimpl->nb_frames) {
			this->iopimpl->frame_counter = 0;
		}
	}
}

void TigerEyeGrabber::run(void) {

	if (false == this->iopimpl->local_streaming) {
		if (true == this->paramspimpl->reset_device) {
			{
				boost::mutex::scoped_lock l(
						this->threadpimpl->mutex_command_without_long_read);
				memcpy(
						this->capturepimpl->packet_command_without_long_read
								+ COMMAND_POSITION, reset_command,
						SIZE_COMMAND * sizeof(unsigned char));
				if (-1
						== pcap_sendpacket(this->capturepimpl->descr,
								this->capturepimpl->packet_command_without_long_read,
								SIZE_PACKET)) {
					Log::add().error("TigerEyeGrabber::setFrameRate",
							"Couldn't send the reset command packet");
					return;
				}
			}
			this->paramspimpl->resetcount = 15;
			while (this->paramspimpl->resetcount > 0) {
				secSleep(1);
				--this->paramspimpl->resetcount;
			}
			this->paramspimpl->resetcount = 0;
			this->paramspimpl->reset_device = false;
			connectCommand();
			secSleep(1);
			takeOneImage();
			secSleep(1);

		}

		/*
		 * Send the Run at Rate command
		 */
		{
			boost::mutex::scoped_lock l(
					this->threadpimpl->mutex_command_without_long_read);
			memcpy(
					this->capturepimpl->packet_command_without_long_read
							+ COMMAND_POSITION, run_at_rate_command,
					SIZE_COMMAND * sizeof(unsigned char));
			if (-1
					== pcap_sendpacket(this->capturepimpl->descr,
							this->capturepimpl->packet_command_without_long_read,
							SIZE_PACKET)) {
				Log::add().error("TigerEyeGrabber::run",
						"Couldn't send the run at rate command packet");
				return;
			}
		}
	}

	struct pcap_pkthdr infos;
	const u_char *packet = 0;

	bool packet_captured[NUMBER_OF_PACKETS];
	unsigned char *packet_intensity;
	float *packet_depth;

	this->paramspimpl->finished = false;
	this->paramspimpl->running = true;

	std::ofstream export_file;

	Timer timer;
	timer.start();
	long int current_time;

	packet_intensity =
			new unsigned char[this->capturepimpl->rgbdi->intensity.getSize().width
					* this->capturepimpl->rgbdi->intensity.getSize().height];
	packet_depth =
			new float[this->capturepimpl->rgbdi->intensity.getSize().width
					* this->capturepimpl->rgbdi->intensity.getSize().height];

	while (true == this->paramspimpl->running) {
		current_time = timer.get();

		if (true == this->iopimpl->recording) {
			std::string filename = this->iopimpl->stream_filename
					+ IO::numberToString(getTimeStamp()) + ".txt";
			export_file.open(filename.c_str(), std::ofstream::out);
			if (false == export_file.is_open()) {
				Log::add().error("TigerEyeGrabber::run",
						"Unable to open the export file " + filename);
				this->iopimpl->recording = false;
			}
		}

		if (false == this->iopimpl->local_streaming) {
			/*
			 * Grab images from the device
			 */
			int number_packet = 0;
			packet = 0;

			memset(packet_captured, 0, NUMBER_OF_PACKETS * sizeof(bool));
			memset(packet_intensity, 0,
					this->capturepimpl->rgbdi->intensity.getSize().width
							* this->capturepimpl->rgbdi->intensity.getSize().height
							* sizeof(unsigned char));
			memset(packet_depth, 0,
					this->capturepimpl->rgbdi->depth.getSize().width
							* this->capturepimpl->rgbdi->depth.getSize().height
							* sizeof(float));

			do {

				packet = pcap_next(this->capturepimpl->descr, &infos);
				if (0 != packet && infos.len == SIZE_DATA_PACKET) {
					int address = getAddress(
							packet + SIZE_HEADER + SIZE_OPCODE);
					int packet_id = address / PIXELS_PER_PACKET;

					/*
					 * Check if we received a packet not belonging to the current image
					 * If it is the case, then we drop the current frame
					 */
					if (true == packet_captured[packet_id]) {
						memset(packet_captured, 0,
								NUMBER_OF_PACKETS * sizeof(bool));
						memset(packet_intensity, 0,
								this->capturepimpl->rgbdi->intensity.getSize().width
										* this->capturepimpl->rgbdi->intensity.getSize().height
										* sizeof(unsigned char));
						memset(packet_depth, 0,
								this->capturepimpl->rgbdi->depth.getSize().width
										* this->capturepimpl->rgbdi->depth.getSize().height
										* sizeof(float));
						number_packet = 0;

#ifdef DEBUG
						Log::add().info("A packet has been dropped.");
#endif
					}

					/*
					 * Tag the packet as received
					 */
					packet_captured[packet_id] = true;

					/*
					 * The last packet of the frame is smaller than the other ones
					 */
					unsigned int max_pixels_to_read = PIXELS_PER_PACKET;
					if (0x3ff0 == address) {
						max_pixels_to_read = 128 * 128
								- PIXELS_PER_PACKET * (NUMBER_OF_PACKETS - 1);
					}

					/*
					 * For each pixel inside of the packet
					 */
					for (unsigned int i = 0; i < max_pixels_to_read; ++i) {

						unsigned int y_pos = (address + i) / 128;
						unsigned int x_pos = (address + i) % 128;
						unsigned int pos = (127 - y_pos) * 128 + x_pos;
						float intensity = 0.0f;
						float depth = 0.0f;
						getIntensityDepth(packet + 0x0016 + i * SIZE_DWORD,
								intensity, depth);

						packet_depth[pos] = Converter::feetToMeters(depth);
						packet_intensity[pos] = intensity * 0.062271062f; //(1.0f / 4095.0f * 255.0f)

						if (true == this->iopimpl->recording) {
#ifdef WIN32
							export_file << x_pos << " " << 127 - y_pos << " "
							<< depth << " " << intensity << "\r\n";
#else
							export_file << x_pos << " " << 127 - y_pos << " "
									<< depth << " " << intensity << "\n";
#endif

						}
					}

					if (0x3ff0 == address) {
						/*
						 * Get extra data
						 */
						updateStatus(packet + 0x00d6);
					}

					++number_packet;
				}
			} while (packet != 0 && number_packet < NUMBER_OF_PACKETS);

			if (false == this->paramspimpl->paused) {
				unsigned char* dest_color =
						this->capturepimpl->rgbdi->intensity.getData().get();
				float* dest_depth =
						this->capturepimpl->rgbdi->depth.getData().get();

				{
					boost::mutex::scoped_lock l(this->threadpimpl->mutex);
					memcpy(dest_color, packet_intensity,
							this->capturepimpl->rgbdi->intensity.getSize().width
									* this->capturepimpl->rgbdi->intensity.getSize().height
									* sizeof(unsigned char));
					memcpy(dest_depth, packet_depth,
							this->capturepimpl->rgbdi->depth.getSize().width
									* this->capturepimpl->rgbdi->depth.getSize().height
									* sizeof(float));
				}
			}
		} else {
			readLocalFile();
		}

		if (false == this->paramspimpl->paused) {
			if (true == this->iopimpl->recording) {
				export_file.close();
			}

			/*
			 * Emit a message to confirm the update
			 */
			emitEvent(this->threadpimpl->updated);
		}

		current_time = long(1000.0 / this->paramspimpl->adaptative_framerate)
				- (timer.get() - current_time);

		if (current_time > 0) {
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(current_time));
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate;
		} else {
			this->paramspimpl->adaptative_framerate =
					this->paramspimpl->framerate - current_time;
		}

	}
	delete[] packet_intensity;
	delete[] packet_depth;
	if (false == this->iopimpl->local_streaming) {
		/*
		 * Send the stop command
		 */
		memcpy(this->capturepimpl->packet_command + COMMAND_POSITION,
				stop_command, SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command, SIZE_PACKET)) {
			throw Exception("TigerEyeGrabber::run",
					"Unable to stop the device");
		}

		/*
		 * Wait until the confirmation
		 */
		bool valid_packet = true;
		do {
			packet = pcap_next(this->capturepimpl->descr, &infos);
			if (infos.len == SIZE_REPLY_PACKET) {
				valid_packet = checkPacket(
						this->capturepimpl->local_mac_address, packet);
			}
		} while (false == valid_packet);
	}

	timer.stop();
	this->paramspimpl->finished = true;

}

void TigerEyeGrabber::reset(void) {
	this->paramspimpl->reset_device = true;
	bool restart = this->paramspimpl->running;
	this->stop();
	while (false == this->paramspimpl->finished) {
		millisecSleep(10);
	}
	if (true == restart) {
		this->play();
	}
}

bool TigerEyeGrabber::setNUC(void) {

	if (true == this->paramspimpl->running) {
		Log::add().error("TigerEyeGrabber::setNUC",
				"Stop the device before activating the Non Uniformity correction");
		return false;
	}

	if (-1 == pcap_sendpacket(this->capturepimpl->descr, NUC_command,
	SIZE_PACKET)) {
		Log::add().error("TigerEyeGrabber::setNUC",
				"Couldn't send the activate the Non Uniformity correction command packet");
		return false;
	}
	millisecSleep(200);

	return true;
}

bool TigerEyeGrabber::setFrameRate(const float& frequency) {

	/*
	 * range of the frequency has to be between 1.01Hz and 20Hz
	 */
	if (1.01f > frequency || 20.0f < frequency) {
		Log::add().error("TigerEyeGrabber::setFrameRate",
				"The frequency of the frame-rate has to be in the range [1.01 - 20]");
		return false;
	}

	if (true == this->paramspimpl->running) {
		Log::add().error("TigerEyeGrabber::setFrameRate",
				"Stop the device before changing the frame rate");
		return false;
	}

	this->paramspimpl->framerate = frequency;
	this->paramspimpl->adaptative_framerate = frequency;

	int conversion = static_cast<int>(std::floor(1.0f / (frequency * .00001504))
			- 1.0f);

	this->paramspimpl->set_framerate_command[0] = ((conversion >> 8) & 255);
	this->paramspimpl->set_framerate_command[1] = (conversion & 255);
	this->paramspimpl->set_framerate_command[4] =
			this->paramspimpl->set_framerate_command[0];
	this->paramspimpl->set_framerate_command[5] =
			this->paramspimpl->set_framerate_command[1];

#ifdef DEBUG
	std::stringstream ss;
	for (unsigned int i = 0; i < SIZE_COMMAND; ++i) {
		ss << std::hex << std::uppercase
		<< (int) (this->paramspimpl->set_framerate_command[i]) << std::dec << " ";
	}
	Log::add().debug("TigerEyeGrabber::setFrameRate",
			"Input frequency: " + IO::numberToString(frequency)
			+ ", Converted in: " + IO::numberToString(conversion)
			+ ", and in Hex: " + ss.str());
#endif

	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION,
				this->paramspimpl->set_framerate_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setFrameRate",
					"Couldn't send the set framerate command packet");
			return false;
		}
		millisecSleep(200);
	}

	return true;
}

bool TigerEyeGrabber::setDetectorGain(const float& voltage) {

	/*
	 * range of the gain has to be between 16.335V and 55.004V
	 */
	if (0.0f > voltage || 100.0f < voltage) {
		Log::add().error("TigerEyeGrabber::setDetectorGain",
				"The gain of the Detector has to be in the range [16.335 - 55.004]");
		return false;
	}

	this->paramspimpl->detector_gain = voltage;

	float converted_voltage = (voltage * 0.01f * (55.004f - 16.335f)) + 16.335f;

	int conversion = static_cast<int>(std::floor(
			(converted_voltage + 3.0f) / 28.0f / 0.00122f));

	this->paramspimpl->set_detector_gain_command[0] = ((conversion >> 8) & 255);
	this->paramspimpl->set_detector_gain_command[1] = (conversion & 255);
	this->paramspimpl->set_detector_gain_command[4] =
			this->paramspimpl->set_detector_gain_command[0];
	this->paramspimpl->set_detector_gain_command[5] =
			this->paramspimpl->set_detector_gain_command[1];

#ifdef DEBUG
	std::stringstream ss;
	for (unsigned int i = 0; i < SIZE_COMMAND; ++i) {
		ss << std::hex << std::uppercase
		<< (int) (this->paramspimpl->set_detector_gain_command[i]) << std::dec
		<< " ";
	}
	Log::add().debug("TigerEyeGrabber::setDetectorGain",
			"Input gain: " + IO::numberToString(voltage) + ", Converted in: "
			+ IO::numberToString(conversion) + ", and in Hex: "
			+ ss.str());
#endif

	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION,
				this->paramspimpl->set_detector_gain_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setDetectorGain",
					"Couldn't send the set framerate command packet");
			return false;
		}
		millisecSleep(100);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION, update_voltages_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setThresholdLevel",
					"Couldn't send the update voltage command packet");
			return false;
		}
		millisecSleep(200);
	}

	return true;
}

bool TigerEyeGrabber::setAmplifierGain(const float& voltage) {

	/*
	 * range of the gain has to be between 16.335V and 55.004V
	 */
	if (0.0f > voltage || 100.0f < voltage) {
		Log::add().error("TigerEyeGrabber::setAmplifierGain",
				"The amplifier gain has to be in the range [1.4 - 1.883]");
		return false;
	}

	this->paramspimpl->amplifier_gain = voltage;

	float converted_voltage = (voltage * 0.01f * (1.883f - 1.4f)) + 1.4f;

	int conversion = static_cast<int>(std::floor(
			converted_voltage / 0.00061 + 0x2000));

	this->paramspimpl->set_amplifier_gain_command[0] =
			((conversion >> 8) & 255);
	this->paramspimpl->set_amplifier_gain_command[1] = (conversion & 255);
	this->paramspimpl->set_amplifier_gain_command[4] =
			this->paramspimpl->set_amplifier_gain_command[0];
	this->paramspimpl->set_amplifier_gain_command[5] =
			this->paramspimpl->set_amplifier_gain_command[1];

#ifdef DEBUG
	std::stringstream ss;
	for (unsigned int i = 0; i < SIZE_COMMAND; ++i) {
		ss << std::hex << std::uppercase
		<< (int) (this->paramspimpl->set_amplifier_gain_command[i]) << std::dec
		<< " ";
	}
	Log::add().debug("TigerEyeGrabber::setAmplifierGain",
			"Input voltage: " + IO::numberToString(voltage)
			+ ", Converted in: " + IO::numberToString(conversion)
			+ ", and in Hex: " + ss.str());
#endif

	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION,
				this->paramspimpl->set_amplifier_gain_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setAmplifierGain",
					"Couldn't send the amplifier gain command packet");
			return false;
		}
		millisecSleep(100);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION, update_voltages_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setAmplifierGain",
					"Couldn't send the update voltage command packet");
			return false;
		}
		millisecSleep(200);
	}

	return true;
}

bool TigerEyeGrabber::setThresholdFilter(const float& voltage) {

	/*
	 * range of the gain has to be between 0.4V and 0.883V
	 */
	if (0.0f > voltage || 100.0f < voltage) {
		Log::add().error("TigerEyeGrabber::setThresholdFilter",
				"The threshold filter has to be in the range [0.4 - 0.883]");
		return false;
	}

	this->paramspimpl->threshold_filter = voltage;

	float converted_voltage = (voltage * 0.01f * (0.883f - 0.4f)) + 0.4f;

	int conversion = static_cast<int>(std::floor(
			converted_voltage / 0.00061 + 0x3000));

	this->paramspimpl->set_threshold_filter_command[0] = ((conversion >> 8)
			& 255);
	this->paramspimpl->set_threshold_filter_command[1] = (conversion & 255);
	this->paramspimpl->set_threshold_filter_command[4] =
			this->paramspimpl->set_threshold_filter_command[0];
	this->paramspimpl->set_threshold_filter_command[5] =
			this->paramspimpl->set_threshold_filter_command[1];

#ifdef DEBUG
	std::stringstream ss;
	for (unsigned int i = 0; i < SIZE_COMMAND; ++i) {
		ss << std::hex << std::uppercase
		<< (int) (this->paramspimpl->set_threshold_filter_command[i]) << std::dec
		<< " ";
	}
	Log::add().debug("TigerEyeGrabber::setThresholdFilter",
			"Input voltage: " + IO::numberToString(voltage)
			+ ", Converted in: " + IO::numberToString(conversion)
			+ ", and in Hex: " + ss.str());
#endif

	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION,
				this->paramspimpl->set_threshold_filter_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setThresholdFilter",
					"Couldn't send the amplifier gain command packet");
			return false;
		}
		millisecSleep(100);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION, update_voltages_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setThresholdLevel",
					"Couldn't send the update voltage command packet");
			return false;
		}
		millisecSleep(200);
	}

	return true;
}

bool TigerEyeGrabber::setThresholdLevel(const float& voltage) {

	/*
	 * range of the gain has to be between 1.0V and 1.483V
	 */
	if (0.0f > voltage || 100.0f < voltage) {
		Log::add().error("TigerEyeGrabber::setThresholdLevel",
				"The threshold level has to be in the range [0.4 - 1.483]");
		return false;
	}

	this->paramspimpl->threshold_level = voltage;

	float converted_voltage = (voltage * 0.01f * (1.483f - 1.0f)) + 1.0f;

	int conversion = static_cast<int>(std::floor(
			converted_voltage / 0.00061 + 0xF000));

	this->paramspimpl->set_threshold_level_command[0] =
			((conversion >> 8) & 255);
	this->paramspimpl->set_threshold_level_command[1] = (conversion & 255);
	this->paramspimpl->set_threshold_level_command[4] =
			this->paramspimpl->set_threshold_level_command[0];
	this->paramspimpl->set_threshold_level_command[5] =
			this->paramspimpl->set_threshold_level_command[1];

#ifdef DEBUG
	std::stringstream ss;
	for (unsigned int i = 0; i < SIZE_COMMAND; ++i) {
		ss << std::hex << std::uppercase
		<< (int) (this->paramspimpl->set_threshold_level_command[i]) << std::dec
		<< " ";
	}
	Log::add().debug("TigerEyeGrabber::setThresholdLevel",
			"Input voltage: " + IO::numberToString(voltage)
			+ ", Converted in: " + IO::numberToString(conversion)
			+ ", and in Hex: " + ss.str());
#endif

	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION,
				this->paramspimpl->set_threshold_level_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setThresholdLevel",
					"Couldn't send the amplifier level command packet");
			return false;
		}
		millisecSleep(100);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION, update_voltages_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::setThresholdLevel",
					"Couldn't send the update voltage command packet");
			return false;
		}
		millisecSleep(200);
	}

	return true;
}

/*
 * This function is called to extract the address of the data corresponding
 * The address is 3 bytes long in the packet, but return as an int relatively to the first stored element (position 0x100000)
 * c.f. EtherIO proc. v2.2 pp. 22
 */
int getAddress(const unsigned char * packet) {
	int address = 0;
	for (unsigned int i = 0; i < 4; ++i) {
		address |= (packet[i] << (8 * (3 - i)));
	}

	//define the address relative to the starting address
	return address - 0x100000;
}

/*
 * This function is called to extract the the intensity plus depth for one pixel
 * Intensity + depth is stored in 4 bytes.
 * 14 bits for the range in feet
 * 6 bits for the fractional part of the range
 * 12 bits for the instensity
 * The function return the depth in meters and the intensity in the range [0-255]
 * c.f. EtherIO proc. v2.2 pp. 22
 */
void getIntensityDepth(const unsigned char * packet, float& intensity,
		float& depth) {

	int range = 0;
	range |= packet[3] << 8;
	range |= packet[2];
	range = range >> 2;

	int fractional_feet = 0;
	fractional_feet |= ((packet[2] & 3) << 8);
	fractional_feet |= packet[1];
	fractional_feet = fractional_feet >> 4;
	depth = static_cast<float>(range)
			+ static_cast<float>(fractional_feet) / 64.0f;
	//depth = Converter::feetToMeters(depth); //feet to meters

	int intensity_local = 0;
	intensity_local |= ((packet[1] & 15) << 8);
	intensity_local |= packet[0];
	intensity = static_cast<float>(intensity_local); // / 4095.0f * 255.0f); // [0-4095] to [0-255]
}

bool checkPacket(const unsigned char* sent, const unsigned char* received) {
	bool valid = true;
	if (0 == sent || 0 == received) {
		Log::add().error("checkPacket", "Invalid parameters");
		return false;
	}
	// Is it a long read reply?
	for (unsigned int i = 0; i < SIZE_LONG_READ && true == valid; ++i) {
		valid = (received[SIZE_HEADER + i] == long_read_reply[i]);
	}

	if (valid == false) {

		Log::add().error("checkPacket", "Invalid long read reply data");
	}

	// Check the validity of the confirmation (data are reversed in the received packet)
	for (unsigned int i = 0; i < SIZE_COMMAND && true == valid; ++i) {
		valid = (received[SIZE_HEADER + SIZE_LONG_READ + SIZE_COMMAND - 1 - i]
				== sent[i]);
	}
	if (valid == false) {
		Log::add().error("checkPacket", "Invalid echo");
	}
	return valid;
}

void TigerEyeGrabber::updateStatus(const unsigned char * data) {

	int value = 0;
	value |= data[1] << 8;
	value |= data[0];
	this->paramspimpl->detector_gain = (static_cast<float>(value & 0x0FFF)
			* 0.00122 * 28.0f) - 3.0f;
	// Conversion  in percentage
	this->paramspimpl->detector_gain = (this->paramspimpl->detector_gain
			- 16.335f) * 100.0f / (55.004f - 16.335f);

	value = 0;
	value |= data[9] << 8;
	value |= data[8];
	this->paramspimpl->amplifier_gain = (static_cast<float>(value & 0x0FFF)
			* 0.00061f);
	// Conversion in percentage
	this->paramspimpl->amplifier_gain = (this->paramspimpl->amplifier_gain
			- 1.4f) * 100.0f / (1.883f - 1.4f);

	value = 0;
	value |= data[13] << 8;
	value |= data[12];
	this->paramspimpl->threshold_filter = (static_cast<float>(value & 0x0FFF)
			* 0.00061f);
	// Conversion in percentage
	this->paramspimpl->threshold_filter = (this->paramspimpl->threshold_filter
			- 0.4f) * 100.0f / (0.883f - 0.4f);

	value = 0;
	value |= data[61] << 8;
	value |= data[60];
	this->paramspimpl->threshold_level = (static_cast<float>(value & 0x0FFF)
			* 0.00061f);
	// Conversion in percentage
	this->paramspimpl->threshold_level = (this->paramspimpl->threshold_level
			- 1.0f) * 100.0f / (1.483f - 1.0f);

	value = 0;
	value |= data[131] << 8;
	value |= data[130];
	this->paramspimpl->laser_drive = (static_cast<float>(value & 0x7fff) * 0.02f
			/ 350.0f);

	value = 0;
	value |= data[133] << 8;
	value |= data[132];
	float V = static_cast<float>(value & 0x0fff) * 0.00122f;
	float R = (V * 4990.0f) / (5.0f - V);
	this->paramspimpl->sensor_temperature = (1.0f
			/ (1.2874e-3 + 2.3573e-4 * log(R)
					+ 9.5053e-8 * (std::pow(log(R), 3.0f)))) - 273.15f;

	value = 0;
	value |= data[135] << 8;
	value |= data[134];
	V = static_cast<float>(value & 0x0fff) * 0.00122f;
	R = (V * 4990.0f) / (5.0f - V);
	this->paramspimpl->laser_temperature = (1.0f
			/ (1.2874e-3 + 2.3573e-4 * log(R)
					+ 9.5053e-8 * (std::pow(log(R), 3.0f)))) - 273.15f;

	value = 0;
	value |= data[137] << 8;
	value |= data[136];
	this->paramspimpl->sensor_voltage = static_cast<float>(value & 0x0fff)
			* 0.00244;

	value = 0;
	value |= data[139] << 8;
	value |= data[138];
	V = static_cast<float>(value & 0x0fff) * 0.00122f;
	R = (V * 4990.0f) / (5.0f - V);
	this->paramspimpl->sensor_heat_sink_temperature = (1.0f
			/ (1.2874e-3 + 2.3573e-4 * log(R)
					+ 9.5053e-8 * (std::pow(log(R), 3.0f)))) - 273.15f;

	value = 0;
	value |= data[141] << 8;
	value |= data[140];
	V = static_cast<float>(value & 0x0fff) * 0.00122f;
	R = (V * 4990.0f) / (5.0f - V);
	this->paramspimpl->laser_heat_sink_temperature = (1.0f
			/ (1.2874e-3 + 2.3573e-4 * log(R)
					+ 9.5053e-8 * (std::pow(log(R), 3.0f)))) - 273.15f;

	value = 0;
	value |= data[143] << 8;
	value |= data[142];
	V = static_cast<float>(value & 0x0fff) * 0.00122f;
	R = (V * 4990.0f) / (5.0f - V);
	this->paramspimpl->laser_diod_temperature = (1.0f
			/ (1.2874e-3 + 2.3573e-4 * log(R)
					+ 9.5053e-8 * (std::pow(log(R), 3.0f)))) - 273.15f;

	value = 0;
	value |= data[145] << 8;
	value |= data[144];
	this->paramspimpl->laser_capacity_voltage = static_cast<float>(value
			& 0x0fff) * 0.0244;

	value = 0;
	value |= data[147] << 8;
	value |= data[146];
	this->paramspimpl->process_board_temperature = (static_cast<float>(value
			& 0x0fff) * 0.00122 - 0.5f) * 100.0f;
}

void TigerEyeGrabber::setStreamFilename(const std::string& filename) {
	this->iopimpl->stream_filename = filename;
}

std::string TigerEyeGrabber::getStreamFilename(void) const {
	return this->iopimpl->stream_filename;
}

bool TigerEyeGrabber::takeOneImage(void) {
	{
		boost::mutex::scoped_lock l(
				this->threadpimpl->mutex_command_without_long_read);
		memcpy(
				this->capturepimpl->packet_command_without_long_read
						+ COMMAND_POSITION, take_one_image_command,
				SIZE_COMMAND * sizeof(unsigned char));
		if (-1
				== pcap_sendpacket(this->capturepimpl->descr,
						this->capturepimpl->packet_command_without_long_read,
						SIZE_PACKET)) {
			Log::add().error("TigerEyeGrabber::init",
					"Couldn't send the capture one single frame command packet");
			return false;
		}
	}

	/*
	 * Get initial parameters from the camera
	 */
	const u_char *packet = 0;
	struct pcap_pkthdr infos;
	do {
		packet = pcap_next(this->capturepimpl->descr, &infos);
		if (0 != packet && infos.len == SIZE_DATA_PACKET) {
			int address = getAddress(packet);
			if (0x3ff0 == address) {
				updateStatus(packet + 0x00d6);
			}
		}
	} while (packet != 0);

	return true;
}

bool TigerEyeGrabber::connectCommand(void) {

	unsigned char packet_write_dma[SIZE_PACKET];

	for (unsigned int i = 0; i < SIZE_ADDRESS * 2 + 2; ++i) {
		packet_write_dma[i] = this->paramspimpl->header[i];
	}

	/*
	 * Create the write dma packet
	 */
	memcpy(packet_write_dma + SIZE_HEADER, opcode_write_dma,
			SIZE_OPCODE * sizeof(unsigned char));
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE] =
			this->paramspimpl->header[SIZE_ADDRESS + 2];
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 1] =
			this->paramspimpl->header[SIZE_ADDRESS + 3];
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 2] =
			this->paramspimpl->header[SIZE_ADDRESS + 4];
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 3] =
			this->paramspimpl->header[SIZE_ADDRESS + 5];
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 4] = '\x00';
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 5] = '\x00';
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 6] =
			this->paramspimpl->header[SIZE_ADDRESS];
	packet_write_dma[SIZE_HEADER + SIZE_OPCODE + 7] =
			this->paramspimpl->header[SIZE_ADDRESS + 1];
	memcpy(packet_write_dma + SIZE_HEADER + SIZE_OPCODE + SIZE_COMMAND,
			long_read, SIZE_LONG_READ * sizeof(unsigned char));

	/*
	 * Let try to send the write dma packet
	 */
	if (-1 == pcap_sendpacket(this->capturepimpl->descr, packet_write_dma,
	SIZE_PACKET)) {
		throw Exception("TigerEyeGrabber::init", "Couldn't send the packet");
	}

	/*
	 * Let read the answer and confirm the connection
	 */
	struct pcap_pkthdr infos;
	const u_char *packet = 0;
	packet = pcap_next(this->capturepimpl->descr, &infos);
	if (infos.len == SIZE_REPLY_PACKET) {
		return checkPacket(this->capturepimpl->local_mac_address, packet);
	}

	return false;

}

}
