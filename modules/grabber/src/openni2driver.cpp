/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opennidriver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opennidriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/openni2driver.hpp"
#include <OpenNI.h>

namespace gedeon {

OpenNI2Driver OpenNI2Driver::driver_singleton;

OpenNI2Driver::OpenNI2Driver(void) :
		initialized(false) {

}

OpenNI2Driver::~OpenNI2Driver(void) {

	this->names.clear();
	openni::OpenNI::shutdown();
}

bool OpenNI2Driver::refresh(void) {

	this->initialized = false;

	openni::Status rc = openni::STATUS_OK;
	rc = openni::OpenNI::initialize();
	if (openni::STATUS_OK != rc) {
		Log::add().error("OpenNIDriver::init", openni::OpenNI::getExtendedError());
		return false;
	}

	/*
	 * Get the list of the devices
	 */
	openni::Array<openni::DeviceInfo> deviceList;
	openni::OpenNI::enumerateDevices(&deviceList);

	/*
	 * Get the available OpenNI devices
	 */
	std::vector<std::string> local_names;
	for (int i = 0; i < deviceList.getSize(); ++i) {
		const openni::DeviceInfo& di = deviceList[i];
		std::string name(
					std::string(di.getVendor()) + " "
							+ std::string(di.getName()) + "[" + std::string(di.getUri()) + "]");
		local_names.push_back(name);
	}

	for (std::vector<std::string>::iterator it = this->names.begin();
			it != this->names.end(); ++it) {
		// The device is no more connected, so we remove it
		if (local_names.end()
				== std::find(local_names.begin(), local_names.end(), *it)) {
			it = this->names.erase(it);
		}
	}
	for (std::vector<std::string>::iterator it = local_names.begin();
			it != local_names.end(); ++it) {
		// Add new devices
		if (this->names.end()
				== std::find(this->names.begin(), this->names.end(), *it)) {
			this->names.push_back(*it);
		}
	}

	local_names.clear();
	this->initialized = true;

	return true;
}

}
