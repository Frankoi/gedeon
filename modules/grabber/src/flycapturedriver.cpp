/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * flycapturedriver.cpp created in 09 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * flycapturedriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/flycapturedriver.hpp"

#include <FlyCapture2.h>

namespace gedeon {

FlyCaptureDriver FlyCaptureDriver::driver_singleton;

FlyCaptureDriver::FlyCaptureDriver(void) :
		initialized(false) {
}

FlyCaptureDriver::~FlyCaptureDriver(void) {

	this->names.clear();

}

bool FlyCaptureDriver::refresh(void) {

	this->initialized = false;
	this->names.clear();

	/*
	 * Get number of connected cameras
	 */
	FlyCapture2::BusManager busMgr;
	unsigned int numCameras;
	FlyCapture2::Error m_error = busMgr.GetNumOfCameras(&numCameras);
	if(m_error != FlyCapture2::PGRERROR_OK){
		Log::add().error("PointGreyDriver::refresh","Could not access the number of connected devices");
		m_error.PrintErrorTrace();
		return false;
	}

	/*
	 * Get Cameras
	 */
	for (unsigned int i=0; i < numCameras; ++i){
		this->names.push_back(std::string("ID ") + IO::numberToString(i));
	}
	this->initialized = true;
	return true;
}

}
