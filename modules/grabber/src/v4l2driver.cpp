/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * v4l2driver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * v4l2driver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/v4l2driver.hpp"

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <linux/videodev2.h>

namespace gedeon {

V4L2Driver V4L2Driver::driver_singleton;

V4L2Driver::V4L2Driver(void) :
		initialized(false) {
	device_names.push_back("/dev/video0");
	device_names.push_back("/dev/video1");
	device_names.push_back("/dev/video2");
	device_names.push_back("/dev/video3");
	device_names.push_back("/dev/video4");
	device_names.push_back("/dev/video5");
	device_names.push_back("/dev/video6");
	device_names.push_back("/dev/video7");
	device_names.push_back("/dev/video8");
	device_names.push_back("/dev/video9");
}

V4L2Driver::~V4L2Driver(void) {

	this->names.clear();
	this->device_names.clear();

}

bool V4L2Driver::refresh(void) {

	this->initialized = false;

	this->names.clear();

	/*
	 * Try to open the cameras with an id
	 */
	int id = 0;
	bool found = false;

	do {
		found = false;
		struct stat st;
		if (-1 != stat(device_names[id].c_str(), &st)) {
			if (S_ISCHR(st.st_mode)) {
				int fd = open(device_names[id].c_str(), O_RDWR | O_NONBLOCK, 0);
				if (fd != -1) {
					this->names.push_back(
							std::string("ID ") + IO::numberToString(id));
					found = true;
					close(fd);
				}
			}
		}
		++id;
	} while (true == found);

	this->initialized = true;

	return true;
}

}
