/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthsensedriver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthsensedriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/grabber/depthsensedriver.hpp"
#include <XnLog.h>
#include <XnTypes.h>

namespace gedeon {

DepthSenseDriver DepthSenseDriver::driver_singleton;

DepthSenseDriver::DepthSenseDriver(void) :
		initialized(false) {

	try{
		this->g_context = DepthSense::Context::create();
	}catch(DepthSense::Exception e){
		throw Exception("DepthSenseDriver::DepthSenseDriver",e.getMessage());
	}
	pthread = new boost::thread(boost::bind(&DepthSenseDriver::run, this));
}

DepthSenseDriver::~DepthSenseDriver(void) {
	this->names.clear();
	this->g_context.stopNodes();
	this->g_context.quit();

}

bool DepthSenseDriver::refresh(void) {

	this->initialized = false;
	std::vector<DepthSense::Device> da = g_context.getDevices();

	std::vector<DepthSense::Device>::iterator it = da.begin();
	while(it != da.end()){
		names.push_back(this->getName() + " " + it->getSerialNumber());
		++it;
	}

	this->initialized = true;
	return true;
}

void DepthSenseDriver::run(void) {
	this->g_context.startNodes();
	this->g_context.run();
}


}
