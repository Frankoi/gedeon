/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * opencvgrabber.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * opencvgrabber.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_GRABBER__OPENCVGRABBER_HPP__
#define GEDEON_GRABBER__OPENCVGRABBER_HPP__

#include "gedeon/grabber/grabbergeneric.hpp"
#include "gedeon/core/core.hpp"
#include <boost/thread.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace gedeon{

class OpenCVGrabberCapture;
class OpenCVGrabberParams;
class OpenCVGrabberThread;
class OpenCVGrabberIO;


/**
 * \brief Class for communicating with an OpenCV Camera
 *
 * \author Francois de Sorbier
 */

class OpenCVGrabber: public Grabber, public EventSender, public EventReceiver{

public:

	/**
	 * \brief Constructor of the class
	 *
	 */
	OpenCVGrabber(void);

	/**
	 * \brief Initialize the grabber
	 *
	 * \param id the id of the device based on its position in the list returned by the driver
	 * \param driver the driver for the OpenNI Camera
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	bool init(const unsigned int & id, Driver* driver);

	/**
	 * \brief Initialize the grabber with a file or directory
	 *
	 * \param source the name of a valid file or directory
	 * \param driver the driver for the OpenNI Camera
	 *
	 * \return true if the initialization is fine, 0 otherwise
	 */
	bool init(const std::string& source, Driver* driver);

	/**
	 * \brief Destructor of the class
	 */
	~OpenCVGrabber(void);

	/**
	 * \brief Return the specific name of the device defined as the usb address
	 *
	 * \return the name of the device
	 */
	std::string getName(void) const;

	/**
	 * \brief Return the id of the device referencing its position in the driver's list
	 *
	 * \return the id
	 */
	unsigned int getID(void) const;

	/**
	 * \brief Return the rgbdi image captured by the device
	 *
	 * There is no color for this camera
	 *
	 * \return The RGBDI image
	 */
	const RGBDIImagePtr& getImage(void) const;

	/**
	 * \brief Return the value related to one parameter of the device
	 *
	 * The parameters can be
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] value is set with the value of the parameter
	 *
	 * \sa GrabberParameter
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool getParameter(const GrabberParameter& param, float& value);

	/**
	 * \brief Return the value related to several parameters of the device
	 *
	 * \param param defines the parameter we want to obtain the value
	 * \param[out] values is set with the values of the parameter. The array has to be allocated beforehand.
	 * \param[out] size size of the array
	 *
	 * \sa GrabberParameter
	 * \return true if the parameter is found, false otherwise
	 */
	bool getParameter(const GrabberParameter& param, float* values, unsigned int& size);

	/**
	 * \brief Set a parameter of the device with a given value
	 *
	 * The parameters can be
	 *
	 * \param param defines the parameter we want to set with the value
	 * \param value used for setting the parameter
	 *
	 * \sa GrabberParameter
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool setParameter(const GrabberParameter& param, const float& value);

	/**
	 * \brief Trigger a functionality of the device
	 *
	 * The parameters can be
	 *
	 * \sa GrabberTrigger
	 *
	 * \return true if the parameter is found, false otherwise
	 */
	bool trigger(const GrabberTrigger& trig);

	/**
	 * \brief Return if the capture is currently active or not
	 *
	 * \return true if capture is active, false otherwise
	 */
	bool isGrabbing(void) const;

	/**
	 * \brief Return if the camera has a color image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasColor(void) const;

	/**
	 * \brief Return if the camera has a intensity image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasIntensity(void) const;

	/**
	 * \brief Return if the camera has a depth image
	 *
	 * \return true if there is one, false otherwise
	 */
	bool hasDepth(void) const;

public:

	/**
	 * \brief Pause the capture.
	 *
	 * The device is still active.
	 * the function can be called again to unpause the capture.
	 */
	void pause(void);

	/**
	 * \brief Start the capture.
	 *
	 * The function can also unpause the device.
	 */
	void play(void);

	/**
	 * \brief Stop the capture.
	 *
	 * The device is no more active.
	 */
	void stop(void);

	/**
	 * \brief Grab one single image
	 *
	 * This functionality is only working with recorded data and may not be always available
	 *
	 * While grabbing one single image any currently playing is stopped
	 *
	 */
	void grabOneFrame(void);

	/**
	 * \brief set the stream filename for reading or recording
	 *
	 * \param filename The name of the recording/reading file
	 */
	void setStreamFilename(const std::string& filename);

	/**
	 * \brief get the stream filename for reading or recording
	 *
	 * \return the current filename
	 */
	std::string getStreamFilename(void) const;

	/**
	 * \brief Change the resolution of the captured images.
	 *
	 * \param w The new width
	 * \param h The new height
	 *
	 * \return if the resolution change succeeded or not
	 */
	bool setResolution(const unsigned int& w, const unsigned int& h);

	/**
	 * \brief Define the codec to use for the internal recording
	 *
	 * It is based on the FourCC code http://www.fourcc.org/codecs.php
	 *
	 * \param c1 character code 1
	 * \param c2 character code 2
	 * \param c3 character code 3
	 * \param c4 character code 4
	 *
	 */
	void setCodecFourCC(const char& c1, const char& c2, const char& c3, const char& c4);

private:

	/**
	 * \brief Change the framerate of the captured images.
	 *
	 * \param fr the new framerate
	 *
	 * \return if the framerate change succeeded or not
	 */
	bool setFramerate(const double& fr);

	void run(void);

private:

	OpenCVGrabberCapture *capturepimpl;
	OpenCVGrabberParams *paramspimpl;
	OpenCVGrabberThread *threadpimpl;
	OpenCVGrabberIO *iopimpl;

};

}


#endif
