/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * openni2driver.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * openni2driver.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRABBER__OPENNI2_DRIVER_HPP__
#define GEDEON_GRABBER__OPENNI2_DRIVER_HPP__

#include "gedeon/grabber/driver.hpp"

namespace gedeon {

/**
 * \brief Class for finding the available OpenNI2 cameras
 *
 * \author Francois de Sorbier
 */
class OpenNI2Driver: public Driver {

private:

	/**
	 * \brief Constructor
	 *
	 */
	OpenNI2Driver(void);

	/**
	 * \brief Copy constructor
	 * \param dsd the object to use for construction
	 *
	 */
	OpenNI2Driver(const OpenNI2Driver& dsd){}

	/**
	 * \brief Destructor
	 *
	 */
	~OpenNI2Driver(void);

	/**
	 * \brief Copy operator
	 * \param dsd the object to copy
	 * \return this updated object
	 *
	 */
	OpenNI2Driver& operator=(const OpenNI2Driver& dsd){ return *this;}

public:

	/**
	 * \brief Return a unique instance of this object
	 *
	 * \return An unique instance of this object
	 *
	 */
	static inline OpenNI2Driver& getInstance(void) {
		return driver_singleton;
	}

	/**
	 * \brief Ask to scan the network interface for finding the available cameras
	 *
	 * \return true or false if something went wrong or not
	 *
	 */
	bool refresh(void);

	/**
	 * \brief Give the name of the driver
	 *
	 * \return the name of the driver
	 *
	 */
	std::string getName(void) const {
		return std::string("OpenNI");
	}

	/**
	 * \brief Return the list of available grabbers
	 *
	 * \return Return a list of grabber names defined by a unique usb port address
	 *
	 */
	std::vector<std::string> populate(void) const {
		if(false == this->initialized){
			Log::add().warning("OpenNIDriver::populate","The driver has not been initialized yet");
		}
		return this->names;
	}

	/**
	 * \brief Return the number of available grabbers
	 *
	 * \return Return the number of available grabbers
	 *
	 */
	unsigned int getCount(void) const {
		if(false == this->initialized){
			Log::add().warning("OpenNIDriver::getCount","The driver has not been initialized yet");
		}
		return this->names.size();
	}

protected:

	std::vector<std::string> names;
	bool initialized;
	static OpenNI2Driver driver_singleton;
};

}

#endif
