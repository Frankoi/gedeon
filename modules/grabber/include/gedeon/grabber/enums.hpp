/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * enums.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * enums.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRABBER_ENUMS_HPP__

#define GEDEON_GRABBER_ENUMS_HPP__

#include <gedeon/core/config.hpp>

namespace gedeon {

#define DEFAULT_FRAMERATE 30.0f
#define COMPRESSION_LEVEL 5

/**
 * \brief Enumeration of the available parameters for each grabber
 *
 * \author Francois de Sorbier
 */
enum GrabberParameter {

#ifdef USE_TIGEREYE
	TIGEREYE_FRAMERATE, ///< Framerate
	TIGEREYE_DETECTOR_GAIN, ///< The Detector Gain controls the bias on the detector. In general the best signalto-noise-ratio (SNR) is obtained with a high gain in the detector. High gain in the detector increases the detector bandwidth (frequency response of the detector) but reduces gain uniformity across the detector.
	TIGEREYE_AMPLIFIER_GAIN, ///< Signals from the detector are amplified in the readout integrated circuit (ROIC). The Amplifier Gain controls this gain.
	TIGEREYE_THRESHOLD_FILTER, ///< The Threshold Filter filters out short trigger pulses by requiring input signals to remain above the threshold longer. Higher settings of this parameter filter less and result in increased sensitivity.
	TIGEREYE_THRESHOLD_LEVEL, ///< The Threshold Level defines the threshold at which a return is detected. Setting this parameter too low will keep any return from being detected at all. On the other hand, noise will be detected if this parameter is set too high.
	TIGEREYE_LASER_DRIVE, ///< Efficiency of the laser (50% ~ 80%).
	TIGEREYE_SENSOR_TEMPERATURE, ///< Sensor temperature 25°~30°C
	TIGEREYE_LASER_TEMPERATURE, ///< Sensor temperature 35°~42°C
	TIGEREYE_SENSOR_VOLTAGE, ///< It should be very close to 6.3 Volt.
	TIGEREYE_SENSOR_HEAT_SINK_TEMPERATURE, ///< This number is information only because the camera cannot control this temperature. The normal expected values are < 40 degrees.
	TIGEREYE_LASER_HEAT_SINK_TEMPERATURE, ///< This number is information and the normal expected values are < 40 degrees.
	TIGEREYE_LASER_DIODE_TEMPERATURE, ///< This number should closely follow Laser Temperature with slight variations because this number represents data from an alternative laser temperature sensor.
	TIGEREYE_LASER_CAPACITY_VOLTAGE, ///< The operation range is from 16 ~ 32. Warning starts when it is smaller than 18 or larger than 31.
	TIGEREYE_PROCESS_BOARD_TEMPERATURE, ///< The operation range is from 0 ~ 60. Warning starts when it is smaller than 10 or larger than 50.
	TIGEREYE_RESET_COUNTDOWN, ///< Time remaining after a reset before the camera is again operational
#endif

#ifdef USE_OPENNI
	OPENNI_FRAMERATE, ///< Framerate (Get only)
	OPENNI_FOCAL_DEPTH, ///< Focal lenght
	OPENNI_PP_X_DEPTH, ///< X coordinate of the principal point
	OPENNI_PP_Y_DEPTH, ///< Y coordinate of the principal point
#endif

#ifdef USE_DEPTHSENSE
	DEPTHSENSE_FRAMERATE, ///< Framerate
#endif

#ifdef USE_OPENNI2
	OPENNI2_FRAMERATE, ///< Framerate (Get only)
	OPENNI2_FOCAL_LENGTH_X_COLOR, ///< Focal lenght on X axis for color image
	OPENNI2_FOCAL_LENGTH_Y_COLOR, ///< Focal lenght on Y axis for color image
	OPENNI2_FOCAL_LENGTH_X_DEPTH, ///< Focal lenght on X axis for depth image
	OPENNI2_FOCAL_LENGTH_Y_DEPTH, ///< Focal lenght on Y axis for depth image
	OPENNI2_PP_X_COLOR, ///< X coordinate of the principal point for color image
	OPENNI2_PP_Y_COLOR, ///< Y coordinate of the principal point for color image
	OPENNI2_PP_X_DEPTH, ///< X coordinate of the principal point for depth image
	OPENNI2_PP_Y_DEPTH, ///< Y coordinate of the principal point for depth image
#endif

#ifdef USE_FLYCAPTURE
	FLYCAPTURE_FRAMERATE, ///< Framerate (Get only)
	FLYCAPTURE_AUTO_EXPOSURE, ///< Value of exposure when in manual mode
	FLYCAPTURE_BRIGHTNESS, ///< Value of brightness
	FLYCAPTURE_SHARPNESS, ///< Value of sharpness
	FLYCAPTURE_WHITE_BALANCE, ///< Value of white balance when in manual mode
	FLYCAPTURE_HUE, ///< Value of hue
	FLYCAPTURE_SATURATION, ///< Value of saturation
	FLYCAPTURE_GAMMA, ///< Value of gamma correction
	FLYCAPTURE_IRIS, ///< Value of iris
	FLYCAPTURE_FOCUS, ///< Value of focus
	FLYCAPTURE_ZOOM, ///< Value of zoom
	FLYCAPTURE_PAN, ///< Value of pan
	FLYCAPTURE_TILT, ///< Value of brigthness
	FLYCAPTURE_SHUTTER, ///< Value of shutter speed
	FLYCAPTURE_GAIN, ///< Value of gain
#endif

#ifdef USE_V4L2
	V4L2_FRAMERATE, ///< Framerate (Get only)
	V4L2_EXPOSURE, ///< Value of exposure when in manual mode
	V4L2_BRIGHTNESS, ///< Value of brightness
	V4L2_SHARPNESS, ///< Value of sharpness
	V4L2_SATURATION, ///< Value of saturation
	V4L2_CONTRAST, ///< Value of contrast
	V4L2_FOCUS, ///< Value of focus
	V4L2_GAIN, ///< Value of gain
	V4L2_GAMMA, ///< Value of gamma correction
	V4L2_HUE, ///< Value of hue

#endif

	GEDEONFILE_FRAMERATE, ///< Framerate
	OPENCV_FRAMERATE, ///< Framerate
	NO_GRABBER_PARAMETER ///< Default parameter
};


/**
 * \brief Enumeration of the available triggers for each grabber
 *
 * \author Francois de Sorbier
 */
enum GrabberTrigger {

#ifdef USE_DEPTHSENSE
	DEPTHSENSE_NEAR_MODE,
#endif

#ifdef USE_TIGEREYE
	TIGEREYE_NUC, ///< The Non-Uniformity Correction (NUC) command is used to alleviate background noise effect.
	TIGEREYE_RESET, ///< Reset the camera (needs 16 seconds)
	TIGEREYE_INTERNAL_RECORD, ///< Record in the tigerEye's TXT file
#endif

#ifdef USE_OPENNI
	OPENNI_GRAB_INTENSITY, ///< Get intensity information instead of the color
	OPENNI_GRAB_COLOR, ///< Get color information instead of the intensity
	OPENNI_INTERNAL_RECORD, ///< Record in the OpenNI's ONI file
#endif

#ifdef USE_OPENNI2
	OPENNI2_GRAB_INTENSITY, ///< Get intensity information instead of the color
	OPENNI2_GRAB_COLOR, ///< Get color information instead of the intensity
	OPENNI2_AUTO_WHITE_BALANCE, ///< Activate or not the white balance
	OPENNI2_AUTO_EXPOSURE, ///< Activate or not the auto exposure
	OPENNI2_QVGA, ///< Use the QVGA resolution
	OPENNI2_VGA, ///< Use the VGA resolution
	//OPENNI2_SXGA,
	OPENNI2_30HZ_FRAMERATE, ///< Use the 30fps framerate
	OPENNI2_60HZ_FRAMERATE, ///< Use the 60fps framerate
	OPENNI2_INTERNAL_RECORD, ///< Record in the OpenNI2's ONI file
#endif

#ifdef USE_FLYCAPTURE
	FLYCAPTURE_AUTO_EXPOSURE_ONOFF, ///< Activate or not the auto exposure
	FLYCAPTURE_BRIGHTNESS_ONOFF, ///< Activate or not the auto brightness
	FLYCAPTURE_SHARPNESS_ONOFF, ///< Activate or not sharpness
	FLYCAPTURE_WHITE_BALANCE_ONOFF, ///< Activate or not the auto white balance
	FLYCAPTURE_HUE_ONOFF, ///< Activate or not the hue
	FLYCAPTURE_SATURATION_ONOFF, ///< Activate or not the saturation
	FLYCAPTURE_GAMMA_ONOFF, ///< Activate or not the auto amma correction
	FLYCAPTURE_IRIS_ONOFF, ///< Activate or not the iris
	FLYCAPTURE_FOCUS_ONOFF, ///< Activate or not the focus
	FLYCAPTURE_ZOOM_ONOFF, ///< Activate or not the zoom
	FLYCAPTURE_PAN_ONOFF, ///< Activate or not the pan
	FLYCAPTURE_TILT_ONOFF, ///< Activate or not the tilt
	FLYCAPTURE_SHUTTER_ONOFF, ///< Activate or not the shutter
	FLYCAPTURE_GAIN_ONOFF, ///< Activate or not the auto gain
	FLYCAPTURE_AUTO_EXPOSURE_AUTOMANUAL, ///< Change exposure to manual/auto
	FLYCAPTURE_BRIGHTNESS_AUTOMANUAL, ///< Change brightness to manual/auto
	FLYCAPTURE_SHARPNESS_AUTOMANUAL, ///< Change sharpness to manual/auto
	FLYCAPTURE_WHITE_BALANCE_AUTOMANUAL, ///< Change white balance to manual/auto
	FLYCAPTURE_HUE_AUTOMANUAL, ///< Change hue to manual/auto
	FLYCAPTURE_SATURATION_AUTOMANUAL, ///< Change saturation to manual/auto
	FLYCAPTURE_GAMMA_AUTOMANUAL, ///< Change gamma to manual/auto
	FLYCAPTURE_IRIS_AUTOMANUAL, ///< Change iris to manual/auto
	FLYCAPTURE_FOCUS_AUTOMANUAL, ///< Change focus to manual/auto
	FLYCAPTURE_ZOOM_AUTOMANUAL, ///< Change zoom to manual/auto
	FLYCAPTURE_PAN_AUTOMANUAL, ///< Change pan to manual/auto
	FLYCAPTURE_TILT_AUTOMANUAL, ///< Change tilt to manual/auto
	FLYCAPTURE_SHUTTER_AUTOMANUAL, ///< Change shutter to manual/auto
	FLYCAPTURE_GAIN_AUTOMANUAL, ///< Change gain to manual/auto
	FLYCAPTURE_EXPOSURE_PRIORITY_AUTOMANUAL, ///< Change the priority mode for the exposure
	FLYCAPTURE_1_875FPS, ///< Use the 1.875fps framerate
	FLYCAPTURE_3_75FPS, ///< Use the 3.75fps framerate
	FLYCAPTURE_7_5FPS, ///< Use the 7.5fps framerate
	FLYCAPTURE_15FPS, ///< Use the 15fps framerate
	FLYCAPTURE_30FPS, ///< Use the 30fps framerate
	FLYCAPTURE_60FPS, ///< Use the 60fps framerate
	FLYCAPTURE_120FPS, ///< Use the 120fps framerate
	FLYCAPTURE_240FPS, ///< Use the 240fps framerate
	FLYCAPTURE_YUV444_QQVGA, ///< Use the QQVGA resolution with YUV444 format
	FLYCAPTURE_YUV422_QVGA, ///< Use the QVGA resolution with YUV422 format
	FLYCAPTURE_YUV422_VGA, ///< Use the VGA resolution with YUV422 format
	FLYCAPTURE_YUV422_SVGA, ///< Use the SVGA resolution with YUV422 format
	FLYCAPTURE_YUV422_XGA, ///< Use the XGA resolution with YUV422 format
	FLYCAPTURE_YUV422_SXGA_, ///< Use the SXGA- resolution with YUV422 format
	FLYCAPTURE_YUV422_UXGA, ///< Use the UXGA resolution with YUV422 format
	FLYCAPTURE_RGB_VGA, ///< Use the VGA resolution with RGB format
	FLYCAPTURE_RGB_SVGA, ///< Use the SVGA resolution with RGB format
	FLYCAPTURE_RGB_XGA, ///< Use the XGA resolution with RGB format
	FLYCAPTURE_RGB_SXGA_, ///< Use the SXGA- resolution with RGB format
	FLYCAPTURE_RGB_UXGA, ///< Use the UXGA resolution with RGB format
#endif

#ifdef USE_V4L2
	V4L2_QQVGA, ///< Use the QQVGA resolution
	V4L2_QVGA, ///< Use the QVGA resolution
	V4L2_VGA, ///< Use the VGA resolution
	V4L2_SVGA, ///< Use the SVGA resolution
	V4L2_XGA, ///< Use the XGA resolution
	V4L2_SXGA, ///< Use the SXGA resolution
	V4L2_HD, ///< Use the HD resolution
	V4L2_FHD, ///< Use the FHD resolution
	V4L2_DV, ///< Use the DV resolution
	V4L2_WHITE_BALANCE_AUTOMANUAL, ///< Activate or not the white balance
	V4L2_HUE_AUTOMANUAL, ///< Activate or not the auto hue
	V4L2_EXPOSURE_AUTOMANUAL, ///< Activate or not the auto exposure
	V4L2_FOCUS_AUTOMANUAL, ///< Activate or not the auto focus
	V4L2_5FPS, ///< Use the 5fps framerate
	V4L2_7FPS, ///< Use the 7fps framerate
	V4L2_10FPS, ///< Use the 10fps framerate
	V4L2_15FPS, ///< Use the 15fps framerate
	V4L2_20FPS, ///< Use the 20fps framerate
	V4L2_24FPS, ///< Use the 24fps framerate
	V4L2_30FPS, ///< Use the 30fps framerate
	V4L2_60FPS, ///< Use the 60fps framerate
	V4L2_120FPS, ///< Use the 120fps framerate
#endif

	OPENCV_QQVGA, ///< Use the QQVGA resolution
	OPENCV_QVGA, ///< Use the QVGA resolution
	OPENCV_VGA, ///< Use the VGA resolution
	OPENCV_SVGA, ///< Use the SVGA resolution
	OPENCV_XGA, ///< Use the XGA resolution
	OPENCV_SXGA, ///< Use the SXGA resolution
	OPENCV_HD, ///< Use the HD resolution
	OPENCV_FHD, ///< Use the FHD resolution
	OPENCV_DV, ///< Use the DV resolution
	OPENCV_INTERNAL_RECORD, ///< Record the images in a movie file using the fourcc codec parameters for encoding
	GEDEONFILE_LOOP, ///< Loop or not the stream
	NO_GRABBER_TRIGGER ///< Default parameter

};

/**
 * \brief Enumeration of the available grabbers
 *
 * \author Francois de Sorbier
 */
enum Grabbers {

#ifdef USE_OPENNI
	OPENNI,
#endif

#ifdef USE_OPENNI2
	OPENNI2,
#endif

#ifdef USE_TIGEREYE
	TIGEREYE,
#endif

#ifdef USE_DEPTHSENSE
	DEPTHSENSE,
#endif

#ifdef USE_FLYCAPTURE
	FLYCAPTURE,
#endif

#ifdef USE_V4L2
	V4L2,
#endif

	OPENCV, GEDEONFILE, NO_GRABBERS

};

}


#endif
