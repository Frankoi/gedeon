/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * driver.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * driver.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRABBER__DRIVER_HPP__
#define GEDEON_GRABBER__DRIVER_HPP__

#include <vector>
#include <string>
#include "gedeon/core/log.hpp"

#ifdef WIN32
	#define UNUSED_VARIABLE(P) P
#endif

namespace gedeon {

/**
 * \brief Abstract class for defining a grabber's driver
 *
 * \author Francois de Sorbier
 */
class Driver {
public:

	/**
	 * \brief Destructor
	 *
	 */
	virtual ~Driver(void) {
	}

	/**
	 * \brief Perform a scan for finding the available grabbers
	 *
	 * \return true or false if something went wrong or not
	 *
	 */
	virtual bool refresh(void) = 0;

	/**
	 * \brief Give the name of the driver
	 *
	 * \return the name of the driver
	 *
	 */
	virtual std::string getName(void) const = 0;

	/**
	 * \brief Return the list of available grabbers
	 *
	 * \return Return a list of grabber names defined by a unique MAC address
	 *
	 */
	virtual std::vector<std::string> populate(void) const = 0;

	/**
	 * \brief Return the number of available grabbers
	 *
	 * \return Return the number of available grabbers
	 *
	 */
	virtual unsigned int getCount(void) const = 0;

};

}

#endif
