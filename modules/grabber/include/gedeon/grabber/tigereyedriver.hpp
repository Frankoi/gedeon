/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereyedriver.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereyedriver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRABBER__TIGER_EYE_DRIVER_HPP__
#define GEDEON_GRABBER__TIGER_EYE_DRIVER_HPP__

#include "gedeon/grabber/driver.hpp"

namespace gedeon {

/**
 * \brief Class for finding the available Tiger Eye cameras
 *
 * The class was written based on the specifications described in the version 2.2 of EtherIO manual
 *
 * \author Francois de Sorbier
 */
class TigerEyeDriver: public Driver {

private:

	/**
	 * \brief Constructor
	 *
	 */
	TigerEyeDriver(void);

	/**
	 * \brief Copy constructor
	 * \param dsd the object to use for construction
	 *
	 */
	TigerEyeDriver(const TigerEyeDriver& dsd){
#ifdef WIN32
		UNUSED_VARIABLE(dsd);
#endif
	}

	/**
	 * \brief Destructor
	 *
	 */
	~TigerEyeDriver(void);

	/**
	 * \brief Copy operator
	 * \param dsd the object to copy
	 * \return this updated object
	 *
	 */
	TigerEyeDriver& operator=(const TigerEyeDriver& dsd){
#ifdef WIN32
		UNUSED_VARIABLE(dsd);
#endif
		return *this;
	}

public:

	/**
	 * \brief Return a unique instance of this object
	 *
	 * \return An unique instance of this object
	 *
	 */
	static inline TigerEyeDriver& getInstance(void) {
		return driver_singleton;
	}

	/**
	 * \brief Ask to scan the network interface for finding the available cameras
	 *
	 * \return true or false if something went wrong or not
	 *
	 */
	bool refresh(void);

	/**
	 * \brief Give the name of the driver
	 *
	 * \return the name of the driver
	 *
	 */
	std::string getName(void) const {
		return std::string("ASC Tiger Eye");
	}

	/**
	 * \brief Return the list of available grabbers
	 *
	 * \return Return a list of grabber names defined by a unique MAC address
	 *
	 */
	std::vector<std::string> populate(void) const {
		if(false == this->initialized){
			Log::add().warning("TigerEyeDriver::populate","The driver has not been initialized yet");
		}
		return this->devices_mac_addresses;
	}

	/**
	 * \brief Define the interface from where the data will be captured
	 *
	 * \param inter The network interface from which we will search for the cameras. Common values are "eth0" for Linux or "Local Area Connection" for windows.
	 * 		  Network interfaces can be different and found through the commands ifconfig or ipconfig (ethernet adapter)
	 * \return if the function succeeded or not
	 */
	bool setInterface(const std::string& inter = "eth0");

	/**
	 * \brief Return the number of available grabbers
	 *
	 * \return Return the number of available grabbers
	 *
	 */
	unsigned int getCount(void) const {
		if(false == this->initialized){
			Log::add().warning("TigerEyeDriver::getCount","The driver has not been initialized yet");
		}
		return this->devices_mac_addresses.size();
	}

	/**
	 * \brief Return the local MAC address of the machine
	 *
	 * \return The local MAC address
	 *
	 */
	unsigned char* getLocalMacAddress(void) const;

	/**
	 * \brief Return the name of the network interface used
	 *
	 * \return The name of the network interface used
	 *
	 */
	std::string getNetworkDevice(void) const;

protected:

	std::vector<std::string> devices_mac_addresses;
	std::string network_device;
	unsigned char *broadcast_packet;
	unsigned char *local_mac_address;
	bool initialized;
	static TigerEyeDriver driver_singleton;
};

}

#endif
