/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * projectivecamera.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * projectivecamera.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/projectivecamera.hpp"
#include "gedeon/calibration/tools.hpp"

namespace gedeon {

cv::Mat intrinsic_parameters;

cv::Mat extrinsic_parameters;

cv::Mat distortion_coefficients;

ProjectiveCamera::ProjectiveCamera(void){
	intrinsic_parameters = cv::Mat::eye(3,3,CV_32F);
	extrinsic_parameters = cv::Mat::eye(3,4,CV_32F);
	distortion_coefficients = cv::Mat::zeros(5,1,CV_32F);
}

ProjectiveCamera::~ProjectiveCamera(void){
	intrinsic_parameters.release();
	extrinsic_parameters.release();
	distortion_coefficients.release();
}

const ProjectiveCamera& ProjectiveCamera::operator=( const ProjectiveCamera& input ){
	input.extrinsic_parameters.copyTo(this->extrinsic_parameters);
	input.intrinsic_parameters.copyTo(this->intrinsic_parameters);
	input.distortion_coefficients.copyTo(this->distortion_coefficients);
	return *this;
}


void ProjectiveCamera::setIntrinsicParameters(const float& focal_x,
		const float& focal_y, const float& principal_x,
		const float& principal_y, const float& skew) {
	intrinsic_parameters = cv::Mat(3, 3, CV_32F);
	this->intrinsic_parameters.at<float>(0, 0) = focal_x;
	this->intrinsic_parameters.at<float>(0, 1) = skew;
	this->intrinsic_parameters.at<float>(0, 2) = principal_x;
	this->intrinsic_parameters.at<float>(1, 0) = 0.0f;
	this->intrinsic_parameters.at<float>(1, 1) = focal_y;
	this->intrinsic_parameters.at<float>(1, 2) = principal_y;
	this->intrinsic_parameters.at<float>(2, 0) = 0.0f;
	this->intrinsic_parameters.at<float>(2, 1) = 0.0f;
	this->intrinsic_parameters.at<float>(2, 2) = 1.0f;
}

void ProjectiveCamera::setIntrinsicParameters(const float input[9]) {
	this->intrinsic_parameters = cv::Mat(3, 3, CV_32F, (void*) input);
}

void ProjectiveCamera::setIntrinsicParameters(const cv::Mat& input) {
	assert(input.rows == 3 && input.cols = 3);
	cv::Mat input_32f;
	input.convertTo(input_32f, CV_32F);
	input_32f.copyTo(this->intrinsic_parameters);
}

void ProjectiveCamera::setDistortionCoeffiecients(const float& k1,
		const float& k2, const float& p1, const float& p2, const float& k3) {
	this->distortion_coefficients =
			(cv::Mat_<float>(5, 1) << k1, k2, p1, p2, k3);
}
void ProjectiveCamera::setDistortionCoeffiecients(const float input[5]) {
	this->distortion_coefficients = cv::Mat(5, 1, CV_32F, (void*) input);
}

void ProjectiveCamera::setDistortionCoeffiecients(const cv::Mat& input) {
	assert(input.rows == 5 && input.cols = 1);
	cv::Mat input_32f;
	input.convertTo(input_32f, CV_32F);
	input_32f.copyTo(this->distortion_coefficients);
}

void ProjectiveCamera::setExtrinsicParameters(const cv::Mat& rotation,
		const cv::Mat& translation) {
	this->extrinsic_parameters = cv::Mat(3, 4, CV_32F);
	cv::Mat rotation_32f;
	if(rotation.cols != rotation.rows){
		cv::Mat converted_rotation;
		cv::Rodrigues(rotation,converted_rotation);
		rotation.convertTo(converted_rotation, CV_32F);
	}else{
		rotation.convertTo(rotation_32f, CV_32F);
	}
	cv::Mat translation_32f;
	translation.convertTo(translation_32f, CV_32F);
	for (unsigned int i = 0; i < 3; ++i) {
		for (unsigned int j = 0; j < 3; ++j) {
			this->extrinsic_parameters.at<float>(i, j) = rotation_32f.at<float>(
					i, j);
		}
		this->extrinsic_parameters.at<float>(i, 3) = translation_32f.at<float>(
				i, 0);
	}
}

void ProjectiveCamera::setExtrinsicParameters(const float input[12]) {
	this->extrinsic_parameters = cv::Mat(3, 4, CV_32F, (void*) input);
}

void ProjectiveCamera::setExtrinsicParameters(const cv::Mat& input) {
	assert(input.rows == 3 && input.cols = 4);
	cv::Mat input_32f;
	input.convertTo(input_32f, CV_32F);
	input_32f.copyTo(this->extrinsic_parameters);
}

bool ProjectiveCamera::save(const std::string& filename){
	cv::FileStorage fs(filename, cv::FileStorage::WRITE);
	if(false == fs.isOpened()){
		return false;
	}
	fs << "camera_matrix" <<  this->intrinsic_parameters;
	fs << "distortion_coefficients" << this->distortion_coefficients;
	fs << "extrinsic_parameters" << this->extrinsic_parameters;
	fs.release();
	return true;
}


bool ProjectiveCamera::load(const std::string& filename){
	cv::FileStorage fs(filename, cv::FileStorage::READ);
	if(false == fs.isOpened()){
		return false;
	}
	cv::FileNode fn;
	fn = fs["camera_matrix"];
	if(fn.type() != cv::FileNode::NONE){
		fn >> this->intrinsic_parameters;
	}
	fn = fs["distortion_coefficients"] ;
	if(fn.type() != cv::FileNode::NONE){
		fn >> this->distortion_coefficients;
	}
	fn = fs["extrinsic_parameters"];
	if(fn.type() != cv::FileNode::NONE){
		fn  >> this->extrinsic_parameters;
	}
	fs.release();
	return true;
}


}

