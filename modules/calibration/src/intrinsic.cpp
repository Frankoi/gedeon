/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * intrinsic.cpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * intrinsic.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/intrinsic.hpp"

#include <opencv2/calib3d/calib3d.hpp>

namespace gedeon {

namespace Intrinsic {

bool calibrate(cv::Mat& intrinsic,
		const std::vector< std::vector<cv::Point2f> >& image,
		const std::vector< std::vector<cv::Point3f> >& object,
		const Size& image_size,
		const bool& fixed_principal_point){

	if(image.size() != object.size()){
		return false;
	}

	std::vector<cv::Mat> rvecs;
	std::vector<cv::Mat> tvecs;
	cv::Mat cameraMatrix;
	cv::Mat distCoeffs = cv::Mat::zeros(5, 1, CV_64F);
	int flags = cv::CALIB_FIX_K1 | cv::CALIB_FIX_K2 | cv::CALIB_FIX_K3 | cv::CALIB_ZERO_TANGENT_DIST;
	if(fixed_principal_point){
		flags |= cv::CALIB_FIX_PRINCIPAL_POINT;
	}
	cv::calibrateCamera(object, image,
			cv::Size(image_size.width,image_size.height), cameraMatrix,
			distCoeffs, rvecs, tvecs, flags );

	cameraMatrix.convertTo(intrinsic, CV_32F);
	return true;
}

bool calibrateWithDistortion(cv::Mat& intrinsic,
		cv::Mat& distortion,
		const  std::vector< std::vector<cv::Point2f> >& image,
		const std::vector< std::vector<cv::Point3f> >& object,
		const Size& image_size,
		const bool& fixed_principal_point){

	if(image.size() != object.size()){
		return false;
	}

	std::vector<cv::Mat> rvecs;
	std::vector<cv::Mat> tvecs;
	cv::Mat cameraMatrix;
	cv::Mat distCoeffs;
	int flags = 0;
	if(fixed_principal_point){
		flags |= cv::CALIB_FIX_PRINCIPAL_POINT;
	}
	cv::calibrateCamera(object, image,
			cv::Size(image_size.width,image_size.height), cameraMatrix,
			distCoeffs, rvecs, tvecs, flags );

	distCoeffs.convertTo(distortion, CV_32F);
	if(distCoeffs.rows < distCoeffs.cols){
		distortion = distortion.t();
	}
	cv::Mat cameraMatrix_32f;
	cameraMatrix.convertTo(intrinsic, CV_32F);

	return true;

}


}

}

