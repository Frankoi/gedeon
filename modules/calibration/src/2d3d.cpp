/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * 2d3d.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * 2d3d.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/2d3d.hpp"
#include "gedeon/calibration/tools.hpp"
#include "gedeon/calibration/projectivecamera.hpp"
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>
namespace gedeon{

namespace Calibration2D3D{

	void calibrate(ProjectiveCamera& camera, const std::vector< cv::Point2f >& coordinates2d, const std::vector< cv::Point3f >& coordinates3d, const unsigned int& ransac_iterations ){

		if(coordinates2d.size() == 0 || coordinates3d.size() == 0 || coordinates2d.size() != coordinates3d.size()){
			return;
		}

		cv::Mat rvec, tvec;

		if(ransac_iterations == 0){
			cv::solvePnP(coordinates3d, coordinates2d, camera.intrinsic_parameters, camera.distortion_coefficients, rvec, tvec, false, cv::EPNP );
		}else{
			cv::Mat inliers;
			cv::solvePnPRansac(coordinates3d, coordinates2d, camera.intrinsic_parameters, camera.distortion_coefficients, rvec, tvec, false, ransac_iterations, 3.0f, static_cast<int>(coordinates2d.size()*0.2), inliers, cv::EPNP );
		}
		cv::Mat rotation;
		cv::Rodrigues(rvec, rotation);
		camera.setExtrinsicParameters(rotation,tvec);
	}


}

}
