/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * backproject.cu created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * backproject.cu is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/calibration/backproject.cuh"
#include "gedeon/core/core.cuh"

namespace gedeon {

namespace GPU {

namespace BackProject {

__global__
void backProjectKernel(float* output, const float* input,
		const float inverse_focal_x, const float inverse_focal_y,
		const float principal_x, const float principal_y, const float skew,
		const unsigned int width, const unsigned int height) {

	// Get the corresponding 2D coordinate
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

	if (x < width && y < height) {

		float depth = input[y * width + x];

		/*
		 * If input value equals 0, then the depth is invalid
		 */
		if (0.0001f > depth) {
			output[(y * width + x) * 3] = quiet_NaN();
			output[(y * width + x) * 3 + 1] = quiet_NaN();
			output[(y * width + x) * 3 + 2] = quiet_NaN();
		} else {
			/*
			 * First compute the Y value then X
			 */
			output[(y * width + x) * 3 + 1] = (height - y - principal_y) * depth
					* inverse_focal_y;
			output[(y * width + x) * 3] = ((width - x - principal_x) * depth
					- skew * output[(y * width + x) * 3 + 1]) * inverse_focal_x;
			output[(y * width + x) * 3 + 2] = depth;
		}
	}

}

bool apply(float *d_output, float *d_input,
		const float intrinsic_parameters[9], const unsigned int& width,
		const unsigned int& height, const unsigned int& block_x,
		const unsigned int& block_y) {

	if(0 == d_output || 0 == d_input || 0 == intrinsic_parameters){
		Log::add().error("GPU::BackProject::backProject", "One of the input array is invalid");
		return false;
	}

	if(0.0f == intrinsic_parameters[0] || 0.0f == intrinsic_parameters[4]){
		Log::add().error("GPU::BackProject::backProject", "Focal lenght is incorrect");
		return false;
	}

	dim3 blockSize(block_x, block_y);
	dim3 gridSize((width + blockSize.x - 1) / blockSize.x,
			(height + blockSize.y - 1) / blockSize.y);

	float inverse_focal_x = 1.0f / intrinsic_parameters[0];
	float inverse_focal_y = 1.0f / intrinsic_parameters[4];
	backProjectKernel<<< gridSize, blockSize>>>( d_output, d_input,
			inverse_focal_x, inverse_focal_y,
			intrinsic_parameters[2], intrinsic_parameters[5],
			intrinsic_parameters[1], width, height);
	return true;
}

}

}

}

