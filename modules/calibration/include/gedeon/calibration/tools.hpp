/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tools.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tools.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__TOOLS_HPP__
#define GEDEON_CALIBRATION__TOOLS_HPP__

#include "gedeon/datatypes/rgbimage.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

namespace gedeon {

namespace Tools {

/**
 * \brief Set the content of an intrinsic parameter matrix with the given arguments
 *
 * The input matrix intrinsicparameters has to be allocated
 *
 * \param[out] intrinsicparameters The matrix that will be filled with intrisinc parameters
 * \param[in] focal_x The focal on the x axis
 * \param[in] focal_y The focal on the y axis
 * \param[in] principal_x The x coordinate of the principal point
 * \param[in] principal_y The y coordinate of the principal point
 * \param[in] skew The skew value if there is one
 *
 * \author Francois de Sorbier
 */
void setIntrinsicParametersMatrix(float* intrinsicparameters,
		const float& focal_x, const float& focal_y, const float& principal_x,
		const float& principal_y, const float& skew = 0.0f);

namespace OpenCV {

/**
 * \brief Detect the corners of a chessboard on a planar surface
 *
 * \param[out] corners The list of detected corners
 * \param[in] image The input opencv image (CV_8UC3)
 * \param[in] pattern The number of inner corners on the chessboard per row and per column
 *
 * \return true if the chessboard has been successfully detected
 *
 * \author Francois de Sorbier
 */
bool detectChessboard(std::vector<cv::Point2f>& corners, const cv::Mat& image,
		const cv::Size& pattern);

/**
 * \brief Detect the center of circles on a planar surface
 *
 * \param[out] corners The list of detected corners
 * \param[in] image The input opencv image (CV_8UC3)
 * \param[in] pattern The number of circles per row and per column
 * \param[in] detector Parameters for the circle detector
 * \param[in] symmetric Define if the circles are organized in a symmetric manner or not
 *
 *	Example of values for the detector:
 *
 * params.maxArea = 10e4;
 * params.minArea = 10;
 * params.minDistBetweenBlobs = 5;
 *
 * \return true if the circles have been successfully detected
 *
 * \author Francois de Sorbier
 */
bool detectCirclesGrid(std::vector<cv::Point2f>& corners, const cv::Mat& image,
		const cv::Size& pattern, const cv::SimpleBlobDetector::Params& detector,
		const bool& symmetric = true);

/**
 * \brief Convert the 3x4 [R|t] OpenCV matrix into the 4x4 OpenGL Modelview matrix
 *
 * The Y and Z axis are inverted and the matrix is transposed
 *
 * \param[in] opencv_mat The OpenCV matrix
 *
 * \return The OpenGL Modelview matrix in an OpenCV structure (float)
 *
 * \author Francois de Sorbier
 */
cv::Mat convertOpenCVToOpenGLExtrinsic(const cv::Mat& opencv_mat);

/**
 * \brief Convert the 3x3 K OpenCV matrix into the 4x4 OpenGL Projection matrix
 *
 * The projection is inverted on the Y axis to match the OpenGL projection matrix
 *
 * \param[in] opencv_mat The OpenCV matrix
 * \param[in] width The width of the OpenCV camera image
 * \param[in] height The height of the OpenCV camera image
 * \param[in] znear The OpenGL near clipping plane value
 * \param[in] zfar The OpenGL far clipping plane value
 *
 * \return The OpenGL Projection matrix in an OpenCV structure (float)
 *
 * \author Francois de Sorbier
 */
cv::Mat convertOpenCVToOpenGLIntrinsic(const cv::Mat& opencv_mat,
		const unsigned int& width, const unsigned int& height,
		const float& znear, const float& zfar);

}

/**
 * \brief Detect the center of circles on a planar surface
 *
 * \param[out] corners The list of detected corners
 * \param[in] image The input gedeon color image
 * \param[in] pattern The number of circles per row and per column
 * \param[in] params Parameters for the circle detector
 * \param[in] symmetric Define if the circles are organized in a symmetric manner or not
 *
 *	Example of values for the detector:
 *
 * params.maxArea = 10e4;
 * params.minArea = 10;
 * params.minDistBetweenBlobs = 5;
 *
 * \return true if the circles have been successfully detected
 *
 * \author Francois de Sorbier
 */
bool detectCirclesGrid(std::vector<cv::Point2f>& corners, const RGBImage& image,
		const cv::Size& pattern, const cv::SimpleBlobDetector::Params& params,
		const bool& symmetric);

/**
 * \brief Detect the corners of a chessboard on a planar surface
 *
 * \param[out] corners The list of detected corners
 * \param[in] image The input gedeon color image
 * \param[in] pattern The number of inner corners on the chessboard per row and per column
 *
 * \return true if the chessboard has been successfully detected
 *
 * \author Francois de Sorbier
 */
bool detectChessboard(std::vector<cv::Point2f>& corners, const RGBImage& image,
		const cv::Size& pattern);

}

}

#endif
