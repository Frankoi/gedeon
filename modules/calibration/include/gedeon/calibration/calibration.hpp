/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * calibration.hpp created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * calibration.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__CALIBRATION_HPP__
#define GEDEON_CALIBRATION__CALIBRATION_HPP__

#include "gedeon/calibration/2d3d.hpp"
#include "gedeon/calibration/3d3d.hpp"
#include "gedeon/calibration/backproject.hpp"
#include "gedeon/calibration/intrinsic.hpp"
#include "gedeon/calibration/projectivecamera.hpp"
#include "gedeon/calibration/projector.hpp"
#include "gedeon/calibration/tools.hpp"

#endif
