/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * projectivecamera.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * projectivecamera.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__PROJECTIVE_CAMERA_HPP__
#define GEDEON_CALIBRATION__PROJECTIVE_CAMERA_HPP__

#include <opencv2/core/core.hpp>

namespace gedeon{

/**
 * \brief Structure to hold calibration parameters
 *
 * It contains intrinsic parameters (camera matrix), extrinsic parameters (camera pose) and distortion coefficients
 *
 * \author Francois de Sorbier
 */

struct ProjectiveCamera{

	cv::Mat intrinsic_parameters; /*!< Intrinsic parameters (camera matrix) */

	cv::Mat extrinsic_parameters; /*!< Extrinsic parameters (camera pose) */

	cv::Mat distortion_coefficients; /*!< Distortion coefficients k1, k2, p1, p2, k3 */

	/**
	 * \brief Constructor
	 *
	 * Set intrinsic parameters and extrinsic parameters to identity, and distortion coefficients to zero
	 *
	 */
	ProjectiveCamera(void);

	/**
	 * \brief Destructor
	 *
	 */
	~ProjectiveCamera(void);

	/**
	 * \brief Assignment operator
	 *
	 * \param input The object that will be copied
	 * \return The new content of the current object
	 */
	const ProjectiveCamera& operator=( const ProjectiveCamera& input );

	/**
	 * \brief Assign values to the intrinsic parameters
	 *
	 * \param focal_x The focal length on the x axis
	 * \param focal_y The focal length on the y axis
	 * \param principal_x The principal point on the x axis
	 * \param principal_y The principal point on the y axis
	 * \param skew The skew parameter
	 */
	void setIntrinsicParameters(const float& focal_x , const float& focal_y,
									const float& principal_x, const float& principal_y,
									const float& skew = 0.0f);
	/**
	 * \brief Assign values to the intrinsic parameters
	 *
	 *	\param input Array of 3x3 floating point values (focal_x, skew, principal_x, 0, focal_y, principal_y, 0, 0, 1)
	 */
	void setIntrinsicParameters(const float input[9]);

	/**
	 * \brief Assign values to the intrinsic parameters
	 *
	 * \param input A 3x3 matrix
	 */
	void setIntrinsicParameters(const cv::Mat& input);

	/**
	 * \brief Assign values to the distortion coefficients
	 *
	 * \param k1 First coefficient of the radial distortion
	 * \param k2 Second coefficient of the radial distortion
	 * \param p1 First coefficient of the tangential distortion
	 * \param p2 Second coefficient of the tangential distortion
	 * \param k3 Third coefficient of the radial distortion
	 */
	void setDistortionCoeffiecients(const float& k1, const float& k2, const float& p1, const float& p2, const float& k3);

	/**
	 * \brief Assign values to the distortion coefficients
	 *
	 * \param input The five distortion coefficients (k1, k2, p1, p2, k3)
	 */
	void setDistortionCoeffiecients(const float input[5]);

	/**
	 * \brief Assign values to the distortion coefficients
	 *
	 * \param input A 5x1 matrix
	 */
	void setDistortionCoeffiecients(const cv::Mat& input);

	/**
	 * \brief Assign values to the extrinsic parameters
	 *
	 * \param rotation a rotation vector (3x1 or 1x3 matrix) or a 3x3 rotation matrix
	 * \param translation a 3x1 translation vector
	 */
	void setExtrinsicParameters(const cv::Mat& rotation, const cv::Mat& translation);

	/**
	 * \brief Assign values to the extrinsic parameters
	 *
	 * \param input The data of the 3x4 matrix [R|t]
	 */
	void setExtrinsicParameters(const float input[12]);

	/**
	 * \brief Assign values to the extrinsic parameters
	 *
	 * \param input a 3x4 matrix [R|t]
	 */
	void setExtrinsicParameters(const cv::Mat& input);

	/**
	 * \brief Save the calibration parameters in a file
	 *
	 * \param filename The destination file
	 * \return true if save action was successful, otherwise false
	 */
	bool save(const std::string& filename);

	/**
	 * \brief Load the calibration parameters from a file
	 *
	 * \param filename The source file
	 * \return true if load action was successful, otherwise false
	 */
	bool load(const std::string& filename);
};

}

#endif
