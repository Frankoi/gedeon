/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * backproject.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * backproject.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__BACK_PROJECT_CUH__
#define GEDEON_CALIBRATION__BACK_PROJECT_CUH__

namespace gedeon {

namespace GPU {

namespace BackProject {

/**
 * \brief Back project depth data to the 3D space
 *
 * If the depth equals zero then the 3D point is considered as invalid
 *
 * \param[out] d_output The point cloud generated from the depth data (array on the device)
 * \param[out] d_input The generated validity_map defining if a 3D point is valid or not (array on the device)
 * \param[in] d_input The depth map (array on the device)
 * \param[in] intrinsic_parameters Intrinsic parameters associated to the depth data
 * \param width the width of the image
 * \param height the height of the image
 * \param block_x the number of threads we want on the x axis of the cuda 2D block
 * \param block_y the number of threads we want on the y axis of the cuda 2D block
 *
 * \author Francois de Sorbier
 */
bool apply( float *d_output, float *d_input,
		const float* intrinsic_parameters,
		const unsigned int& width, const unsigned int& height,
		const unsigned int& block_x, const unsigned int& block_y);

}

}

}

#endif
