/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * projector.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * projector.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_CALIBRATION__PROJECTOR_HPP__
#define GEDEON_CALIBRATION__PROJECTOR_HPP__

#include <boost/shared_array.hpp>

namespace gedeon {

namespace Projector {

namespace GrayCode {

/**
 * \brief Convert a graycode into a decimal
 *
 * \param graycode The graycode to be converted
 *
 * \return The corresponding decimal value
 *
 * \author Francois de Sorbier
 */
unsigned int toValue(const unsigned int& graycode);

/**
 * \brief Return a given number of gray codes
 *
 * \param[out] levels The maximum number of levels for the generated gray codes
 * \param size The level of the gray to check
 *
 * \return The 1D graycode encoded in unsigned int for each pixel
 *
 * \author Francois de Sorbier
 */
boost::shared_array<unsigned int> get(int& levels, const unsigned int& size);

/**
 * \brief Determine if the corresponding level of a gray code is 0 (black) or 1 (white)
 *
 * \param code The graycode to be analyzed
 * \param level The level of the gray to check
 *
 * \return True if it is white, false if it black
 *
 * \author Francois de Sorbier
 */
bool isWhite(const unsigned int& code, const unsigned int& level);

/**
 * \brief Store the given value at a specific position in the code
 *
 * \param code The code inside of which the value will be encoded
 * \param value The value to be encoded
 * \param level The level where to place the value
 *
 * \author Francois de Sorbier
 */
void encode(unsigned int& code, const bool& value, const unsigned int& level);

/**
 * \brief Give the corresponding value to a code
 *
 * \param code The code to decode
 *
 * \return The decoded value
 *
 * \author Francois de Sorbier
 */
unsigned int decode(const unsigned int& code);

/**
 * \brief Set an image with the gray code at one specific level
 *
 * \param[out] grayimage The grayscale image (1 channel) that will contain the gray code
 * \param width The width of the input image
 * \param height The height of the input image
 * \param graycode The graycode that will be used to set the image
 * \param horizontal_axis TTrue if the graycode is along the horizontal axis (vertical stripes), false otherwise
 * \param level The level of the gray to use
 *
 * \sa get
 *
 * \return True if everything was fine, false otherwise
 *
 * \author Francois de Sorbier
 */
void createImageC1(unsigned char *grayimage, const unsigned int& width,
		const unsigned int& height,
		const boost::shared_array<unsigned int> &graycode, const bool& horizontal_axis,
		const unsigned int& level);


/**
 * \brief Set an image with the gray code at one specific level
 *
 * \param[out] grayimage The grayscale image (3 channels) that will contain the gray code
 * \param width The width of the input image
 * \param height The height of the input image
 * \param graycode The graycode that will be used to set the image
 * \param horizontal_axis True if the graycode is along the horizontal axis (vertical stripes), false otherwise
 * \param level The level of the gray to use
 *
 * \sa get
 *
 * \return True if everything was fine, false otherwise
 *
 * \author Francois de Sorbier
 */
void createImageC3(unsigned char *grayimage,
		const unsigned int& width, const unsigned int& height,
		const boost::shared_array<unsigned int> &graycode,
		const bool& horizontal_axis,
		const unsigned int& level);

}

}

}

#endif
