/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * glslshader.hpp created in 09 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * glslshader.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_GRAPHICS__GLSL_SHADER_HPP__
#define GEDEON_GRAPHICS__GLSL_SHADER_HPP__

#include <GL/glew.h>

namespace gedeon {


namespace GLSLShader{

/**
 * \brief check if shaders are available
 *
 * \author Francois de Sorbier
 * \return true if shaders are available, false otherwise
 */
bool checkAvailability(void);

/**
 * \brief Compile a shader object
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 *
 * \warning Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 * \author Francois de Sorbier
 * \param object a shader object
 */
bool compile(GLuint object);

/** \brief Compile an array of shader objects
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 *
 * \warning Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 * \author Francois de Sorbier
 * \param objects array of shader objects
 * \param nbobjects number of shader objects
 */
bool compile(GLuint * objects,const size_t& nbobjects);

/**
 * \brief Link an array of compiled shader objects
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 *
 * \warning Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 * \author Francois de Sorbier
 * \param objects array of compiled shader objects
 * \param nb number of shader objects
 * \return a program object linked with compiled shader objects
 */
GLuint link(GLuint* objects, const unsigned int& nb);

/**
 * \brief Load a GLSL file and create the related shader object. It can be a vertex, geometry or fragment shader.
 *
 * A vertex file has the extension ".vert" or contains the word "vertex".
 * A fragment file has the extension ".frag" or contains the word "fragment" or "pixel".
 * A fragment file has the extension ".geom" or contains the word "geometry".
 *
 * The function return 0 if the loading have failed
 * \author Francois de Sorbier
 * The function requires the ARB_shading_language_100 OpenGL extension.
 * The function requires the ARB_geometry_shader4 OpenGL extension to load geometry shader files.
 *
 * Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 *
 * \param shader_code the GLSL code
 * \return GLuint the shader object created
 */
GLuint loadShaderFromFile(const char * filename);

/**
 * \brief Load a vertex shader file and create the vertex shader object.
 *
 * The function return 0 if the loading have failed
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 *
 * Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 *
 * \param shader_code the vertex shader code
 * \return GLuint the vertex shader object created
 */
GLuint loadVertexShader(const char * shader_code);

/**
 * \brief \brief Load a fragment shader file and create the fragment shader object.
 *
 * The function return 0 if the loading have failed
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 * \author Francois de Sorbier
 * \param shader_code the fragment shader code
 * \return GLuint the fragment shader object created
 */
GLuint loadFragmentShader(const char * shader_code);

/**
 * \brief Load a geometry shader file and create the geometry shader object.
 *
 * The function return 0 if the loading have failed
 *
 * The function requires the ARB_shading_language_100 OpenGL extension.
 * The function requires the ARB_geometry_shader4 OpenGL extension.
 *
 * Don't forget to create a valid OpenGL context and to call the function glewInit() before using this function.
 * \author Francois de Sorbier
 * \param shader_code the geometry shader code
 * \return GLuint the geometry shader object created
 */
GLuint loadGeometryShader(const char * shader_code);

}

}

#endif
