/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * fbo.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * fbo.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_GRAPHICS__FBO_HPP__
#define GEDEON_GRAPHICS__FBO_HPP__

/*
 * External Includes
 */
#include <string>

namespace gedeon
{

/*
 * Namespace
 */
namespace fbo
{

/**
 * \brief check if fbo are available
 *
 * \author Francois de Sorbier
 * \return true if fbo are available, false otherwise
 */
bool checkAvailability(void);

/**
 * \brief Return the status of the current FBO
 *
 * It returns 0 if everything is ok
 * \author Francois de Sorbier
 * \return the value of the current error
 */
unsigned int getError(void);

/**
 * \brief Return the string corresponding to the current FBO error
 *
 * \param error the value of the current error
 * \author Francois de Sorbier
 * \sa fboGetError
 * \return the string corresponding to the error
 */
std::string getErrorString(const unsigned int error);

/**
 * \brief Bind the FBO given in argument
 *
 * \param id the id of the FBO
 * \author Francois de Sorbier
 * \sa unbind
 */
void bind(const unsigned int& id);

/**
 * \brief Unbind the current bind FBO
 *
 * \author Francois de Sorbier
 * \sa bind
 */
void unbind(void);

/**
 * \brief Get the current bind FBO
 *
 * \author Francois de Sorbier
 * \sa bind
 * \return return the current bind FBO
 */
unsigned int getCurrent(void);

}

}


#endif
