/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * graphics.hpp created in 09 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * graphics.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_GRAPHICS__GRAPHICS_HPP__
#define GEDEON_GRAPHICS__GRAPHICS_HPP__

#include "gedeon/graphics/glslshader.hpp"
#include "gedeon/graphics/vao.hpp"
#include "gedeon/graphics/fbo.hpp"
#include "gedeon/graphics/texture.hpp"
#include "gedeon/graphics/virtualcamera.hpp"

#endif
