/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * time.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * time.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__TIME_HPP__
#define GEDEON_CORE__TIME_HPP__

#include <gedeon/core/log.hpp>

#ifdef WIN32
#include <WinSock2.h>
#include <time.h>
#else
#include <sys/time.h>
#include <time.h>
#endif

#include <stdio.h>

namespace gedeon {

/**
 * \brief Class for time management
 *
 * \author Francois de Sorbier
 */
class Timer {

private:

#ifdef _WIN32
	LARGE_INTEGER start_;
	LARGE_INTEGER now;
	LARGE_INTEGER freq;
#else
	timeval start_;
#endif

public:

	/**
	 * \brief Start the timer
	 *
	 */
	inline void start(void) {
#ifdef _WIN32
		QueryPerformanceCounter(&start_);
#else
		gettimeofday(&start_, 0);
#endif
	}

	/**
	 * \brief Stop the timer
	 *
	 */
	inline void stop(void) {

#ifdef _WIN32
		QueryPerformanceCounter(&start_);
#else
		gettimeofday(&start_, 0);
#endif

	}

	/**
	 * \brief Return the time elapsed since the last call to start
	 *
	 * \return elapsed time in milliseconds
	 */
	inline long get(void) {
#ifdef _WIN32

		QueryPerformanceCounter(&now);
		QueryPerformanceFrequency(&freq);

		return long(((now.QuadPart - start_.QuadPart)*1000.0) / static_cast<double>(freq.QuadPart));
#else
		timeval now;
		gettimeofday(&now, 0);
		return (now.tv_sec - start_.tv_sec) * 1000
				+ (now.tv_usec - start_.tv_usec)*0.001;
#endif
	}

};

/**
 * \brief Sleep a given number of milliseconds
 *
 * \param milliseconds Number of milliseconds to wait
 * \author Francois de Sorbier
 */
#define NANO_SECOND_MULTIPLIER  1000000
inline void millisecSleep(const unsigned int& milliseconds) {
#ifdef _WIN32
	Sleep(milliseconds);
#else
	timespec sleepValue = {0};
	const long INTERVAL_MS = milliseconds * NANO_SECOND_MULTIPLIER;
	sleepValue.tv_nsec = INTERVAL_MS;
	nanosleep(&sleepValue, NULL);
	//usleep(milliseconds*1000);
#endif
}

/**
 * \brief Sleep a given number of seconds
 *
 * \param seconds Number of seconds to wait
 * \author Francois de Sorbier
 */
inline void secSleep(const unsigned int& seconds) {
#ifdef _WIN32
	Sleep(seconds*1000);
#else
	sleep(seconds);
#endif
}


#if defined(_WIN32)

/*
 * Function gettimeofday for windows
 * version obtain from http://code.google.com/p/tesseract-ocr/
 */

inline void
  l_getCurrentTime(int  *sec,
                   int  *usec)
  {
  ULARGE_INTEGER  utime, birthunix;
  FILETIME        systemtime;
  LONGLONG        birthunixhnsec = 116444736000000000;  /*in units of 100 ns */
  LONGLONG        usecs;

      GetSystemTimeAsFileTime(&systemtime);
      utime.LowPart  = systemtime.dwLowDateTime;
      utime.HighPart = systemtime.dwHighDateTime;

      birthunix.LowPart = (DWORD) birthunixhnsec;
      birthunix.HighPart = birthunixhnsec >> 32;

      usecs = (LONGLONG) ((utime.QuadPart - birthunix.QuadPart) / 10);

      if (sec) *sec = (int) (usecs /   1000000);
      if (usec) *usec = (int) (usecs % 1000000);
      return;
  }

inline int gettimeofday(struct timeval *tp, struct timezone *tzp) {
	UNREFERENCED_PARAMETER(tzp);
  int sec, usec;
  if (tp == NULL)
    return -1;

  l_getCurrentTime(&sec, &usec);
  tp->tv_sec = sec;
  tp->tv_usec = usec;
  return 0;
}

#endif



/**
 * \brief return a string with the current data and time
 *
 * \return the data and time in the format %Y-%m-%d_%H-%M-%S
 * \author Francois de Sorbier
 */
inline const std::string currentDateTime(void) {
	time_t now = time(0);
	struct tm tstruct;
	tstruct = *localtime(&now);

	char buf[64];
	char tmbuf[64];
	strftime(buf, sizeof(buf), "%Y-%m-%d %H-%M-%S", &tstruct);

	struct timeval tval;
	if (gettimeofday(&tval, NULL) < 0) {
		Log::add().error("currentDateTime","Error while getting the time");
		return std::string(buf);
	}
#if defined(_WIN32)
	_snprintf(tmbuf, sizeof buf, "%s.%03d", buf, int(tval.tv_usec / 1000));
#else
	snprintf(tmbuf, sizeof buf, "%s.%03d", buf, int(tval.tv_usec / 1000));
#endif
	return std::string(tmbuf);
}

/**
 * \brief return a timestamp in millisecond based on the current time
 *
 * \return the  timestamp in milliseconds
 * \author Francois de Sorbier
 */
inline long long unsigned int getTimeStamp(void) {
	struct timeval tval;

	if (gettimeofday(&tval, NULL) < 0) {
		Log::add().error("getTimeStamp","Error while getting the time");
		return 1;
	}
	return tval.tv_sec * 1000LL + tval.tv_usec / 1000;
}


/**
 * \brief Convert a millisecond timestamp into a string
 *
 * \param ts The timestamp in milliseconds
 * \return the converted timestamp in the format %Y-%m-%d %H:%M:%S.%ms
 * \author Francois de Sorbier
 */
inline std::string timeStampToString(const long long unsigned int& ts){
	struct timeval tval;
	tval.tv_sec = long(ts) / 1000L;
	tval.tv_usec = (long(ts) - (tval.tv_sec * 1000L));

	time_t nowtime;
	char tmbuf[64], buf[64];
	nowtime = tval.tv_sec;
	struct tm *nowtm = localtime(&nowtime);
	strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", nowtm);
#if defined(_WIN32)
	_snprintf(buf, sizeof buf, "%s.%03d", tmbuf, int(tval.tv_usec));
#else
	snprintf(buf, sizeof buf, "%s.%03d", tmbuf, int(tval.tv_usec));
#endif
	return std::string(buf);
}

}

#endif
