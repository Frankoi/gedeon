/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tools.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tools.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/
#ifndef GEDEON_CORE__MATHS_CUH__
#define GEDEON_CORE__MATHS_CUH__

#include <cutil.h>
#include <cutil_math.h>

namespace gedeon {

namespace GPU {

static __device__ __forceinline__ float3 operator*(const float mat[9], const float3& point){
		return make_float3(mat[0] * point.x + mat[1] * point.y + mat[2] * point.z,
							mat[3] * point.x + mat[4] * point.y + mat[5] * point.z,
							mat[6] * point.x + mat[7] * point.y + mat[8] * point.z);
	}

static __device__ __forceinline__ float norm(const float3& point){
		return sqrtf(dot(point, point));
	}


}

}

#endif
