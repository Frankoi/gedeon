/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * core.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * core.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_CORE__CORE_CUH__
#define GEDEON_CORE__CORE_CUH__

#include "gedeon/core/config.hpp"

#ifdef USE_CUDA
	#include "gedeon/core/cudaio.hpp"
	#include "gedeon/core/tools.cuh"
	#include "gedeon/core/maths.cuh"
#endif


#endif
