/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tools.cuh created in 07 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tools.cuh is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/
#ifndef GEDEON_CORE__TOOLS_CUH__
#define GEDEON_CORE__TOOLS_CUH__

#include <cuda_runtime.h>
#include <cuda.h>
#include <cutil.h>
#include <cutil_math.h>

namespace gedeon {

namespace GPU {

static __device__ __forceinline__ float quiet_NaN(void) {
	return __int_as_float(0x7fffffff); /*CUDART_NAN_F*/
}

static __device__  __forceinline__ float3 makeFloat3FromArray(float *array,
		const int& x, const int& y, const int& stride) {
	float3 point;
	float *ptr = array + (x + y * stride) * 3;
	point.x = ptr[0];
	point.y = ptr[1];
	point.z = ptr[2];
	return point;
}

static __device__ __forceinline__ void setArrayFromPoint(const float3& point,
		float *array, const int& x, const int& y, const int& stride) {
	float *ptr = array + (x + y * stride) * 3;
	ptr[0] = point.x;
	ptr[1] = point.y;
	ptr[2] = point.z;
}

template<unsigned int blockSize>
__device__ __forceinline__ void reduceSumInPlace(float* buffer) {

	int tid = threadIdx.y * blockDim.x + threadIdx.x;
	float val = buffer[tid];

	if (blockSize >= 1024) {
		if (tid < 512)
			buffer[tid] = val = (val + buffer[tid + 512]);
		__syncthreads();
	}
	if (blockSize >= 512) {
		if (tid < 256)
			buffer[tid] = val = (val + buffer[tid + 256]);
		__syncthreads();
	}
	if (blockSize >= 256) {
		if (tid < 128)
			buffer[tid] = val = (val + buffer[tid + 128]);
		__syncthreads();
	}
	if (blockSize >= 128) {
		if (tid < 64)
			buffer[tid] = val = (val + buffer[tid + 64]);
		__syncthreads();
	}

	if (tid < 32) {

		volatile float *smem = buffer;

		if (blockSize >= 64) {
			smem[tid] = val = (val + smem[tid + 32]);
		}
		if (blockSize >= 32) {
			smem[tid] = val = (val + smem[tid + 16]);
		}
		if (blockSize >= 16) {
			smem[tid] = val = (val + smem[tid + 8]);
		}
		if (blockSize >= 8) {
			smem[tid] = val = (val + smem[tid + 4]);
		}
		if (blockSize >= 4) {
			smem[tid] = val = (val + smem[tid + 2]);
		}
		if (blockSize >= 2) {
			smem[tid] = val = (val + smem[tid + 1]);
		}
	}
}

template<unsigned int blockSize>
__device__ __forceinline__ void ReduceSum(float* result, float* array,
		const int size_array) {

	int tid = threadIdx.x;

	const float *end = array + size_array;


	float sum = 0.f;
	for (const float *t = array + tid; t < end; t += blockSize) {
		sum += *t;
	}

	__shared__ float smem[blockSize];

	smem[tid] = sum;
	__syncthreads();

	reduceSumInPlace< blockSize >(smem);

	if (tid == 0)
		result[blockIdx.x] = smem[0];
}

}

}

#endif
