#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * CMakeLists.txt created in 05 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * This CMakeLists.txt is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

find_package(Doxygen)

if(DOXYGEN_FOUND) 
  configure_file(${${PROJECT_NAME}_SOURCE_DIR}/doc/Doxyfile-html.in ${${PROJECT_NAME}_BINARY_DIR}/doc/Doxyfile-html)
  add_custom_target (doc ${DOXYGEN_EXECUTABLE} ${${PROJECT_NAME}_BINARY_DIR}/doc/Doxyfile-html)
 SET(CMAKE_DOXYGEN_IMAGE_PATH ${${PROJECT_NAME}_SOURCE_DIR}/${IMAGE_PATH})
endif (DOXYGEN_FOUND)
